package com.tvs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tvs.Activity.Home;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import APIInterface.CategoryAPI;
import Adapter.OrderDetailsAdapter;
import Model.orderdetailsview.ViewOrderDetailsData;
import Model.orderdetailsview.ViewOrderDetailsFully;
import RetroClient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsFully extends AppCompatActivity {

    private ListView orderdetailsfully;
    private String orderid,tot;
    private List<ViewOrderDetailsData> viewOrderDetails;
    private OrderDetailsAdapter orderDetailsAdapter;
    private TextView totalamtmonth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_fully);
        orderdetailsfully=findViewById(R.id.orderdetailsfully);
        totalamtmonth=findViewById(R.id.totalamtmonth);
        orderid=getIntent().getStringExtra("orderid");
        tot=getIntent().getStringExtra("total");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        int ordervalue= Integer.parseInt(tot);
        String moneyString = formatter.format(ordervalue);
        totalamtmonth.setText(moneyString);
        getorderdetailsfully();
    }

    private void getorderdetailsfully() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(OrderDetailsFully.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<ViewOrderDetailsFully> call = service.orderdetails(orderid);
            call.enqueue(new Callback<ViewOrderDetailsFully>() {
                @Override
                public void onResponse(Call<ViewOrderDetailsFully> call, Response<ViewOrderDetailsFully> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        viewOrderDetails=response.body().getData();
                        orderDetailsAdapter=new OrderDetailsAdapter(OrderDetailsFully.this,viewOrderDetails);
                        orderdetailsfully.setAdapter(orderDetailsAdapter);
                    } else {
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<ViewOrderDetailsFully> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
