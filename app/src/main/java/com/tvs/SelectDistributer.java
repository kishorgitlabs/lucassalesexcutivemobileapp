package com.tvs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.SelectDistributerAdapter;
import Model.distributerdetails.DistriButerData;
import Model.distributerdetails.DistributerDetails;
import Model.new_search.states.StateList;
import Model.orderdetailstosend.ExistOrdersList;
import Model.orderdetailstosend.OrderDetailsToPost;
import Model.ordermodel.Order_Parts;
import Model.orderresult.OrderNumber;
import RetroClient.RetroClient;
import Shared.Config;
import dbhelper.DbHelper;
import dbhelper.PartDetailsDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectDistributer extends AppCompatActivity implements SelectDistributerAdapter.Disdetails{

    private AutoCompleteTextView statespinner,cityspinner;
    private int stateValue,cityValue;
    List<String> Cityarray;
    private String CITY,selectedcity,selectedstate;
    AlertDialog.Builder builder;
    ArrayAdapter cityAdapter;
    private Button searchdis,placeorder;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private ListView distributerlist;
    private SelectDistributerAdapter selectDistributerAdapter;
    private List<DistriButerData> distriButerData;
    private String disid="",disname="",disaddress="";
    private String dealermob,dealerusertype,shopname,salesexestate,execode;
    private List<Order_Parts> order_parts;
    private PartDetailsDB db;
    private SQLiteDatabase database;
    private AlertDialog alerterror;
    private OrderDetailsToPost orderDetailsToPost;
    public static final String CART_TABLE_NAME = "CartItems";
    public static final String COLUMN_PART_SEGMENT = "segments";
    public static final String COLUMN_PART_NUMBER = "partnumber";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_SNO="SNO";
    public static final String COLUMN_TOTALAMT="totalamunt";
    private String distributerid,distributername,distributeraddress,formattedDate,grandtotal,executiveid;
    private AlertDialog orderplaced;
    private List<ExistOrdersList> existOrdersLists;
    private TextView grandtotals;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_distributer);
        builder = new AlertDialog.Builder(this);
//        statespinner=findViewById(R.id.statespinner);
        cityspinner=findViewById(R.id.cityspinner);
        searchdis=findViewById(R.id.searchdis);
        placeorder=findViewById(R.id.placeorder);
        distributerlist=findViewById(R.id.distributerlist);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        dealermob=getIntent().getStringExtra("dealermobile");
        dealerusertype=getIntent().getStringExtra("dealerusertype");
        shopname=getIntent().getStringExtra("shopname");
        execode=sharedPreferences.getString("KEY_log_User_Name","");
        grandtotals=findViewById(R.id.grandtotals);
        salesexestate=sharedPreferences.getString("state", "");
        DbHelper dbHelper =new DbHelper(SelectDistributer.this);
        database=dbHelper.readDataBase();
        executiveid=sharedPreferences.getString("executiveid","");
        db=new PartDetailsDB(this);
        order_parts=db.getCartItem();
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);


        cityspinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(Cityarray);
            }
        });

        searchdis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedcity==null||TextUtils.isEmpty(selectedcity)){
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Please select city", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else {
                    getdistributerdetails();
                }

            }
        });

        placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (disid.equals("")) {
                    StyleableToast st = new StyleableToast(getApplication(),
                            "Select Distributor", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else {
                    new getorderitems().execute();


                }
            }
        });

        getcitystatewise(salesexestate);
    }

    private class getorderitems extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            existOrdersLists=new ArrayList<ExistOrdersList>();
            orderDetailsToPost=new OrderDetailsToPost();
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                String query="SELECT  DISTINCT partnumber,segments,quantity,description,price,totalamunt FROM CartItems";
                Cursor curr = database.rawQuery(query, null);
                int overalltotal=0;
                if (curr.moveToFirst()) {
                    do {
                        String totalamt=curr.getString(curr.getColumnIndex(COLUMN_TOTALAMT));
                        int tt=Integer.parseInt(totalamt);
                        overalltotal+=tt;
                    }
                    while (curr.moveToNext());
                    grandtotal=String.valueOf(overalltotal);
//                        grandtotals.setText(grandtotal);
                    if(curr.moveToFirst()) {
                        do {
                            ExistOrdersList existOrdersList = new ExistOrdersList();
                            existOrdersList.setDisid(disid);
                            existOrdersList.setDistributorName(disname);
                            existOrdersList.setDealermobile(dealermob);
                            existOrdersList.setDealerusertype(dealerusertype);
                            existOrdersList.setEmpCode(execode);
                            existOrdersList.setPartNo(curr.getString(curr.getColumnIndex(COLUMN_PART_NUMBER)));
                            existOrdersList.setQuantity(curr.getString(curr.getColumnIndex(COLUMN_QUANTITY)));
                            existOrdersList.setAmount(curr.getString(curr.getColumnIndex(COLUMN_PRICE)));
                            existOrdersList.setProductname(curr.getString(curr.getColumnIndex(COLUMN_DESCRIPTION)));
                            existOrdersList.setUnitprice(grandtotal);
                            existOrdersList.setOrderType("Secondary Order");
                            existOrdersList.setSegment(curr.getColumnName(curr.getColumnIndex(COLUMN_PART_SEGMENT)));
                            existOrdersList.setOrderMode("CashBill");
                            existOrdersList.setExcuteid(executiveid);
                            existOrdersList.setDate(formattedDate);
                            existOrdersList.setGrandtotal(curr.getString(curr.getColumnIndex(COLUMN_TOTALAMT)));
                            existOrdersList.setExecutivename(sharedPreferences.getString("KEY_Name", ""));
                            existOrdersLists.add(existOrdersList);
                        }
                        while (curr.moveToNext());
                    }
                    curr.close();
                    db.closeDatabase();
//                        grandtotal=String.valueOf(overalltotal);
                    orderDetailsToPost.setExistOrdersList(existOrdersLists);
//                        progressDialog.dismiss();
                    return "Success";
                }
            }
            catch (Exception e){
                return e.getMessage();
            }
            return "Notsuccess";
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("Success")){
                grandtotals.setText(grandtotal);
                placeorder();
            }else {
                StyleableToast st = new StyleableToast(getApplication(),
                        "Something went wrong order details please try again later", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getApplication().getResources().getColor(R.color.colorPrimary));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            }
        }
    }

    private void getdistributerdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SelectDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<DistributerDetails> call = service.getdistriburter(selectedcity);
            call.enqueue(new Callback<DistributerDetails>() {
                @Override
                public void onResponse(Call<DistributerDetails> call, Response<DistributerDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        distriButerData=response.body().getData();
                        selectDistributerAdapter=new SelectDistributerAdapter(SelectDistributer.this,distriButerData);
                        distributerlist.setAdapter(selectDistributerAdapter);
                    } else if (response.body().getResult().equals("NotSuccess")) {
                        progressDialog.dismiss();
                        getfailurealert("No distributer found in selected city");
                    }
                }
                @Override
                public void onFailure(Call<DistributerDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }

    }

    public void getcitystatewise(String state) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SelectDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<StateList> call = service.getdistributer(state);
            call.enqueue(new Callback<StateList>() {
                @Override
                public void onResponse(Call<StateList> call, Response<StateList> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        Cityarray = response.body().getData();
//                        cityAdapter = new ArrayAdapter(SelectDistributer.this, android.R.layout.simple_list_item_1, Cityarray);
//                        cityspinner.setAdapter(cityAdapter);
//                        CITY = cityspinner.getText().toString();
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No distributer found in selected state and city");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<StateList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert(t.getMessage());
//                    Toast.makeText(SelectDistributer.this, "Server Problem", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert(ex.getMessage());
//            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showPopup(List<String> city) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);
            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final TextView close=(TextView) dialogView.findViewById(R.id.close);
            cityAdapter =new ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,city);
            list.setAdapter(cityAdapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    cityspinner.setText(selectedcity);
                    alertDialog.dismiss();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void placeorder() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SelectDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<OrderNumber> call = service.placeorder(orderDetailsToPost);
            call.enqueue(new Callback<OrderNumber>() {
                @Override
                public void onResponse(Call<OrderNumber> call, Response<OrderNumber> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        db.deleteTable();
                        getorderstatus(response.body().getData().get(0).getOrderid());
                    } else if (response.body().getResult().equals("MobileNumberExist")){
                        progressDialog.dismiss();
                        getfailurealert("Please Select Another Distributer To Place Order");
//                        Toast.makeText(getApplicationContext(), "Please Try Again Later", Toast.LENGTH_LONG).show();
                    }else {
                        progressDialog.dismiss();
                        //for result error from json method
                        getfailurealert("Cannot place order now please try again later");
                    }
                }
                @Override
                public void onFailure(Call<OrderNumber> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
//                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error please try again later");
        }
    }
    private void getorderstatus(String orderid) {
        orderplaced=new AlertDialog.Builder(SelectDistributer.this).create();
        View orderplacedshow= LayoutInflater.from(SelectDistributer.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText("Your order placed successfully, Your id is"+" "+orderid+"");

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        orderplaced.setView(orderplacedshow);
        orderplaced.setCanceledOnTouchOutside(false);
        orderplaced.show();
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(SelectDistributer.this).create();
        View orderplacedshow= LayoutInflater.from(SelectDistributer.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);
        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerterror.dismiss();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void senddistributerid(String id) {
        disid=id;
    }
    @Override
    public void senddistributername(String distriname) {
        disname=distriname;
    }
    @Override
    public void senddistributeraddress(String distriaddress) {
        disaddress=distriaddress;
    }
}
