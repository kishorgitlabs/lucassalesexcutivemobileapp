package com.tvs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import APIInterface.CategoryAPI;
import Model.customerdetails.CustomerDetails;
import Model.usertypewisecustomerlist.CustomerMobileList;
import RetroClient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitReport extends AppCompatActivity {

    private boolean permissionGranted=false;
    private FusedLocationProviderClient mFusedLocationClient;
    private FusedLocationProviderApi api;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private Boolean mRequestingLocationUpdates;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 500;
    private String TAG="sehome";
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private TextView address,name,companyname;
    SharedPreferences sharedPreferences;
    private String visitaddress="";
    SharedPreferences.Editor editor;
    private TextView placeofvisit,visitdatee,customername;
    private String visitdate,selectedusertype,selectedmobile;
    private Spinner usertypevisit;
    private List<String> usermobilelist;
    private AutoCompleteTextView mobilenumber;
    private String [] usertypes ={"Select User type","RETAILER","MECHANIC","ELECTRICIAN"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_report);
        placeofvisit=findViewById(R.id.placeofvisit);
        visitdatee=findViewById(R.id.visitdatee);
        usertypevisit=findViewById(R.id.usertypevisit);
        mobilenumber=findViewById(R.id.mobilenumber);
        customername=findViewById(R.id.customername);
        initPermission();
        init();
        startLocationUpdate();
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        visitdate = df.format(c.getTime());
        visitdatee.setText(visitdate);
        ArrayAdapter user=new ArrayAdapter(VisitReport.this,R.layout.simple_spinner_item,usertypes);
        user.setDropDownViewResource(R.layout.simple_spinner_item);
        usertypevisit.setAdapter(user);

        usertypevisit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedusertype = parent.getItemAtPosition(position).toString();
                if(selectedusertype.equals("Select Usertype"))
                {
                    StyleableToast st = new StyleableToast(VisitReport.this,
                            "Select Usertype", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    getcustomermobile();
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        mobilenumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedmobile=mobilenumber.getAdapter().getItem(i).toString();
                getcustomerdetails();
            }
        });
    }

    private void getcustomermobile() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(VisitReport.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<CustomerMobileList> call = service.getcusmob(selectedusertype);
            call.enqueue(new Callback<CustomerMobileList>() {
                @Override
                public void onResponse(Call<CustomerMobileList> call, Response<CustomerMobileList> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        usermobilelist=response.body().getData();
                        usermobilelist.remove(null);
                        mobilenumber.setAdapter(new ArrayAdapter<String>(VisitReport.this,android.R.layout.simple_dropdown_item_1line,usermobilelist));
                        mobilenumber.setThreshold(1);
                    } else {
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<CustomerMobileList> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void getcustomerdetails() {
        try {

            final ProgressDialog progressDialog = new ProgressDialog(VisitReport.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<CustomerDetails> call = service.cusdetails(selectedmobile,selectedusertype);
            call.enqueue(new Callback<CustomerDetails>() {
                @Override
                public void onResponse(Call<CustomerDetails> call, Response<CustomerDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        customername.setText(response.body().getData().getOwnerName());
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<CustomerDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    private void initPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            permissionGranted=true;
            init();
            startLocationUpdate();
        }
        else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},9 );
        }
        }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==9&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
            init();
        }else {
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialogBuilder.setMessage("Please allow the permission to access your loacation");
            alertDialogBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            initPermission();
                        }
                    });
            alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    initPermission();
                }
            });


            alertDialogBuilder.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==100){
            startLocationUpdate();
        }
    }

    private void init() {
        if(permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateLocationUI();
                }
            };
            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }

    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

//                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());



//                        LocationManager manager= (LocationManager) getSystemService(LOCATION_SERVICE);
                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(VisitReport.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(VisitReport.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted=true;
                                init();
                        }
                        updateLocationUI();
                    }
                });
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
        }
    }
    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {
        String errorMessage = "";
        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(VisitReport.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            }
//            else {
//                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
//            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {
            if (addresss == null) {
//                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                String address = addresss.getAddressLine(0);
                String City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                visitaddress=address +
                        "\n"
                        + title;
                placeofvisit.setText(visitaddress);

                Geocoder geocoder = new Geocoder(VisitReport.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
