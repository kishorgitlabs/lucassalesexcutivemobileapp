package com.tvs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.Activity.Home;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.ViewAttendanceAdapter;
import Model.attendancedatewise.Attedancedatewiseview;
import Model.attendancedatewise.DatewiseAttendanceData;
import Model.getpartnumber.GetPartNumber;
import Model.viewattendance.ViewAttendanceData;
import Model.viewattendance.ViewAttendanceJson;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendance extends AppCompatActivity {

    private ListView listattendance;
    private ViewAttendanceAdapter viewAttendanceAdapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String empid;
    private List<ViewAttendanceData> attendanceData;
    private List<DatewiseAttendanceData> datewiseAttendanceData;
    private AlertDialog alerterror;
    private Button searchdatewise;
    private boolean mAutoHighlight;
    private LinearLayout datechoosen;
    private static String FromDate = "",Todate = "";
    private static TextView fromdateeatt,todateeatt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);

        sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        listattendance=findViewById(R.id.listattendance);
        empid=sharedPreferences.getString("executiveid", "");
        searchdatewise=findViewById(R.id.searchdatewise);
        datechoosen=findViewById(R.id.datechoosen);
        fromdateeatt=findViewById(R.id.fromdateeatt);
        todateeatt=findViewById(R.id.todateeatt);

        searchdatewise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FromDate.equals("")){
                    StyleableToast st = new StyleableToast(ViewAttendance.this,
                            "Select from date", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }else if (Todate.equals("")){
                    StyleableToast st = new StyleableToast(ViewAttendance.this,
                            "Select to date", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }else {
                    datewiseattendance();
                }


            }
        });

        fromdateeatt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });
        todateeatt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragmentto();
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });

        getattendance();
    }

    private void datewiseattendance() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewAttendance.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<Attedancedatewiseview> call = service.getdatewise(FromDate,Todate,empid);
            call.enqueue(new Callback<Attedancedatewiseview>() {
                @Override
                public void onResponse(Call<Attedancedatewiseview> call, Response<Attedancedatewiseview> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        datewiseAttendanceData=response.body().getData();
                        viewAttendanceAdapter=new ViewAttendanceAdapter(ViewAttendance.this,datewiseAttendanceData,true);
                        listattendance.setAdapter(viewAttendanceAdapter);
                    } else  if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("No data found");
                    }
                }
                @Override
                public void onFailure(Call<Attedancedatewiseview> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }

    }


    public static class DatePickerFragmentto extends DialogFragment
            implements android.app.DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
//            DatePickerDialog =   dialog.getDatePicker().setMaxDate(new Date().getTime());
//            // Create a new instance of DatePickerDialog and return it
//            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthselect=month+1;
            String monthselected = new DateFormatSymbols().getMonths()[month];
            String monthget=monthselected.substring(0, 3);
            todateeatt.setText(day+"-"+monthget +"-"+year);
            Todate=year+"-"+monthselect+"-"+day;
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements android.app.DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
//            DatePickerDialog =   dialog.getDatePicker().setMaxDate(new Date().getTime());
//            // Create a new instance of DatePickerDialog and return it
//            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthselect=month+1;
            String monthselected = new DateFormatSymbols().getMonths()[month];
            String monthget=monthselected.substring(0, 3);
            fromdateeatt.setText(day+"-"+monthget +"-"+year);
            FromDate=year+"-"+monthselect+"-"+day;
        }
    }

    private void getattendance() {
        try {

            final ProgressDialog progressDialog = new ProgressDialog(ViewAttendance.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<ViewAttendanceJson> call = service.viewattendance(empid);
            call.enqueue(new Callback<ViewAttendanceJson>() {
                @Override
                public void onResponse(Call<ViewAttendanceJson> call, Response<ViewAttendanceJson> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        attendanceData=response.body().getData();
                        viewAttendanceAdapter=new ViewAttendanceAdapter(ViewAttendance.this,attendanceData,true);
                        listattendance.setAdapter(viewAttendanceAdapter);
                    } else  if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("You not traveled any trip");
                    }
                }
                @Override
                public void onFailure(Call<ViewAttendanceJson> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Execption error to fetch data from server");
        }

    }



    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ViewAttendance.this).create();
        View orderplacedshow= LayoutInflater.from(ViewAttendance.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        alert_head.setText("View Attendance");
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
}
