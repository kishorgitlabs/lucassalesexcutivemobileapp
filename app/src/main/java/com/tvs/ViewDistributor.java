package com.tvs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.Activity.Home;

import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.SelectDistributerAdapter;
import Adapter.ViewDistributorAdapter;
import Model.distributerdetails.DistriButerData;
import Model.distributerdetails.DistributerDetails;
import Model.new_search.states.StateList;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDistributor extends AppCompatActivity {


    private ListView distributorlist;
    private ViewDistributorAdapter viewDistributorAdapter;
    private AutoCompleteTextView city;
    private AlertDialog alerterror;
    private List<String> citylist;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String salesexestate,selectedcity="";
    private ArrayAdapter cityadapter;
    private List<DistriButerData> distriButerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_distributor);
        distributorlist=findViewById(R.id.distributorlist);
        city=findViewById(R.id.city);

        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        salesexestate=sharedPreferences.getString("state", "");

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupcity(citylist);
            }
        });
        getcitystatewise(salesexestate);

    }

    public  void Searchdis(View view){
        if (selectedcity.equals("")){
            StyleableToast st = new StyleableToast(ViewDistributor.this,
                    "Please select city", Toast.LENGTH_SHORT);
            st.setBackgroundColor(getResources().getColor(R.color.logocolor));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }else {
            getdistributerlist();
        }

    }

    private void getdistributerlist() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewDistributor.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<DistributerDetails> call = service.getdistriburter(selectedcity);
            call.enqueue(new Callback<DistributerDetails>() {
                @Override
                public void onResponse(Call<DistributerDetails> call, Response<DistributerDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        distriButerData=response.body().getData();
                        viewDistributorAdapter=new ViewDistributorAdapter(ViewDistributor.this,distriButerData);
                        distributorlist.setAdapter(viewDistributorAdapter);
                    } else if (response.body().getResult().equals("NotSuccess")) {
                        progressDialog.dismiss();
                        getfailurealert("No distributer found in selected city");
                    }
                }
                @Override
                public void onFailure(Call<DistributerDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error");
        }

    }
    public void getcitystatewise(String state) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewDistributor.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<StateList> call = service.getSateCity(state);
            call.enqueue(new Callback<StateList>() {
                @Override
                public void onResponse(Call<StateList> call, Response<StateList> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        citylist=response.body().getData();
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No city found in your state");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<StateList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert("Failed to ferch data from server");
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert("Execption error please try again later");

        }
    }

    private void showPopupcity(List<String> cStrings) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);
            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
            spinerTitle.setText("Select City");
            cityadapter=new ArrayAdapter(ViewDistributor.this, android.R.layout.simple_dropdown_item_1line, citylist);
            list.setAdapter(cityadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    city.setText(selectedcity);
                    alertDialog.dismiss();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ViewDistributor.this).create();
        View orderplacedshow= LayoutInflater.from(ViewDistributor.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        ordernumber.setText(alert);
        alert_head.setText("View Distributor");
        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
