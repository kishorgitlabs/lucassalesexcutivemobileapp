package com.tvs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.tvs.Activity.Home;
import com.tvs.Activity.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.ConfirmOrderAdapter;
import Adapter.SelectDistributerAdapter;
import Model.distributerdetails.DistributerDetails;
import Model.orderdetailstosend.ExistOrdersList;
import Model.orderdetailstosend.OrderDetailsToPost;
import Model.ordermodel.Order_Parts;
import Model.orderresult.OrderNumber;
import RetroClient.RetroClient;
import Shared.Config;
import dbhelper.DbHelper;
import dbhelper.PartDetailsDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmOrder extends AppCompatActivity {

    private List<Order_Parts> order_parts;
    private PartDetailsDB db;
    private SQLiteDatabase database;
    private ConfirmOrderAdapter confirmOrderAdapter;
    private ListView confirmdetails;
    private String distributerid,distributername,distributeraddress,dealermob,dealerusertype,formattedDate,grandtotal,shopname,executiveid;
    private TextView distributernameview,distiaddressview;
    private OrderDetailsToPost orderDetailsToPost;
    public static final String CART_TABLE_NAME = "CartItems";
    public static final String COLUMN_PART_SEGMENT = "segments";
    public static final String COLUMN_PART_NUMBER = "partnumber";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_SNO="SNO";
    public static final String COLUMN_TOTALAMT="totalamunt";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private List<ExistOrdersList> existOrdersLists;
    private Button confirmorder;
    private AlertDialog orderplaced;
    private AlertDialog alerterror;
    private TextView grandtotals;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        confirmdetails=findViewById(R.id.confirmdetails);
        distributernameview=findViewById(R.id.distributername);
        distiaddressview=findViewById(R.id.distiaddress);
//        grandtotals=findViewById(R.id.grandtotals);
        distributerid=getIntent().getStringExtra("disid");
        distributername=getIntent().getStringExtra("disname");
        distributeraddress=getIntent().getStringExtra("disaddress");
        dealermob=getIntent().getStringExtra("dealermob");
        dealerusertype=getIntent().getStringExtra("dealerusertype");
        shopname=getIntent().getStringExtra("shopname");
        distributernameview.setText(distributername);
        distiaddressview.setText(distributeraddress);
        DbHelper dbHelper =new DbHelper(ConfirmOrder.this);
        database=dbHelper.readDataBase();
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        executiveid=sharedPreferences.getString("executiveid","");
        db=new PartDetailsDB(this);
        confirmorder=findViewById(R.id.confirmorder);
        order_parts=db.getCartItem();

        confirmOrderAdapter=new ConfirmOrderAdapter(this,order_parts);
        confirmdetails.setAdapter(confirmOrderAdapter);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);
        new  getorderitems().execute();

        confirmorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeorder();
            }
        });
    }
    private class getorderitems extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            existOrdersLists=new ArrayList<ExistOrdersList>();
            orderDetailsToPost=new OrderDetailsToPost();
        }
        @Override
        protected String doInBackground(String... strings) {
                try {
//
                    String query="SELECT  DISTINCT partnumber,segments,quantity,description,price,totalamunt FROM CartItems";
                    Cursor curr = database.rawQuery(query, null);
                    int overalltotal=0;
                    if (curr.moveToFirst()) {
                        do {
                            String totalamt=curr.getString(curr.getColumnIndex(COLUMN_TOTALAMT));
                            int tt=Integer.parseInt(totalamt);
                            overalltotal+=tt;
                        }
                        while (curr.moveToNext());
                        grandtotal=String.valueOf(overalltotal);
//                        grandtotals.setText(grandtotal);
                        if(curr.moveToFirst()) {
                            do {
                                ExistOrdersList existOrdersList = new ExistOrdersList();
                                existOrdersList.setDisid(distributerid);
                                existOrdersList.setDistributorName(distributername);
                                existOrdersList.setDealermobile(dealermob);
                                existOrdersList.setDealerusertype(dealerusertype);
                                existOrdersList.setPartNo(curr.getString(curr.getColumnIndex(COLUMN_PART_NUMBER)));
                                existOrdersList.setQuantity(curr.getString(curr.getColumnIndex(COLUMN_QUANTITY)));
                                existOrdersList.setAmount(curr.getString(curr.getColumnIndex(COLUMN_TOTALAMT)));
                                existOrdersList.setUnitprice(curr.getString(curr.getColumnIndex(COLUMN_PRICE)));
                                existOrdersList.setOrderType("Secondary Order");
                                existOrdersList.setSegment(curr.getColumnName(curr.getColumnIndex(COLUMN_PART_SEGMENT)));
                                existOrdersList.setOrderMode("CashBill");
                                existOrdersList.setExcuteid(executiveid);
                                existOrdersList.setDate(formattedDate);
                                existOrdersList.setGrandtotal(grandtotal);
//                                existOrdersList.setAddress("24-41, Ram Nagar, Velachery, Chennai, Tamil Nadu 600042, India Chennai-Tamil Nadu");
                                existOrdersList.setExecutivename(sharedPreferences.getString("KEY_Name", ""));
                                existOrdersLists.add(existOrdersList);
//                            int temptotal=Integer.valueOf(existOrdersList.getAmount());
//                            overalltotal+= temptotal;
                            }
                            while (curr.moveToNext());
                        }
                        curr.close();

                        db.closeDatabase();
//                        grandtotal=String.valueOf(overalltotal);
                        orderDetailsToPost.setExistOrdersList(existOrdersLists);
//                        progressDialog.dismiss();
                        return "Success";
                    }
                }
                catch (Exception e){
                    return e.getMessage();
                }
            return "Notsuccess";
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("Success")){

            }
        }
    }

    private void placeorder() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ConfirmOrder.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<OrderNumber> call = service.placeorder(orderDetailsToPost);
            call.enqueue(new Callback<OrderNumber>() {
                @Override
                public void onResponse(Call<OrderNumber> call, Response<OrderNumber> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        db.deleteTable();
                        getorderstatus();
                    } else if (response.body().getResult().equals("MobileNumberExist")){
                        progressDialog.dismiss();
                        getfailurealert("Please Select Another Distributer To Place Order");
//                        Toast.makeText(getApplicationContext(), "Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<OrderNumber> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert( t.getMessage());
//                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert( ex.getMessage());
        }
    }

    private void getorderstatus() {
        orderplaced=new AlertDialog.Builder(ConfirmOrder.this).create();
        View orderplacedshow= LayoutInflater.from(ConfirmOrder.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText("Your Order Placed Successfully");

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        orderplaced.setView(orderplacedshow);
        orderplaced.setCanceledOnTouchOutside(false);
        orderplaced.show();
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ConfirmOrder.this).create();
        View orderplacedshow= LayoutInflater.from(ConfirmOrder.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerterror.dismiss();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, Home.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}

