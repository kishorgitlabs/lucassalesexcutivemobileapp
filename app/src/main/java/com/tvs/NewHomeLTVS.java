package com.tvs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.Activity.Changepass;
import com.tvs.Activity.Home;
import com.tvs.Activity.Login;
import com.tvs.Activity.MainActivity;
import com.tvs.Edit.EditProfile;

import java.util.List;

import APIInterface.CategoryAPI;
import Model.executivedetails.GetExecutiveDetails;
import Model.orderresult.OrderNumber;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewHomeLTVS extends AppCompatActivity {

    private LinearLayout attendances,contentmanagement,sales,visitreport,expensereport,complaints;
    private AlertDialog salesoption;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView tv_name;
    private String name;
    private ImageView img_menu;
    private AlertDialog attendanceoption;
    private String manufacturer;
    private static final int IGNORE_BATTERY_OPTIMIZATION_REQUEST = 1002;
    private AlertDialog alerterror;
    private boolean autostart=true;
    private boolean executivedetails=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home_ltvs);
        tv_name = (TextView) findViewById(R.id.title_name);
        sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        name = sharedPreferences.getString("KEY_Name", " ");
        executivedetails=sharedPreferences.getBoolean("exedetailsupdated",true);
        tv_name.setText("Welcome  " +"Mr."+" "+ name);
        attendances=findViewById(R.id.attendances);
        contentmanagement=findViewById(R.id.contentmanagement);
        visitreport=findViewById(R.id.visitreport);
        sales=findViewById(R.id.sales);
        expensereport=findViewById(R.id.expensereport);
        complaints=findViewById(R.id.complaints);
        final ImageView img_menu = (ImageView)findViewById(R.id.setting);
        editor = sharedPreferences.edit();
        editor.putString("autostartpermission","notstarted");
        editor.commit();
        editor.apply();


        manufacturer = android.os.Build.MANUFACTURER;

        attendances.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    autostart=sharedPreferences.getBoolean("auto",true);
                if (manufacturer.equalsIgnoreCase("xiaomi")||manufacturer.equalsIgnoreCase("oppo")||manufacturer.equalsIgnoreCase("vivo")||manufacturer.equalsIgnoreCase("Letv")||manufacturer.equalsIgnoreCase("Honor")){
                    if (autostart){
                        editor.putBoolean("auto",false);
                        editor.commit();
                        editor.apply();
                        getautostartalert("Please enable the autostart option to the Lucas App to use the fully");
                    }else {
                        attendancealert();
                    }
                }
                else {
                    attendancealert();
                }
            }
        });

        expensereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StyleableToast st = new StyleableToast(NewHomeLTVS.this,
                        "This menu is under development", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            }
        });
        complaints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StyleableToast st = new StyleableToast(NewHomeLTVS.this,
                        "This menu is under development", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            }
        });
        contentmanagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contentmanagement=new Intent(getApplicationContext(), Home.class);
                startActivity(contentmanagement);
            }
        });
        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getorderstatus();
            }
        });

        img_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(NewHomeLTVS.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_editprofile:
                                startActivity(new Intent(NewHomeLTVS.this, EditProfile.class));
                                break;
                            case R.id.menu_changepassword:
                                startActivity(new Intent(NewHomeLTVS.this, Changepass.class));
                                break;
                            case R.id.menu_logout:
                                logout();
                                break;
                            case R.id.menu_exit:
                                Exit();
                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.settingmenu);
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    int version = pInfo.versionCode;
                    MenuItem appVersion=pop.getMenu().findItem(R.id.version);
                    appVersion.setTitle("App Version : "+version+".1"+"");
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                pop.show();
            }
        });

        visitreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StyleableToast st = new StyleableToast(NewHomeLTVS.this,
                        "This menu is under development", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();

            }
        });

        askIgnoreOptimization();

//        if (manufacturer.equalsIgnoreCase("xiaomi")||manufacturer.equalsIgnoreCase("oppo")||manufacturer.equalsIgnoreCase("vivo")||manufacturer.equalsIgnoreCase("Letv")||manufacturer.equalsIgnoreCase("Honor")){
//            if (autostart.equals("notstarted")){
//                getautostartalert("Please enable the autostart option to the Lucas App to use the fully");
//            }
//        }

        if (executivedetails){
        getsalesexedetails();
        }

    }
    private void getsalesexedetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(NewHomeLTVS.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetExecutiveDetails> call = service.getexecutivedetails(sharedPreferences.getString("KEY_Mobile_Number", ""));
            call.enqueue(new Callback<GetExecutiveDetails>() {
                @Override
                public void onResponse(Call<GetExecutiveDetails> call, Response<GetExecutiveDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        editor = sharedPreferences.edit();
                        editor.putString("KEY_Name",response.body().getData().get(0).getName());
                        editor.putString("KEY_log_User_Name", response.body().getData().get(0).getUsername());
                        editor.putString("KEY_User_Type", response.body().getData().get(0).getUsertype());
                        editor.putString("KEY_Mobile_Number", response.body().getData().get(0).getMobile());
                        editor.putString("KEY_Company_Name",response.body().getData().get(0).getCompanyName() );
                        editor.putString("KEY_Place", response.body().getData().get(0).getPlace());
                        editor.putString("Time", "In Time");
                        editor.putString("KEY_log_Password",response.body().getData().get(0).getPassword());
                        editor.putString("KEY_Email",response.body().getData().get(0).getEmailId());
                        editor.putBoolean("KEY_isLoggedin",true);
                        editor.putString("serviceenggnmae",response.body().getData().get(0).getName());
                        editor.putString("executiveid",response.body().getData().get(0).getId());
                        editor.putString("state",response.body().getData().get(0).getState());
                        editor.putString("city",response.body().getData().get(0).getCity());
                        editor.putBoolean("exedetailsupdated",false);
                        editor.commit();
                        editor.apply();
                        recreate();
                    } else if (response.body().getResult().equals("MobileNumberExist")){
                        progressDialog.dismiss();

//                        Toast.makeText(getApplicationContext(), "Please Try Again Later", Toast.LENGTH_LONG).show();
                    }else {
                        progressDialog.dismiss();
                        //for result error from json method

                    }
                }
                @Override
                public void onFailure(Call<GetExecutiveDetails> call, Throwable t) {
                    progressDialog.dismiss();

//                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    private void getautostartforapp() {
        try {
            Intent intent = new Intent();
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }else {

            }
            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                startActivity(intent);
                startActivityForResult(intent,100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void askIgnoreOptimization() {

//        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
//        String packageName = getPackageName();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Intent i = new Intent();
//            if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
//                i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                i.setData(Uri.parse("package:" + packageName));
//                startActivity(i);
//            } else if (manufacturer.equalsIgnoreCase("xiaomi") || manufacturer.equalsIgnoreCase("oppo") || manufacturer.equalsIgnoreCase("vivo") || manufacturer.equalsIgnoreCase("Letv") || manufacturer.equalsIgnoreCase("Honor")) {
//                if (autostart.equals("notstarted")) {
//                    getautostartalert("Please enable the autostart option to the Lucas App to use the fully");
//                }
//            }

//        if(Build.VERSION.SDK_INT>=23){
//            int permissionCheck= ContextCompat
//                    .checkSelfPermission(this, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//
//            if(permissionCheck == PackageManager.PERMISSION_DENIED){
//
//                //Should we show an explanation
//                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)){
//                    //Show an explanation
//                    final String message = "";
//                    Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                     intent.setData(Uri.parse("package:" + getPackageName()));
//                     startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
////                    Snackbar.make(coordinatorLayoutView,message,Snackbar.LENGTH_LONG)
////                            .setAction("GRANT", new View.OnClickListener() {
////                                @Override
////                                public void onClick(View v) {
////                                    ActivityCompat.requestPermissions(NewHomeLTVS.this, new String[]{ Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS }, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
////                                }
////                            })
////                            .show();
//
//                }else{
//                    //No explanation need,we can request the permission
//                    ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS }, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
//                }
//            }
//        }
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//            intent.setData(Uri.parse("package:" + getPackageName()));
//            startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            String packageName = getPackageName();
//            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
//                try {
//                    //some device doesn't has activity to handle this intent
//                    //so add try catch
//                    Intent intent = new Intent();
//                    intent.setAction(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//                    intent.setData(Uri.parse("package:" + packageName));
//                    startActivity(intent);
//                } catch (Exception e) {
//
//                }
//            }
//        }
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
//            intent.setData(Uri.parse("package:" + getPackageName()));
//            startActivityForResult(intent, IGNORE_BATTERY_OPTIMIZATION_REQUEST);
////        }
//
        Intent intent = new Intent();
        String packageName = getPackageName();
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (pm.isIgnoringBatteryOptimizations(packageName)) {

//                intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
//                intent.setData(Uri.parse("package:" + getPackageName()));
            } else {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }

        }
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode==1002&&resultCode==-1) {
//            if (manufacturer.equalsIgnoreCase("xiaomi")||manufacturer.equalsIgnoreCase("oppo")||manufacturer.equalsIgnoreCase("vivo")||manufacturer.equalsIgnoreCase("Letv")||manufacturer.equalsIgnoreCase("Honor")){
//                if (autostart.equals("notstarted")){
//                    getautostartalert("Please enable the autostart option to the Lucas App to use the fully");
//                }
//            }
//            else {
//
//            }
//        }
//        else {
//            askIgnoreOptimization();
//        }
    }

    private void getautostartalert(String alert) {
        alerterror=new AlertDialog.Builder(NewHomeLTVS.this).create();
        View orderplacedshow= LayoutInflater.from(NewHomeLTVS.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        alert_head.setText("Field Executive Application");
        ordernumber.setText(alert);
        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerterror.dismiss();
                getautostartforapp();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
    private void attendancealert() {
        attendanceoption=new AlertDialog.Builder(NewHomeLTVS.this).create();
        View orderplacedshow= LayoutInflater.from(NewHomeLTVS.this).inflate(R.layout.attendanceoptionalert,null);

        LinearLayout markattendance=orderplacedshow.findViewById(R.id.markattendance);
        LinearLayout viewattendacne=orderplacedshow.findViewById(R.id.viewattendacne);
        markattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent attendance=new Intent(getApplicationContext(),AttendanceSE.class);
                startActivity(attendance);
                attendanceoption.dismiss();

            }
        });
        viewattendacne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewattendance=new Intent(getApplicationContext(),ViewAttendance.class);
                startActivity(viewattendance);
                attendanceoption.dismiss();
            }
        });
        attendanceoption.setCanceledOnTouchOutside(false);
        attendanceoption.setView(orderplacedshow);
        attendanceoption.show();

    }

    public void logout() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(NewHomeLTVS.this);
        alertDialog.setTitle(name);
        alertDialog.setMessage("Are you sure want to Logout?");
        alertDialog.setIcon(R.drawable.close);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            editor = sharedPreferences.edit();
                            editor.putBoolean("KEY_isLoggedin", false);

                            editor.apply();
                            startActivity(new Intent(NewHomeLTVS.this, Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(NewHomeLTVS.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();

    }

    public void Exit() {
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);

//        startActivity(new Intent(this, Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(NewHomeLTVS.this);
        alertDialog.setTitle(name);
        alertDialog.setMessage("Are you sure want to Exit?");
        alertDialog.setIcon(R.drawable.close);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {

                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(NewHomeLTVS.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void getorderstatus() {
        salesoption=new AlertDialog.Builder(NewHomeLTVS.this).create();
        View orderplacedshow= LayoutInflater.from(NewHomeLTVS.this).inflate(R.layout.salesoption,null);
        final LinearLayout sales=orderplacedshow.findViewById(R.id.sales);
        LinearLayout viewsale=orderplacedshow.findViewById(R.id.viewsale);

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sales=new Intent(getApplicationContext(), SalesActivity.class);
                startActivity(sales);
                salesoption.dismiss();
            }
        });
        viewsale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewsales=new Intent(getApplicationContext(), ViewOrderSalesRep.class);
                startActivity(viewsales);
                salesoption.dismiss();
            }
        });
        salesoption.setView(orderplacedshow);
        salesoption.setCanceledOnTouchOutside(false);
        salesoption.show();
    }
}
