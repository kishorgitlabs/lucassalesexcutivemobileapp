package com.tvs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.Activity.Home;

import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import APIInterface.CategoryAPI;
import Adapter.OrderViewAdapter;
import Model.viewordersalesrep.ViewOrderSalesData;
import Model.viewordersalesrep.ViewOrderSalesRepJson;
import Model.viewsalesdatewise.OrderList;
import Model.viewsalesdatewise.ViewSalesDateWise;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderSalesRep extends AppCompatActivity  {

    private ListView orderview;
    private OrderViewAdapter orderViewAdapter;
    private List<Model.viewordersalesrep.OrderList> viewOrderSalesData;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String executiveid;
    private AlertDialog alerterror;
    private Button selectdate;
    private boolean mAutoHighlight;
    private  static  String FromDate = "",Todate = "";
    private List<OrderList> orderLists;
    private TextView totalamtmonth;
    private LinearLayout datechoosen,totalvalue;
    private static EditText fromdatee,todatee;
    private String frmdate,getdates;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order_sales_rep);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        executiveid=sharedPreferences.getString("executiveid","");
        orderview=findViewById(R.id.orderview);
        selectdate=findViewById(R.id.selectdate);
        totalamtmonth=findViewById(R.id.totalamtmonth);
        fromdatee=findViewById(R.id.fromdatee);
        todatee=findViewById(R.id.todatee);
        datechoosen=findViewById(R.id.datechoosen);
        totalvalue=findViewById(R.id.totalvalue);

        getorderdetails();

        fromdatee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });

        todatee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragmentto();
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });
        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FromDate.equals("")){
                    StyleableToast st = new StyleableToast(ViewOrderSalesRep.this,
                            "Select from date", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }else if (Todate.equals("")) {
                    StyleableToast st = new StyleableToast(ViewOrderSalesRep.this,
                            "Select to date", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    viewsalesdatewise();
                }

            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements android.app.DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
//            String mon=c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());

            android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
//            DatePickerDialog =   dialog.getDatePicker().setMaxDate(new Date().getTime());
//            // Create a new instance of DatePickerDialog and return it
//            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthselect=month+1;
            String monthselected = new DateFormatSymbols().getMonths()[month];
            String monthget=monthselected.substring(0, 3);
            fromdatee.setText(day+"-"+monthget +"-"+year);
            FromDate=year+"-"+monthselect+"-"+day;
        }
    }

    public static class DatePickerFragmentto extends DialogFragment
            implements android.app.DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
//            DatePickerDialog =   dialog.getDatePicker().setMaxDate(new Date().getTime());
//            // Create a new instance of DatePickerDialog and return it
//            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthselect=month+1;
            String monthselected = new DateFormatSymbols().getMonths()[month];
            String monthget=monthselected.substring(0, 3);
            todatee.setText(day+"-"+monthget +"-"+year);
            Todate=year+"-"+monthselect+"-"+day;
        }
    }



    private void getorderdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewOrderSalesRep.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON

            Call<ViewOrderSalesRepJson> call = service.vieworder(executiveid);
            call.enqueue(new Callback<ViewOrderSalesRepJson>() {
                @Override
                public void onResponse(Call<ViewOrderSalesRepJson> call, Response<ViewOrderSalesRepJson> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        viewOrderSalesData=response.body().getData().getOrderList();
                        orderViewAdapter=new OrderViewAdapter(ViewOrderSalesRep.this,viewOrderSalesData);
                        orderview.setAdapter(orderViewAdapter);
                        String total= response.body().getData().getTot();
                        if (total.contains(".")){
                            String spiltmrp[] = total.split("\\.");
                            String mrp =total.split("\\.")[0];
                            total=mrp;
                        }
                        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
                        int ordervalue= Integer.parseInt(total);
                        String moneyString = formatter.format(ordervalue);
                        totalamtmonth.setText(moneyString);
//                        totalamtmonth.setText(response.body().getData().getTot());
                    } else if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("You not placed any order");
                    }else {
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<ViewOrderSalesRepJson> call, Throwable t) {
                    progressDialog.dismiss();
                   getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception error to fetch data from server");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ViewOrderSalesRep.this).create();
        View orderplacedshow= LayoutInflater.from(ViewOrderSalesRep.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        ordernumber.setText(alert);
        alert_head.setText("View Sales Order");

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void viewsalesdatewise() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ViewOrderSalesRep.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<ViewSalesDateWise> call = service.viewsalesdatewise(FromDate,Todate,executiveid);
            call.enqueue(new Callback<ViewSalesDateWise>() {
                @Override
                public void onResponse(Call<ViewSalesDateWise> call, Response<ViewSalesDateWise> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        orderLists=response.body().getData().getOrderList();
                        totalvalue.setVisibility(View.VISIBLE);
                       String total= response.body().getData().getTot();
                        if (total.contains(".")){
                            String spiltmrp[] = total.split("\\.");
                            String mrp =total.split("\\.")[0];
                            total=mrp;
                        }
                        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
                        int ordervalue= Integer.parseInt(total);
                        String moneyString = formatter.format(ordervalue);
                        totalamtmonth.setText(moneyString);
                        orderViewAdapter=new OrderViewAdapter(ViewOrderSalesRep.this,orderLists, true);
                        orderview.setAdapter(orderViewAdapter);
                    } else if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("You Did Not Placed Any Order");
                    }else {
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<ViewSalesDateWise> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Faliure"+ " "+t.getMessage());
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Exception"+ " "+ex.getMessage());
        }
    }
}
