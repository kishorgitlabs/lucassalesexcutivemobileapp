package com.tvs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import APIInterface.CategoryAPI;
import Adapter.OrderPartsAdapter;
import Model.getpartnumber.GetPartNumber;
import Model.getpartnumberdetails.GetPartDetails;
import Model.ordermodel.Order_Parts;
import RetroClient.RetroClient;
import Shared.Config;
import dbhelper.PartDetailsDB;
import dbhelper.PartModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesParts extends AppCompatActivity implements OrderPartsAdapter.Recretatesipelistview {


    public SwipeMenuListView parts;
    private OrderPartsAdapter orderPartsAdapter;
    private List<String> partnumberlist,citylist;
    private AutoCompleteTextView partnumber;
    private AlertDialog alerterror;
    private Button addtocart,selectdistributer;
    private EditText qty,partdesc;
    private String selectedmrp,totalvalues="",segments,selectedpartnumber,selectedmobile,quantity,spiltmrp,partdescription;
    private List<Order_Parts> order_parts;
    private int totqty,totvalues;
    private PartDetailsDB db;
    private LinearLayout background;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String cart="",dealermobile,dealerusertype,shopname,totlvalue;
    private OrderPartsAdapter.Recretatesipelistview recretatesipelistview;
    private TextView totalcart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_parts);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        parts = findViewById(R.id.parts);
        cart=getIntent().getStringExtra("cart");
        db = new PartDetailsDB(this);
        partnumber = findViewById(R.id.partnumber);
        addtocart = findViewById(R.id.addtocart);
        qty = findViewById(R.id.qty);
        selectdistributer = findViewById(R.id.selectdistributer);
        selectedmobile = getIntent().getStringExtra("selectedmobile");
        background = findViewById(R.id.background);
        partdesc = findViewById(R.id.partdesc);
        totalcart=findViewById(R.id.totalcart);
        order_parts = db.getCartItem();
        orderPartsAdapter = new OrderPartsAdapter(SalesParts.this, order_parts);
        parts.setAdapter(orderPartsAdapter);
        totlvalue=db.gettotalordervalue();
        if (totlvalue.equals("")){
            totalcart.setText("₹"+" "+"0"+".00");
        }else {
            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            int ordervalue= Integer.parseInt(totlvalue);
            String moneyString = formatter.format(ordervalue);
            totalcart.setText(moneyString);
        }
        if (cart.equals("cart")){
            if (order_parts.isEmpty()) {
                StyleableToast st = new StyleableToast(SalesParts.this,
                        "No Items In Your Cart", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
                finish();
            }
            else {
//                dealerusertype=getIntent().getStringExtra("dealerusertype");
//                shopname=getIntent().getStringExtra("shopname");
//                dealermobile=getIntent().getStringExtra("selectedmobile");
                getpartnumber();
            }
        }else {
            dealerusertype=getIntent().getStringExtra("dealerusertype");
            shopname=getIntent().getStringExtra("shopname");
            dealermobile=getIntent().getStringExtra("selectedmobile");
            getpartnumber();
        }

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
               // set item width
                deleteItem.setWidth(70);
                // set a icon
                deleteItem.setIcon(R.drawable.delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        parts.setMenuCreator(creator);

        parts.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                Order_Parts item=order_parts.get(position);
                switch (index) {
                    case 0:
                        String partnumbertodelete=item.getPartNumber();
                        showDeleteAlertBox(partnumbertodelete);
                        break;
                }
                return false;
            }
        });

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                partdescription=partdesc.getText().toString();
                selectedpartnumber=partnumber.getText().toString();
                quantity=qty.getText().toString();
                if (selectedpartnumber==(null)||TextUtils.isEmpty(selectedpartnumber)){
                    StyleableToast st = new StyleableToast(SalesParts.this,
                            "Please enter partnumber", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (quantity==(null)||TextUtils.isEmpty(quantity)){
                    StyleableToast st = new StyleableToast(SalesParts.this,
                            "Please enter quantity", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (quantity.equals("0")){
                    StyleableToast st = new StyleableToast(SalesParts.this,
                            "Quantity cannot be zero", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else {
                    PartModel partModel=new PartModel();
                    PartDetailsDB db=new PartDetailsDB(SalesParts.this);
                    partModel.setPartNo(selectedpartnumber);
                    partModel.setQuantity(quantity);
                    partModel.setMrp(selectedmrp);
                    partModel.setTotalamt(totalvalues);
                    partModel.setDescription(partdescription);
                    partModel.setSegement("");
                    db.insertPartsData(partModel, selectedpartnumber, quantity,selectedmobile);
                    qty.setText("");
                    partnumber.setText("");
                    partdesc.setText("");
                    order_parts=db.getCartItem();
//                    orderPartsAdapter.setOrder_partss(order_parts);
                    orderPartsAdapter.notifyDataSetChanged();

                    recreate();
                }
            }
        });
        partnumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedpartnumber=partnumber.getAdapter().getItem(i).toString();
                getpartnumberdetails();
            }
        });

        qty.addTextChangedListener(new TextWatcher() {
                        @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()>0){
                    try {
                        quantity=editable.toString();
                        totqty=Integer.parseInt(quantity);
                        totvalues= Integer.parseInt(selectedmrp);
                        totalvalues= String.valueOf((totqty *totvalues));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        selectdistributer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (order_parts.isEmpty()) {
                    StyleableToast st = new StyleableToast(SalesParts.this,
                            "No Items In Your Cart", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    Intent selectdis=new Intent(getApplicationContext(),SelectDistributer.class);
                     selectdis.putExtra("dealermobile",dealermobile);
                     selectdis.putExtra("dealerusertype",dealerusertype);
                     selectdis.putExtra("shopname",shopname);
                    startActivity(selectdis);
                }
            }
        });
        }

    private void getpartnumber() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SalesParts.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetPartNumber> call = service.getpartnumber();
            call.enqueue(new Callback<GetPartNumber>() {
                @Override
                public void onResponse(Call<GetPartNumber> call, Response<GetPartNumber> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        partnumberlist=response.body().getData();
                        partnumber.setAdapter(new ArrayAdapter<String>(SalesParts.this,android.R.layout.simple_dropdown_item_1line,partnumberlist));
                        partnumber.setThreshold(1);
                    } else {
                        getfailurealert("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<GetPartNumber> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert(t.getMessage());
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert(ex.getMessage());
        }
    }

    private void getpartnumberdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SalesParts.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetPartDetails> call = service.getpartdetails(selectedpartnumber);
            call.enqueue(new Callback<GetPartDetails>() {
                @Override
                public void onResponse(Call<GetPartDetails> call, Response<GetPartDetails> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        selectedmrp=response.body().getData().getMrp();
                        if (selectedmrp.contains(".")){
                           String spiltmrp[] = selectedmrp.split("\\.");
                            String mrp =selectedmrp.split("\\.")[0];
                            selectedmrp=mrp;
                        }if (response.body().getData().getUnittype().equals("Child Parts")){
                            partdesc.setText(response.body().getData().getDescription());
                            qty.setText("1");
                        }else {
                            partdesc.setText(response.body().getData().getProductname());
                            qty.setText("1");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<GetPartDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(SalesParts.this).create();
        View orderplacedshow= LayoutInflater.from(SalesParts.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    private void showDeleteAlertBox(final String deletepartnumber)
    {
        final android.support.v7.app.AlertDialog alertDialog=new android.support.v7.app.AlertDialog.Builder(SalesParts.this).create();
        View deleteAlert=LayoutInflater.from(SalesParts.this).inflate(R.layout.cart_delete_alert,null);
        TextView yes=deleteAlert.findViewById(R.id.delete_yes);
        TextView no=deleteAlert.findViewById(R.id.delete_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PartDetailsDB partDetailsDB=new PartDetailsDB(SalesParts.this);
                boolean bool=partDetailsDB.deleteCartItems(deletepartnumber);
                order_parts=partDetailsDB.getCartItem();
//                orderPartsAdapter.setOrder_partss(order_parts);
                orderPartsAdapter.notifyDataSetChanged();
                recreate();
                alertDialog.dismiss();
                if(!bool)
                {
                    StyleableToast st = new StyleableToast(SalesParts.this,
                            "Please try again", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(SalesParts.this.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(deleteAlert);
        alertDialog.show();
    }
    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void recreted() {
        recreate();
    }
}
