package com.tvs;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.animatepolyline.RouteEvaluator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;




/**
 * Created by system01 on 10/26/2017.
 */

public class MapAnimatorHistory {
    static final int GREY = Color.parseColor("#FFA7A6A6");
    public static int markerCount;
    private static MapAnimatorHistory mapAnimator;
    private Polyline backgroundPolyline;
    private Polyline foregroundPolyline;
    private PolylineOptions optionsForeground;
    private AnimatorSet firstRunAnimSet;
    private AnimatorSet secondLoopRunAnimSet;
    private Marker mk;
    private LatLng source, destination;
    private GoogleMap googleMap;
    private String distance, duration;
    private Context context;
    private Marker mdesMarker;
    private MapAnimatorHistory() {

    }

    public static MapAnimatorHistory getInstance() {
        if (mapAnimator == null) mapAnimator = new MapAnimatorHistory();
        return mapAnimator;
    }


    public void animateRoute(final Context context, GoogleMap googleMap, List<LatLng> bangaloreRoute,
                             LatLng source, LatLng destination, String from, String to, List<LatLng> completeRoute) {
        this.source = source;
        this.destination = destination;
        this.googleMap = googleMap;
        this.context = context;

        if (firstRunAnimSet == null) {
            firstRunAnimSet = new AnimatorSet();
        } else {
            firstRunAnimSet.removeAllListeners();
            firstRunAnimSet.end();
            firstRunAnimSet.cancel();

            firstRunAnimSet = new AnimatorSet();
        }
        if (secondLoopRunAnimSet == null) {
            secondLoopRunAnimSet = new AnimatorSet();
        } else {
            secondLoopRunAnimSet.removeAllListeners();
            secondLoopRunAnimSet.end();
            secondLoopRunAnimSet.cancel();

            secondLoopRunAnimSet = new AnimatorSet();
        }
        //Reset the polylines
        if (foregroundPolyline != null) foregroundPolyline.remove();
        if (backgroundPolyline != null) backgroundPolyline.remove();

        if(bangaloreRoute.size()!=0) {
            PolylineOptions optionsBackground = new PolylineOptions().add(bangaloreRoute.get(0)).color(GREY).width(8);
            backgroundPolyline = googleMap.addPolyline(optionsBackground);
            optionsForeground = new PolylineOptions().add(bangaloreRoute.get(0)).color(Color.BLACK).width(8);
            foregroundPolyline = googleMap.addPolyline(optionsForeground);
        }
        BitmapDescriptor sourceicon = getBitmapDescriptor(R.drawable.ic_location);
        mk = googleMap.addMarker(new MarkerOptions()
                .position(source)
                .title("From")
                .snippet(from)
                .icon(sourceicon));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 13));

        BitmapDescriptor desicon = getBitmapDescriptor(R.drawable.info_map);
        mdesMarker = googleMap.addMarker(new MarkerOptions()
                .position(destination)
                .title("To")
                .snippet(to)
                .icon(desicon));
        mdesMarker.showInfoWindow();

        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(completeRoute);
        lineOptions.width(5);
        lineOptions.color(context.getResources().getColor(R.color.colorAccent));
        if(lineOptions != null) {
            googleMap.addPolyline(lineOptions);
        }

        final ValueAnimator percentageCompletion = ValueAnimator.ofInt(0, 100);
        percentageCompletion.setDuration(2000);
        percentageCompletion.setInterpolator(new DecelerateInterpolator());
        percentageCompletion.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                List<LatLng> foregroundPoints = backgroundPolyline.getPoints();

                int percentageValue = (int) animation.getAnimatedValue();
                int pointcount = foregroundPoints.size();
                int countTobeRemoved = (int) (pointcount * (percentageValue / 100.0f));
                List<LatLng> subListTobeRemoved = foregroundPoints.subList(0, countTobeRemoved);
                subListTobeRemoved.clear();

                foregroundPolyline.setPoints(foregroundPoints);
            }
        });

        percentageCompletion.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                foregroundPolyline.setColor(GREY);
                foregroundPolyline.setPoints(backgroundPolyline.getPoints());
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), GREY, Color.BLACK);
        colorAnimation.setInterpolator(new AccelerateInterpolator());
        colorAnimation.setDuration(1200); // milliseconds

        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                foregroundPolyline.setColor((int) animator.getAnimatedValue());
            }

        });
        if(bangaloreRoute.size()!=0) {
            ObjectAnimator foregroundRouteAnimator = ObjectAnimator.ofObject(this, "routeIncreaseForward", new RouteEvaluator(), bangaloreRoute.toArray());
            foregroundRouteAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            foregroundRouteAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    backgroundPolyline.setPoints(foregroundPolyline.getPoints());
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            foregroundRouteAnimator.setDuration(1600);
//        foregroundRouteAnimator.start();

            firstRunAnimSet.playSequentially(foregroundRouteAnimator,
                    percentageCompletion);
            firstRunAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    secondLoopRunAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            secondLoopRunAnimSet.playSequentially(colorAnimation,
                    percentageCompletion);
            secondLoopRunAnimSet.setStartDelay(200);

            secondLoopRunAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    secondLoopRunAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            firstRunAnimSet.start();
        }

    }


    private BitmapDescriptor getBitmapDescriptor(int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    /**
     * This will be invoked by the ObjectAnimator multiple times. Mostly every 16ms.
     **/
    public void setRouteIncreaseForward(LatLng endLatLng) {
        List<LatLng> foregroundPoints = foregroundPolyline.getPoints();
        foregroundPoints.add(endLatLng);
        foregroundPolyline.setPoints(foregroundPoints);
    }


}






