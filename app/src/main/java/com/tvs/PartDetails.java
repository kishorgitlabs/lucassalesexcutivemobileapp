package com.tvs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import APIInterface.CategoryAPI;
import Model.getpartnumberdetails.GetPartDetails;
import RetroClient.RetroClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PartDetails extends AppCompatActivity {

    private TextView partno,product,type,voltage,rating,model,oecust,application,engine,status,mrp;
    private String partnumber;
    private AlertDialog alerterror;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part_details);
        partno=findViewById(R.id.partno);
        product=findViewById(R.id.product);
        type=findViewById(R.id.type);
        voltage=findViewById(R.id.voltage);
        rating=findViewById(R.id.rating);
        model=findViewById(R.id.model);
        oecust=findViewById(R.id.oecust);
        application=findViewById(R.id.application);
        engine=findViewById(R.id.engine);
        status=findViewById(R.id.status);
        mrp=findViewById(R.id.mrp);
        partnumber=getIntent().getStringExtra("partnumber");
        getpartnumberdetails();
    }

    private void getpartnumberdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(PartDetails.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetPartDetails> call = service.getpartdetails(partnumber);
            call.enqueue(new Callback<GetPartDetails>() {
                @Override
                public void onResponse(Call<GetPartDetails> call, Response<GetPartDetails> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        partno.setText(partnumber);
                        type.setText(response.body().getData().getProType());
                        voltage.setText(response.body().getData().getPartVolt());
                        oecust.setText(response.body().getData().getOEname());
                        application.setText(response.body().getData().getAppname());
                        engine.setText(response.body().getData().getEnginType());
                        status.setText(response.body().getData().getProStatus());
                        mrp.setText("₹"+" "+response.body().getData().getMrp());
                        product.setText(response.body().getData().getProductname());
                        rating.setText(response.body().getData().getPartOutputrng());
                        model.setText(response.body().getData().getProModel());

                        if (response.body().getData().getUnittype().equals("Child Parts")){
                            product.setText(response.body().getData().getDescription());
                        }else {
                            product.setText(response.body().getData().getProductname());
                        }

                    } else {
                        getfailurealert("Please Try Again Later");

                    }
                }
                @Override
                public void onFailure(Call<GetPartDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert("Failed to fetch data from server");

            }
            });
        } catch (Exception ex) {
            getfailurealert("Exception to fetch data from server");
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(PartDetails.this).create();
        View orderplacedshow= LayoutInflater.from(PartDetails.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        ordernumber.setText(alert);
        alert_head.setText("Part Details");

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
