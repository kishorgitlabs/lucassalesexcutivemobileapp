package com.tvs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.tvs.Activity.Home;

import java.util.List;

import Adapter.ViewCartAdapter;
import Model.ordermodel.Order_Parts;
import dbhelper.PartDetailsDB;

public class ViewCart extends AppCompatActivity {


    private ListView cartdata;
    private ViewCartAdapter viewCartAdapter;
    private List<Order_Parts> order_parts;
    private Button selectdistributer;
    private PartDetailsDB db;
    private String dealermobile,dealerusertype,shopname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cart);
        db=new PartDetailsDB(this);
        cartdata=findViewById(R.id.cartdata);
        selectdistributer=findViewById(R.id.selectdistributer);
        order_parts=db.getCartItem();
        dealermobile=getIntent().getStringExtra("dealermobile");
        dealerusertype=getIntent().getStringExtra("dealerusertype");
        shopname=getIntent().getStringExtra("shopname");

        viewCartAdapter=new ViewCartAdapter( ViewCart.this,order_parts);
        cartdata.setAdapter(viewCartAdapter);

        selectdistributer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectdis=new Intent(getApplicationContext(),SelectDistributer.class);
                selectdis.putExtra("dealermobile",dealermobile);
                selectdis.putExtra("dealerusertype",dealerusertype);
                selectdis.putExtra("shopname",shopname);
                startActivity(selectdis);
            }
        });
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
