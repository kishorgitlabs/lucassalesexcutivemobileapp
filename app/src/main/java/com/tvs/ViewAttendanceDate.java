package com.tvs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tvs.Activity.Home;

import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.AttendanceDateWise;
import Adapter.ViewAttendanceAdapter;
import Model.viewattendance.ViewAttendanceJson;
import Model.viewattendancedatewise.ViewAttendanceData;
import Model.viewattendancedatewise.ViewAttendanceDateWiseDetails;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  ViewAttendanceDate extends AppCompatActivity {

    private RecyclerView attendancedetails;
    private String empid,traveldate,daytemonthyear,trackid,coordinatedate,dateformap;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private AlertDialog alerterror;
    private List<ViewAttendanceData> viewAttendanceData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance_date);

        attendancedetails=findViewById(R.id.attendancedetails);
        sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        traveldate=getIntent().getStringExtra("traveldate");
        trackid=getIntent().getStringExtra("trackids");
        String [] getdateonly=traveldate.split("T");
        String dateonly=getdateonly[0];
        String [] getdaymonthyear=dateonly.split("-");
        daytemonthyear=getdaymonthyear[0]+"-"+getdaymonthyear[1]+"-"+getdaymonthyear[2];
        coordinatedate=getdaymonthyear[2]+"-"+getdaymonthyear[1]+"-"+getdaymonthyear[0];

        empid=sharedPreferences.getString("executiveid", "");

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        attendancedetails.setLayoutManager(linearLayoutManager);


    gettraveldetails();

    }

    private void gettraveldetails() {
        try {

            final ProgressDialog progressDialog = new ProgressDialog(ViewAttendanceDate.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<ViewAttendanceDateWiseDetails> call = service.viewattendancedate(empid,daytemonthyear);
            call.enqueue(new Callback<ViewAttendanceDateWiseDetails>() {
                @Override
                public void onResponse(Call<ViewAttendanceDateWiseDetails> call, Response<ViewAttendanceDateWiseDetails> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        viewAttendanceData=response.body().getData();
                        AttendanceDateWise attendanceDateWise=new AttendanceDateWise(ViewAttendanceDate.this,viewAttendanceData,trackid,coordinatedate,daytemonthyear);
                        attendancedetails.setAdapter(attendanceDateWise);
                    } else  if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<ViewAttendanceDateWiseDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert(t.getMessage());
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert(ex.getMessage());
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(ViewAttendanceDate.this).create();
        View orderplacedshow= LayoutInflater.from(ViewAttendanceDate.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
}
