package com.tvs.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.tvs.AddDistributer;
import com.tvs.ConfirmOrder;
import com.tvs.Edit.EditProfile;
import com.tvs.NewHomeLTVS;
import com.tvs.OEDealer.Add_OEDealer;
import com.tvs.OEDealer.View_OEDealer;
import com.tvs.R;
import com.tvs.SalesActivity;
import com.tvs.Split.User_Type;
import com.tvs.ViewDistributor;
import com.tvs.ViewOrderSalesRep;

import APIInterface.CategoryAPI;
import Model.ChangePass.Changepassword;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity {
    TextView tv_name;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String name;
    String OLDPASS, NEWPASS, CONFIRMPASS;
    String User_Name, Password;
    Boolean isLoggedin = false;
    private AlertDialog salesoption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            tv_name = (TextView) findViewById(R.id.title_name);
            sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
            name = sharedPreferences.getString("KEY_Name", " ");
            tv_name.setText("WELCOME  " + name);
            sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);

            User_Name = sharedPreferences.getString("KEY_log_User_Name", " ");
            Password = sharedPreferences.getString("KEY_log_Password", " ");
        } catch (Exception ex) {
            ex.getMessage();
        }
//        final ImageView img_menu = (ImageView)findViewById(R.id.setting);

//        img_menu.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(Home.this, R.style.PopupMenu);
//                final PopupMenu pop = new PopupMenu(wrapper, v);
//                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {
//
//                    @Override
//                    public void onDismiss(PopupMenu arg0) {
//                        // TODO Auto-generated method stub
//                        pop.dismiss();
//                    }
//                });
//                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//
//                    public boolean onMenuItemClick(MenuItem item) {
//                        switch (item.getItemId()) {
//                            case R.id.menu_editprofile:
//                                startActivity(new Intent(Home.this, EditProfile.class));
//                                break;
//                            case R.id.menu_changepassword:
//                                startActivity(new Intent(Home.this, Changepass.class));
//                                break;
//                            case R.id.menu_logout:
//                                logout();
//                                break;
//                            case R.id.menu_exit:
//                                Exit();
//                                break;
//                        }
//                        return false;
//                    }
//                });
//                pop.inflate(R.menu.settingmenu);
//                try {
//                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                    int version = pInfo.versionCode;
//                    MenuItem appVersion=pop.getMenu().findItem(R.id.version);
//                    appVersion.setTitle("App Version : "+version+".0"+"");
//                } catch (PackageManager.NameNotFoundException e) {
//                    e.printStackTrace();
//                }
//                pop.show();
//            }
//        });

        getVersionCode();
    }

    public int getVersionCode() {
        int v = 0;
        try {
            v = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return v;
    }


    public void logout() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
        alertDialog.setTitle(name);
        alertDialog.setMessage("Are you sure want to Logout?");
        alertDialog.setIcon(R.drawable.close);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            editor = sharedPreferences.edit();
                            editor.putBoolean("KEY_isLoggedin", false);

                            editor.apply();
                            startActivity(new Intent(Home.this, Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(Home.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();

    }




    public void popup(View view) {
    }

    public void Changepswd(View view) {
        try {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                    Home.this).create();

            LayoutInflater inflater = (Home.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.changepassword, null);
            alertDialog.setView(dialogView);
            Button Cancel = (Button) dialogView.findViewById(R.id.ch_cancel);
            Button save = (Button) dialogView.findViewById(R.id.ch_save);
            final EditText old_passwd = (EditText) dialogView.findViewById(R.id.ed_oldPass);
            final EditText New_Password = (EditText) dialogView.findViewById(R.id.ed_newPass);
            final EditText Confirm_pass = (EditText) dialogView.findViewById(R.id.ed_confirmPass);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OLDPASS = old_passwd.getText().toString().trim();
                    NEWPASS = New_Password.getText().toString().trim();
                    CONFIRMPASS = Confirm_pass.getText().toString().trim();

                    if (OLDPASS.equals("")) {
                        old_passwd.setError("Please Enter Old Password");
                    } else if (NEWPASS.equals("")) {
                        New_Password.setError("Please Enter New Password");
                    } else if (CONFIRMPASS.equals("")) {
                        Confirm_pass.setError("Please Enter Confirm New Password");
                    } else if (!NEWPASS.equals(CONFIRMPASS)) {
                        Confirm_pass.setError("Please Check Confirm New Password");
                        Toast.makeText(Home.this, "Please Check NewPAssword", Toast.LENGTH_SHORT).show();
                    } else {
                        CHPSW();
//                    Toast.makeText(Home.this, "Your Password Changed", Toast.LENGTH_SHORT).show();

                        alertDialog.dismiss();
                    }
                }
            });
            Cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void CHPSW() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(Home.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            CategoryAPI service = RetroClient.getApiService();

            Call<Changepassword> call = service.ChangePASS(User_Name, OLDPASS, NEWPASS);
            call.enqueue(new Callback<Changepassword>() {
                @Override
                public void onResponse(Call<Changepassword> call, Response<Changepassword> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                Home.this).create();

                        LayoutInflater inflater = (Home.this).getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert, null);
                        alertDialog.setView(dialogView);


                        Button Ok = (Button) dialogView.findViewById(R.id.ok);


                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
                        Message.setText("Password has Changed Successfully");
                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(Home.this, Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                                alertDialog.dismiss();
                            }
                        });


                        alertDialog.show();
//                        Toast.makeText(Home.this, "Your Password Changed", Toast.LENGTH_SHORT).show();

                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(Home.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Changepassword> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void add(View view) {
        startActivity(new Intent(this, User_Type.class));
    }

    public void all(View view) {
        startActivity(new Intent(this, ViewAll.class));
    }

    public void Edit(View view) {
        startActivity(new Intent(this, EditProfile.class));
    }

    public void localview(View view) {
        startActivity(new Intent(this, LocalView.class));
    }

    public void addOE(View view) {
        startActivity(new Intent(this, Add_OEDealer.class));
    }

    public void allOE(View view) {
        startActivity(new Intent(this, View_OEDealer.class));
    }

      public void alldistributor(View view) {

        startActivity(new Intent(this, ViewDistributor.class));
    }

       public void adddistibutor(View view) {
        startActivity(new Intent(this, AddDistributer.class));
    }



    public void sales(View view){
        getorderstatus();

    }
    public void Exit() {
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);

//        startActivity(new Intent(this, Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
        alertDialog.setTitle(name);
        alertDialog.setMessage("Are you sure want to Exit?");
        alertDialog.setIcon(R.drawable.close);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        
                        try {

                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(Home.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }
    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getorderstatus() {
        salesoption=new AlertDialog.Builder(Home.this).create();
        View orderplacedshow= LayoutInflater.from(Home.this).inflate(R.layout.salesoption,null);
        TextView sales=orderplacedshow.findViewById(R.id.sales);
        TextView viewsale=orderplacedshow.findViewById(R.id.viewsale);

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sales=new Intent(getApplicationContext(), SalesActivity.class);
                startActivity(sales);
            }
        });
        viewsale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewsales=new Intent(getApplicationContext(), ViewOrderSalesRep.class);
                startActivity(viewsales);
            }
        });
        salesoption.setView(orderplacedshow);
        salesoption.setCanceledOnTouchOutside(false);
        salesoption.show();
    }
}
