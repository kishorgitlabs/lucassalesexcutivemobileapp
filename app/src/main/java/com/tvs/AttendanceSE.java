package com.tvs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.service.BGServicenormal;
import com.tvs.Activity.Home;
import com.tvs.Activity.MainActivity;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import APIInterface.CategoryAPI;
import Model.attendance.AttendanceDetails;
import Model.orderresult.OrderNumber;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.service.BGServicenormal.ACTION_START_FOREGROUND_SERVICE;

public class AttendanceSE extends AppCompatActivity {

    private boolean permissionGranted=false;
    private FusedLocationProviderClient mFusedLocationClient;
    private FusedLocationProviderApi api;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private Boolean mRequestingLocationUpdates;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 500;
    private String TAG="sehome";
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private TextView currentaddress,name,companyname,attendancemarked;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String locationaddress="";
    private Button attendancebtn;
    private AlertDialog attendance;
    private String daydetails,datedetails, dateandtime,postinandouttime,getattendanceinorout;
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    private AlertDialog alerterror;
    private Button attendanceinout;
    private String employename,employeeid,addressss,destination,regiid,inlatitude,inlogitude,outlatitude,outlongitude,employecode,attenday,distance,names;
    private ProgressDialog mProgressDialog;
    private int trytogetaddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_se);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        currentaddress=findViewById(R.id.address);
        companyname=findViewById(R.id.companyname);
        name=findViewById(R.id.name);
        attendancebtn=findViewById(R.id.attendancebtn);
        attendancemarked=findViewById(R.id.attendancemarked);
        employename=sharedPreferences.getString("KEY_Name","");
        employeeid=sharedPreferences.getString("executiveid", "");
        employecode=sharedPreferences.getString("KEY_log_User_Name", "");
        names=sharedPreferences.getString("serviceenggnmae", "");
        getattendanceinorout=sharedPreferences.getString("Time","");
        name.setText(employename);
        companyname.setText(employecode);
        mProgressDialog = ProgressDialog.show(AttendanceSE.this, "Location", "Getting Your Location", false, false);
        initPermission();
        init();
        startLocationUpdate();

        if (getattendanceinorout.equals("Out Time")){
            attendancebtn.setText("Mark Attendance Out");
        }else {
            attendancebtn.setText("Mark Attendance In");
        }
        attendancebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //date
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                datedetails = df.format(c.getTime());
                //Time
                Calendar cal = Calendar.getInstance();
                Date currentTime = cal.getTime();
                SimpleDateFormat dftime = new SimpleDateFormat("HH:mm a");
                dateandtime = dftime.format(currentTime);

                Calendar calp = Calendar.getInstance();
                Date currentTimep = cal.getTime();
                SimpleDateFormat dftimep = new SimpleDateFormat("HH:mm:ss");
                postinandouttime = dftimep.format(currentTimep);
                //Day
                SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
                Date date = new Date();
                daydetails = sdf_.format(date);
                getattendance();
            }
        });


        if (getattendanceinorout.equals("Out Time")){
            attendancemarked.setVisibility(View.VISIBLE);
            attendancemarked.setText(sharedPreferences.getString("datedetails","")+""+sharedPreferences.getString("daydetails","")+" "+sharedPreferences.getString("dateandtime","")+" "+"Marked as your in time");
        }else {
            attendancemarked.setVisibility(View.GONE);
        }
    }
    private void initPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            permissionGranted=true;
            init();
            startLocationUpdate();
        }
        else
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},9 );
//                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},9 );
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
//        if(PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(AttendanceSE.this, Manifest.permission.ACCESS_FINE_LOCATION))
//        {
//            ActivityCompat.requestPermissions(AttendanceSE.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
//            init();
//        }
//        else {
//
//        }
    }

//    @SuppressLint("LongLogTag")
//    @Override
//    public void onLocationChanged(Location location) {
//        if(mProgressDialog!=null)
//            if(mProgressDialog.isShowing())
//                mProgressDialog.dismiss();
//        // check_button.setVisibility(View.VISIBLE);
//        if (location != null) {
//            mCurrentLocation = location;
//            inlogitude=String.valueOf(mCurrentLocation.getLatitude());
//            inlatitude=String.valueOf(mCurrentLocation.getLongitude());
//            editor.putString("latitude", inlatitude.toString());
//            editor.putString("longitude", inlogitude.toString());
//            Log.d(TAG, "onLocationChanged: " + mCurrentLocation.getLatitude() + " long " + mCurrentLocation.getLongitude());
//            editor.commit();
//            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
//
//            //Or Do whatever you want with your location
//        } else if (mCurrentLocation != null) {
//            inlogitude=String.valueOf(mCurrentLocation.getLatitude());
//            inlatitude=String.valueOf(mCurrentLocation.getLongitude());
//            editor.putString("latitude", inlatitude.toString());
//            editor.putString("longitude", inlogitude.toString());
//            Log.d(TAG, "onLocationChanged: " + mCurrentLocation.getLatitude() + " long " + mCurrentLocation.getLongitude());
//            editor.commit();
//            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
//        }
//
//    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
                if(mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    inlogitude = String.valueOf(mCurrentLocation.getLongitude());
                    inlatitude = String.valueOf(mCurrentLocation.getLatitude());
                    new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    if (locationaddress.equals("")){
                        trytogetaddress++;
                        mProgressDialog.show();
                        if (trytogetaddress==3){
                            mProgressDialog.dismiss();
                            currentaddress.setText(inlogitude+" "+inlatitude);
                            attendancebtn.setVisibility(View.VISIBLE);
                        }
                    } else {
                        currentaddress.setText(locationaddress);
                    }
                }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==9&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
            init();
        }else {
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
            final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialogBuilder.setMessage("Please allow the permission to access your loacation");
            alertDialogBuilder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                           initPermission();
                        }
                    });
            alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    initPermission();
                }
            });
            alertDialogBuilder.show();
        }
    }



    private void init() {
        if(permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateLocationUI();
                }
            };
            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }
    private void startLocationUpdate() {

//        mProgressDialog = ProgressDialog.show(AttendanceSE.this, "Location", "Getting Your Location", false, false);
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

//                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mProgressDialog.show();
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());



//                        LocationManager manager= (LocationManager) getSystemService(LOCATION_SERVICE);
                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(AttendanceSE.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(AttendanceSE.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted=true;
                                init();
                        }
                        updateLocationUI();
                    }
                });
    }


    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {
        String errorMessage = "";
        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(AttendanceSE.this, Locale.getDefault());
            List<Address> addresses = null;

            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            }
//            else {
//                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
//            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {
            if (addresss == null) {
//                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
//                updateLocationUI();
            } else {
//                progressBar.setVisibility(View.GONE);
                String address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                String City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;

                locationaddress=address +
                        "\n"
                        + title;
                currentaddress.setText(locationaddress);
                if (locationaddress.equals("")){

                }else {
                    attendancebtn.setVisibility(View.VISIBLE);
                }
                editor.putString("FromAddress", address);
                editor.commit();

                Geocoder geocoder = new Geocoder(AttendanceSE.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void getattendance() {
        attendance=new AlertDialog.Builder(AttendanceSE.this).create();
        View attendancealert= LayoutInflater.from(AttendanceSE.this).inflate(R.layout.attendancealert,null);
        TextView timedetails=attendancealert.findViewById(R.id.timedetails);
        attendanceinout=attendancealert.findViewById(R.id.attendanceinout);
        final String getattendanceinorout=sharedPreferences.getString("Time","");

        if (getattendanceinorout.equals("Out Time")) {
            outlatitude = String.valueOf(mCurrentLocation.getLatitude());
            outlongitude = String.valueOf(mCurrentLocation.getLongitude());
            timedetails.setText(datedetails+" "+daydetails+" "+dateandtime+" "+"Marked as your out time");
            sendattendancedetailsforout();
        }else {
            sendattendancedetails();
            editor.putString("datedetails",datedetails);
            editor.putString("daydetails",daydetails);
            editor.putString("dateandtime",dateandtime);
            editor.apply();
            editor.commit();
            timedetails.setText(datedetails+" "+daydetails+" "+dateandtime+" "+"Marked as your in time");
        }
        attendanceinout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendance.dismiss();
            }
        });
        attendance.setView(attendancealert);
        attendance.setCanceledOnTouchOutside(false);
        attendance.show();
    }

    private void sendattendancedetailsforout() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AttendanceSE.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            Log.e("First","call");
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<AttendanceDetails> call = service.attendnace(employeeid,names,datedetails,postinandouttime,locationaddress,"SalesExecutive",locationaddress,"","",outlatitude,outlongitude,employecode,daydetails,"0");
            call.enqueue(new Callback<AttendanceDetails>() {
                @Override
                public void onResponse(Call<AttendanceDetails> call, Response<AttendanceDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        editor.putString("trackid",response.body().getData().getId());
                        editor.putString("Time", "In time");
                        editor.commit();
                        editor.apply();
                        recreate();
                        stopService(new Intent(AttendanceSE.this, BGServicenormal.class).setAction(ACTION_STOP_FOREGROUND_SERVICE));
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
//                        Toast.makeText(getApplicationContext(), "Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<AttendanceDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert( "Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Execption error please try again later");
        }
    }


    private void sendattendancedetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AttendanceSE.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<AttendanceDetails> call = service.attendnace(employeeid,names,datedetails,postinandouttime,locationaddress,"SalesExecutive","",inlatitude,inlogitude,"","",employecode,daydetails,"0");
            call.enqueue(new Callback<AttendanceDetails>() {
                @Override
                public void onResponse(Call<AttendanceDetails> call, Response<AttendanceDetails> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        editor.putString("trackid",response.body().getData().getId());
                        editor.putString("Time", "Out Time");
                        editor.commit();
                        editor.apply();
                        recreate();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            ContextCompat.startForegroundService(   AttendanceSE.this,new Intent(AttendanceSE.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                        }
                        else {
                            startService(new Intent(AttendanceSE.this, BGServicenormal.class).setAction(ACTION_START_FOREGROUND_SERVICE));
                        }
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("Please Try Again Later");
//                        Toast.makeText(getApplicationContext(), "Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<AttendanceDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert( "Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert("Execption error please try again later");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==100){
            startLocationUpdate();
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(AttendanceSE.this).create();
        View orderplacedshow= LayoutInflater.from(AttendanceSE.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerterror.dismiss();
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
