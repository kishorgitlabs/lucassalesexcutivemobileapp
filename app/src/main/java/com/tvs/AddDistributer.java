package com.tvs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.Activity.Home;

import java.util.List;

import APIInterface.CategoryAPI;
import Model.adddistributor.AddDistributor;
import Model.new_search.states.StateList;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDistributer extends AppCompatActivity {

    private String salesexestate,selectedcity,distributorcode,distribtorname,distributeremail,contactper,mobilenumber,distributeraddress;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private AlertDialog alerterror;
    private List<String> citylist;
    private ArrayAdapter cityadapter;
    private EditText citydis,discode,disname,disemail,discontper,mobiledis,disaddresss;
    private Button savedis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_distributer);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        salesexestate=sharedPreferences.getString("state", "");

        citydis=findViewById(R.id.citydis);
        savedis=findViewById(R.id.savedis);
        discode=findViewById(R.id.discode);
        disname=findViewById(R.id.disname);
        disemail=findViewById(R.id.disemail);
        discontper=findViewById(R.id.discontper);
        mobiledis=findViewById(R.id.mobiledis);
        disaddresss=findViewById(R.id.disaddresss);
        getcitystatewise(salesexestate);

        citydis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupcity(citylist);
            }
        });

        savedis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distributorcode=discode.getText().toString();
                distribtorname=disname.getText().toString();
                distributeremail=disemail.getText().toString();
                contactper=discontper.getText().toString();
                mobilenumber=mobiledis.getText().toString();
                distributeraddress=disaddresss.getText().toString();

                if (selectedcity==(null)|| TextUtils.isEmpty(selectedcity)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please select distributor city", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (distributorcode==(null)|| TextUtils.isEmpty(distributorcode)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter distributor code", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (distribtorname==(null)||TextUtils.isEmpty(distribtorname)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter distributor name", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }

                else if (distributeremail==(null)||TextUtils.isEmpty(distributeremail)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter distributor email", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else if (!checkmail(distributeremail)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter valid email id", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (mobilenumber==(null)||TextUtils.isEmpty(mobilenumber)) {
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter distributor mobile number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                  else if (mobilenumber.length()!=10){
                        StyleableToast st = new StyleableToast(AddDistributer.this,
                                "Please enter distributor valid mobile number", Toast.LENGTH_SHORT);
                        st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                        st.setTextColor(Color.WHITE);
                        st.setMaxAlpha();
                        st.show();

                }else if (distributeraddress==(null)||TextUtils.isEmpty(distributeraddress)){
                    StyleableToast st = new StyleableToast(AddDistributer.this,
                            "Please enter distributor address", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    adddistributer();
                }

            }
        });
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target))
        {
            return false;
        }
        else
        {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private boolean checkmail(String useremail)
    {

        if (useremail.contains("@")) {
            String[] email = useremail.split("@");
            String emailexte = email[1];
            if (emailexte.contains(".com")||(emailexte.contains(".co.in"))||emailexte.contains(".info")||emailexte.contains(".in")) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void adddistributer() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AddDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<AddDistributor> call = service.adddis(distributorcode,distribtorname,distributeremail,mobilenumber,distributeraddress,selectedcity,contactper);
            call.enqueue(new Callback<AddDistributor>() {
                @Override
                public void onResponse(Call<AddDistributor> call, Response<AddDistributor> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        getfailurealert("Distributor Added Successfully");

                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("Distributor code already exists");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<AddDistributor> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert("Execption error please try again later");

        }
    }

    public void getcitystatewise(String state) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AddDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON

            Call<StateList> call = service.getSateCity(state);
            Log.e("Ec",state);
            call.enqueue(new Callback<StateList>() {
                @Override
                public void onResponse(Call<StateList> call, Response<StateList> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();

                        citylist=response.body().getData();
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No city found in your state");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<StateList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert("Execption error please try again later");

        }
    }

    private void showPopupcity(final List<String> citylist) {
        try {
            final android.app.AlertDialog alertDialog;

            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);
            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
            spinerTitle.setText("Select City");
            cityadapter=new ArrayAdapter(AddDistributer.this, android.R.layout.simple_dropdown_item_1line, citylist);
            list.setAdapter(cityadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    citydis.setText(selectedcity);
                    alertDialog.dismiss();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(AddDistributer.this).create();
        View orderplacedshow= LayoutInflater.from(AddDistributer.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        ordernumber.setText(alert);
        alert_head.setText("Add Distributor");
        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
