package com.tvs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tvs.Activity.Home;

import java.util.List;

import APIInterface.CategoryAPI;
import Adapter.AttendanceDateWise;
import Adapter.CooridnatesAdapter;
import Model.cooridnates.CoordinatesData;
import Model.cooridnates.CoordinatesView;
import Model.viewattendancedatewise.ViewAttendanceDateWiseDetails;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Coordinates extends AppCompatActivity {


    private RecyclerView coordinatesview;
    private String trackid,trackdate,empid;
    private AlertDialog alerterror;
    private List<CoordinatesData> coordinatesData;
    private CooridnatesAdapter cooridnatesAdapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinates);

        sharedPreferences = this.getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        empid=sharedPreferences.getString("executiveid", "");
        coordinatesview=findViewById(R.id.coordinatesview);
        trackid=getIntent().getStringExtra("trackid");
        trackdate=getIntent().getStringExtra("trackdate");
        getcoorinates();

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        coordinatesview.setLayoutManager(linearLayoutManager);
    }

    private void getcoorinates() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(Coordinates.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<CoordinatesView> call = service.getcoordinates(empid,trackdate,trackid);
            call.enqueue(new Callback<CoordinatesView>() {
                @Override
                public void onResponse(Call<CoordinatesView> call, Response<CoordinatesView> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        coordinatesData=response.body().getData();
                        cooridnatesAdapter=new CooridnatesAdapter(Coordinates.this,coordinatesData);
                        coordinatesview.setAdapter(cooridnatesAdapter);
                    } else  if (response.body().getResult().equals("NotSuccess")){
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<CoordinatesView> call, Throwable t) {
                    progressDialog.dismiss();
                    getfailurealert(t.getMessage());
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
            getfailurealert(ex.getMessage());
        }
    }

    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(Coordinates.this).create();
        View orderplacedshow= LayoutInflater.from(Coordinates.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS .class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }
}
