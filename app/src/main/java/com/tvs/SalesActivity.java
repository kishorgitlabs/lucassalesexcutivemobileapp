package com.tvs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import APIInterface.CategoryAPI;
import Model.customerdetailsforname.CustomerDetailsName;
import Model.customerlistcitybased.GetCustomerListCityWise;
import Model.getpartnumber.GetPartNumber;
import Model.getpartnumberdetails.GetPartDetails;
import Model.new_search.states.StateList;
import RetroClient.RetroClient;
import Shared.Config;
import dbhelper.DbHelper;
import dbhelper.PartDetailsDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesActivity extends AppCompatActivity {


    private Spinner usertypeorder;
    private AutoCompleteTextView partnumber,city,customerlist;
    private Button processed;
    private String selectedusertype,cartmobile="",selectedmobile,selectedpartnumber,quantity,totalvalues,selectedmrp,segments,salesexestate,selectedcity,selectedcustomer;
    private TextView contactpersontext,emailidtext,addresstext,gsttext,paroductinfo,mrpdetails,shopname,mobiletext;
    private List<String> usermobilelist,partnumberlist,citylist,customernamelist;
    private DbHelper dbHelper;
    private EditText qty,remark;
    private int totqty,totvalues;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private AlertDialog alerterror;
    private PartDetailsDB db;
    private ImageView cart;
    private ArrayAdapter customerlistadapter,cityadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
//        partnumber=findViewById(R.id.partnumber);
        mobiletext=findViewById(R.id.mobiletext);
        processed=findViewById(R.id.processed);
        salesexestate=sharedPreferences.getString("state", "");
        emailidtext=findViewById(R.id.emailidtext);
        addresstext=findViewById(R.id.addresstext);
        city=findViewById(R.id.city);
        gsttext=findViewById(R.id.gsttext);
        customerlist=findViewById(R.id.customerlist);
        mobiletext=findViewById(R.id.mobiletext);
//        shopname=findViewById(R.id.shopname);
        cart=findViewById(R.id.cart);
        db=new PartDetailsDB(SalesActivity.this);
        cartmobile=db.getcartmobile();
        String [] usertypes ={"Select User type","RETAILER","MECHANIC","ELECTRICIAN"};
        customernamelist=new ArrayList<>();
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerlist.setText("");
                emailidtext.setText("");
                addresstext.setText("");
                gsttext.setText("");
//                shopname.setText("");
                selectedusertype="";
                mobiletext.setText("");
                showPopupcity(citylist);
            }
        });
//        city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//               selectedcity=city.getAdapter().getItem(i).toString();
//
//                showPopupcity(citylist);
//            }
//        });

        customerlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customernamelist.size()==0){
                    StyleableToast st = new StyleableToast(SalesActivity.this,
                            "No customer found in selected city", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
//                    spinnerDialog.showSpinerDialog();
                    showPopup(customernamelist);
                }

            }
        });


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cart=new Intent(getApplicationContext(),SalesParts.class);
                cart.putExtra("cart","cart");
                startActivity(cart);
            }
        });

        processed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedcity==(null)|| TextUtils.isEmpty(selectedcity)){
                    StyleableToast st = new StyleableToast(SalesActivity.this,
                            "Please select company city", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (selectedcustomer==(null)||TextUtils.isEmpty(selectedcustomer)){
                    StyleableToast st = new StyleableToast(SalesActivity.this,
                            "Please select company name", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (cartmobile.equals(selectedmobile)||db.getcartmobile().equals("")){
                    Intent customerdetails=new Intent(getApplicationContext(),SalesParts.class);
                    customerdetails.putExtra("selectedmobile",selectedmobile);
                    customerdetails.putExtra("cart","");
                    customerdetails.putExtra("dealername",selectedcustomer);
//                    customerdetails.putExtra("shopname",shopname.getText().toString());
                    customerdetails.putExtra("dealerusertype",selectedusertype);
                    startActivity(customerdetails);
                    customerlist.setText("");
                    city.setText("");
                    emailidtext.setText("");
                    addresstext.setText("");
                    gsttext.setText("");
                    selectedusertype="";
                    mobiletext.setText("");
                    }
                else {
                    StyleableToast st = new StyleableToast(SalesActivity.this,
                            "You Have Already Added Parts For Another User Please Place Order Or Delete", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.logocolor));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }

            }
        });
        getcitystatewise(salesexestate);
    }

    private void showPopup(List<String> customernamemobile) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);

            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
//            final EditText searchBox = (EditText) dialogView.findViewById(R.id.searchBox);
//            final AutocompleteAdapter adapter = new AutocompleteAdapter(SalesActivity.this, R.layout.simple_spinner_item, items);
            spinerTitle.setText("Select Customer");
            customerlistadapter =new ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,customernamelist);
            list.setAdapter(customerlistadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    customerlist.setText(selectedcity);
                    alertDialog.dismiss();
                    if (selectedcity==(null)||selectedcity.equals("")){

                    }else {
                        String namesplit[] =selectedcity.split("-");
                        selectedcustomer=namesplit[0].trim();
                        selectedmobile=namesplit[1].trim();
                        mobiletext.setText(selectedmobile);
                        getcustomerdetails();
                    }
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void showPopupcity(List<String> citylist) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);

            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
//            final EditText searchBox = (EditText) dialogView.findViewById(R.id.searchBox);
//            final AutocompleteAdapter adapter = new AutocompleteAdapter(SalesActivity.this, R.layout.simple_spinner_item, items);
            spinerTitle.setText("Select City");
            cityadapter=new ArrayAdapter(SalesActivity.this, android.R.layout.simple_dropdown_item_1line, citylist);
            list.setAdapter(cityadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    city.setText(selectedcity);

                    if (selectedcity==(null)||selectedcity.equals("")){

                    }
                    else {
                        getcustomerforcity();
                    }

                    alertDialog.dismiss();
//
//                    else {
////                        getcustomerforcity();
////                        String namesplit[] =selectedcity.split("-");
////                        selectedcustomer=namesplit[0].trim();
////                        selectedmobile=namesplit[1].trim();
////                        getcustomerdetails();
//                    }
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getcustomerforcity() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SalesActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetCustomerListCityWise> call = service.getcustomername(selectedcity);
            call.enqueue(new Callback<GetCustomerListCityWise>() {
                @Override
                public void onResponse(Call<GetCustomerListCityWise> call, Response<GetCustomerListCityWise> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        customernamelist=response.body().getData();
//                        ArrayAdapter customerlistadapter=new ArrayAdapter(SalesActivity.this, android.R.layout.simple_dropdown_item_1line, customernamelist);
//                        customerlist.setAdapter(customerlistadapter);
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No city found in your state");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<GetCustomerListCityWise> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert(t.getMessage());
//                    Toast.makeText(SelectDistributer.this, "Server Problem", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert(ex.getMessage());
//            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void getcitystatewise(String state) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SalesActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();
            // Calling JSON
            Call<StateList> call = service.getSateCity(state);
            call.enqueue(new Callback<StateList>() {
                @Override
                public void onResponse(Call<StateList> call, Response<StateList> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        citylist=response.body().getData();
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No city found in your state");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<StateList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert(t.getMessage());
//                    Toast.makeText(SelectDistributer.this, "Server Problem", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert(ex.getMessage());
//            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getpartnumberdetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(SalesActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetPartDetails> call = service.getpartdetails(selectedpartnumber);
            call.enqueue(new Callback<GetPartDetails>() {
                @Override
                public void onResponse(Call<GetPartDetails> call, Response<GetPartDetails> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
//                        paroductinfo.setText(response.body().getData().getSegments()+" "+response.body().getData().getCategory());
                        mrpdetails.setText(response.body().getData().getMrp());
                        selectedmrp=response.body().getData().getMrp();
//                        segments=response.body().getData().getSegments();
                        qty.setText("1");
                    } else {
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<GetPartDetails> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void getpartnumber() {
        try {

            final ProgressDialog progressDialog = new ProgressDialog(SalesActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<GetPartNumber> call = service.getpartnumber();
            call.enqueue(new Callback<GetPartNumber>() {
                @Override
                public void onResponse(Call<GetPartNumber> call, Response<GetPartNumber> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        partnumberlist=response.body().getData();
                        partnumber.setAdapter(new ArrayAdapter<String>(SalesActivity.this,android.R.layout.simple_dropdown_item_1line,partnumberlist));
                        partnumber.setThreshold(1);
                    } else {
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<GetPartNumber> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void getcustomerdetails() {
        try {

            final ProgressDialog progressDialog = new ProgressDialog(SalesActivity.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<CustomerDetailsName> call = service.customerdetails(selectedcustomer,selectedmobile);
            call.enqueue(new Callback<CustomerDetailsName>() {
                @Override
                public void onResponse(Call<CustomerDetailsName> call, Response<CustomerDetailsName> response) {
                    if (response.body().getResult().equals("Success")) {
                        progressDialog.dismiss();
                        emailidtext.setText(response.body().getData().getEmail());
                        addresstext.setText(response.body().getData().getArea());
                        gsttext.setText(response.body().getData().getGSTNumber());
//                        shopname.setText(response.body().getData().getShopName());
                        selectedusertype=response.body().getData().getType();

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Please Try Again Later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<CustomerDetailsName> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }

    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(SalesActivity.this).create();
        View orderplacedshow= LayoutInflater.from(SalesActivity.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        ordernumber.setText(alert);

        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), NewHomeLTVS.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
