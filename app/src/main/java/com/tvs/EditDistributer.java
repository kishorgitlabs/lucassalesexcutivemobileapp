package com.tvs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.tvs.Activity.Home;

import java.util.List;

import APIInterface.CategoryAPI;
import Model.adddistributor.AddDistributor;
import Model.new_search.states.StateList;
import RetroClient.RetroClient;
import Shared.Config;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDistributer extends AppCompatActivity {

    private List<String> citylist;
    private ArrayAdapter cityadapter;
    private String salesexestate,selectedcity;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private EditText citydis,discode,disname,disemail,discontper,mobiledis,disaddresss;
    private AlertDialog alerterror;

    private Button update;
    private String distributorcode,distribtorname,distributeremail,contactper,mobilenumber,distributeraddress,discity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_distributer);

        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_Userupdate, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        salesexestate=sharedPreferences.getString("state", "");
        citydis=findViewById(R.id.citydis);
        update=findViewById(R.id.update);
        discode=findViewById(R.id.discode);
        disname=findViewById(R.id.disname);
        disemail=findViewById(R.id.disemail);
        discontper=findViewById(R.id.discontper);
        mobiledis=findViewById(R.id.mobiledis);
        disaddresss=findViewById(R.id.disaddresss);

        discity=getIntent().getStringExtra("city");
        distributorcode=getIntent().getStringExtra("discode");
        distribtorname=getIntent().getStringExtra("disname");
        distributeremail=getIntent().getStringExtra("disemail");
        contactper=getIntent().getStringExtra("discontac");
        mobilenumber=getIntent().getStringExtra("dismobile");
        distributeraddress=getIntent().getStringExtra("disaddress");

        citydis.setText(discity);
        discode.setText(distributorcode);
        disname.setText(distribtorname);
        disemail.setText(distributeremail);
        discontper.setText(contactper);
        mobiledis.setText(mobilenumber);
        disaddresss.setText(distributeraddress);

        citydis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupcity(citylist);
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discity=citydis.getText().toString();
                distributorcode=discode.getText().toString();
                distribtorname=disname.getText().toString();
                distributeremail=disemail.getText().toString();
                contactper=discontper.getText().toString();
                mobilenumber=mobiledis.getText().toString();
                distributeraddress=disaddresss.getText().toString();
                adddistributer();
            }
        });
        getcitystatewise(salesexestate);
    }

    private void adddistributer() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(EditDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service = RetroClient.getApiServicesales();
            // Calling JSON
            Call<AddDistributor> call = service.adddis(distributorcode,distribtorname,distributeremail,mobilenumber,distributeraddress,discity,contactper);
            call.enqueue(new Callback<AddDistributor>() {
                @Override
                public void onResponse(Call<AddDistributor> call, Response<AddDistributor> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        getfailurealert("Distributor details updated successfully");

                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("Distributor code already exists");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<AddDistributor> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert("Failed to fetch data from server");
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert("Execption error please try again later");

        }
    }
    public void getcitystatewise(String state) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(EditDistributer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI service =RetroClient.getApiService();
            // Calling JSON
            Call<StateList> call = service.getSateCity(state);
            call.enqueue(new Callback<StateList>() {
                @Override
                public void onResponse(Call<StateList> call, Response<StateList> response) {
                    //Dismiss Dialog
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        citylist=response.body().getData();
                    } else if (response.body().getResult().equals("NotSuccess")){
                        progressDialog.dismiss();
                        getfailurealert("No city found in your state");
                    }else {
                        progressDialog.dismiss();
                        getfailurealert("Please try again later");
                    }
                }
                @Override
                public void onFailure(Call<StateList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    t.getMessage();
                    getfailurealert(t.getMessage());
                }
            });
        } catch (Exception ex) {
            Log.v("Error", ex.getMessage());
            ex.printStackTrace();
            getfailurealert(ex.getMessage());

        }
    }
    private void showPopupcity(List<String> citylist) {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);
            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            final  TextView close=dialogView.findViewById(R.id.close);
            final  TextView spinerTitle=dialogView.findViewById(R.id.spinerTitle);
            spinerTitle.setText("Select City");
            cityadapter=new ArrayAdapter(EditDistributer.this, android.R.layout.simple_dropdown_item_1line, citylist);
            list.setAdapter(cityadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedcity = list.getItemAtPosition(i).toString();
                    citydis.setText(selectedcity);
                    alertDialog.dismiss();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getfailurealert(String alert) {
        alerterror=new AlertDialog.Builder(EditDistributer.this).create();
        View orderplacedshow= LayoutInflater.from(EditDistributer.this).inflate(R.layout.orderplaced,null);
        TextView ordernumber=orderplacedshow.findViewById(R.id.ordernumber);
        TextView okcart=orderplacedshow.findViewById(R.id.okcart);
        TextView alert_head=orderplacedshow.findViewById(R.id.alert_head);
        ordernumber.setText(alert);
        alert_head.setText("Edit Distributor");
        okcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        alerterror.setView(orderplacedshow);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }

    private void distributorinfo() {
        alerterror=new AlertDialog.Builder(EditDistributer.this).create();
        View distributordetails= LayoutInflater.from(EditDistributer.this).inflate(R.layout.distributeralertview,null);


        alerterror.setView(distributordetails);
        alerterror.setCanceledOnTouchOutside(false);
        alerterror.show();
    }


    public void Home(View view) {
        startActivity(new Intent(this, NewHomeLTVS.class));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
