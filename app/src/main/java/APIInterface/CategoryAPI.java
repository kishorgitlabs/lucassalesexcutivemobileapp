package APIInterface;

import Model.AddOEdealer.RegOEdealer;
import Model.ChangePass.Changepassword;
import Model.EditPageModel.UpdateModel;
import Model.EditProfileModel.Update;
import Model.FilterData.Filter;
import Model.Forgot.Forgot;
import Model.LoginRegistermodel.Result;
import Model.Predictive.Citypredictive;
import Model.Predictive.StatePridictive;
import Model.Register.Register;
import Model.ViewAllData.View;
import Model.ViewOEdealer.ViewOE;
import Model.ViewSearchOEdealer.FilterOe;
import Model.adddistributor.AddDistributor;
import Model.attendance.AttendanceDetails;
import Model.attendancecoordination.AttendanceCoordinationMap;
import Model.attendancedatewise.Attedancedatewiseview;
import Model.backgroundattendance.BackGroundServiceJson;
import Model.cooridnates.CoordinatesView;
import Model.customerdetails.CustomerDetails;
import Model.customerdetailsforname.CustomerDetailsName;
import Model.customerlistcitybased.GetCustomerListCityWise;
import Model.distributerdetails.DistributerDetails;
import Model.executivedetails.GetExecutiveDetails;
import Model.getpartnumber.GetPartNumber;
import Model.getpartnumberdetails.GetPartDetails;
import Model.new_search.states.StateList;
import Model.orderdetailstosend.OrderDetailsToPost;
import Model.orderdetailsview.ViewOrderDetailsFully;
import Model.orderresult.OrderNumber;
import Model.regionbasedstatelist.RegionBasedStateList;
import Model.statebasedcitylist.StateBasedCityList;
import Model.usertypewisecustomerlist.CustomerMobileList;
import Model.viewattendance.ViewAttendanceJson;
import Model.viewattendancedatewise.ViewAttendanceDateWiseDetails;
import Model.viewordersalesrep.ViewOrderSalesRepJson;
import Model.viewsalesdatewise.ViewSalesDateWise;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by system9 on 6/30/2017.
 */

public interface CategoryAPI {
    @FormUrlEncoded
    @POST("UpdateSalesExecutive")
    Call<Result> login_register(
            @Field("Name") String Name,
            @Field("Username") String Code,
            @Field("Mobile") String Mobile_Number,
            @Field("CompanyName") String Company_Name,
            @Field("EmailId") String EmailId,
            @Field("Region") String region,
            @Field("State") String city,
            @Field("City") String state
    );

    @FormUrlEncoded
    @POST("LoginSales")
    Call<Model.Login.Result> login(
            @Field("Username") String User_Name,
            @Field("Password") String Password
    );

    @FormUrlEncoded
    @POST("getForgotPassword")
    Call<Forgot> forgot(
            @Field("EmailId") String EmailId

    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_ret(
            @Field("UserName") String UserName,
            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("DealsWithOEBrand") String DealsWith_OEBrand,
            @Field("LucasTVSPurchaseDealsWith") String LucasTVSPurchaseDealsWith,
            @Field("LucasTVSPurchaseDealsWithOther") String LucasTVSPurchaseDealsWith_Other,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther,
            @Field("DealsWithOEBrandOthers") String DealsWithOEBrandOthers

    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_mech(
            @Field("UserName") String UserName,

            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("MonthyTurnOver") String MonthyTurnOver,

            @Field("SpecialistIn") String SpecialistIn,
            @Field("MaintainingStock") String MaintainingStock,
            @Field("VehicleAlterMonth") String VehicleAlterMonth,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("SpecialistInOther") String SpecialistInOther,
            @Field("CaptureLocation") String CaptureLocation
    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_elect(
            @Field("UserName") String UserName,

            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,

            @Field("NoOfStarterMotorServicedInMonth") String NoOfStarterMotorServicedInMonth,
            @Field("NoOfAlternatorServicedInMonth") String NoOfAlternatorServicedInMonth,
            @Field("NoOfWiperMotorSevicedInMonth") String NoOfWiperMotorSevicedInMonth,
            @Field("NoOfStaff") String NoOfStaff,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther

    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_ret_mech(
            @Field("UserName") String UserName,

            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("DealsWithOEBrand") String DealsWith_OEBrand,
            @Field("LucasTVSPurchaseDealsWith") String LucasTVSPurchaseDealsWith,
            @Field("LucasTVSPurchaseDealsWithOther") String LucasTVSPurchaseDealsWith_Other,

            @Field("SpecialistIn") String SpecialistIn,
            @Field("MaintainingStock") String MaintainingStock,
            @Field("VehicleAlterMonth") String VehicleAlterMonth,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("SpecialistInOther") String SpecialistInOther,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther,
            @Field("DealsWithOEBrandOthers") String DealsWithOEBrandOthers

    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_mech_elect(
            @Field("UserName") String UserName,

            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("NoOfStarterMotorServicedInMonth") String NoOfStarterMotorServicedInMonth,
            @Field("ProductDealsWith") String mech_elect_pros_dels,
            @Field("NoOfAlternatorServicedInMonth") String NoOfAlternatorServicedInMonth,
            @Field("NoOfWiperMotorSevicedInMonth") String NoOfWiperMotorSevicedInMonth,
            @Field("NoOfStaff") String NoOfStaff,
            @Field("SpecialistIn") String SpecialistIn,
            @Field("MaintainingStock") String MaintainingStock,
            @Field("VehicleAlterMonth") String VehicleAlterMonth,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("SpecialistInOther") String SpecialistInOther,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther

    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_ret_elect(
            @Field("UserName") String UserName,
            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("DealsWithOEBrand") String DealsWith_OEBrand,
            @Field("LucasTVSPurchaseDealsWith") String LucasTVSPurchaseDealsWith,
            @Field("LucasTVSPurchaseDealsWithOther") String LucasTVSPurchaseDealsWith_Other,
            @Field("NoOfStarterMotorServicedInMonth") String NoOfStarterMotorServicedInMonth,
            @Field("NoOfAlternatorServicedInMonth") String NoOfAlternatorServicedInMonth,
            @Field("NoOfWiperMotorSevicedInMonth") String NoOfWiperMotorSevicedInMonth,
            @Field("NoOfStaff") String NoOfStaff,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther,
            @Field("DealsWithOEBrandOthers") String DealsWithOEBrandOthers
    );

    @FormUrlEncoded
    @POST("GetRegisterDetails")
    Call<Register> register_ret_mech_elect(
            @Field("UserName") String UserName,
            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("DealsWithOEBrand") String DealsWith_OEBrand,
            @Field("LucasTVSPurchaseDealsWith") String LucasTVSPurchaseDealsWith,
            @Field("LucasTVSPurchaseDealsWithOther") String LucasTVSPurchaseDealsWith_Other,
            @Field("NoOfStarterMotorServicedInMonth") String NoOfStarterMotorServicedInMonth,
            @Field("NoOfAlternatorServicedInMonth") String NoOfAlternatorServicedInMonth,
            @Field("NoOfWiperMotorSevicedInMonth") String NoOfWiperMotorSevicedInMonth,
            @Field("NoOfStaff") String NoOfStaff,
            @Field("SpecialistIn") String SpecialistIn,
            @Field("MaintainingStock") String MaintainingStock,
            @Field("VehicleAlterMonth") String VehicleAlterMonth,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("SpecialistInOther") String SpecialistInOther,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("ProductDealsWithOther") String ProductDealsWithOther,
            @Field("DealsWithOEBrandOthers") String DealsWithOEBrandOthers


    );

    @GET("ViewRegisterDetails")
    Call<View> getalldetails();



    @FormUrlEncoded
    @POST("SearchRegister")
    Call<Filter> Search(
            @Field("Type") String User_Type,
            @Field("Citypredictive") String state,
            @Field("City") String city


    );

    @FormUrlEncoded
    @POST("EditProfile")
    Call<Update> EditData(
            @Field("Username") String Username,
            @Field("Password") String Password,
            @Field("Type") String Type,
            @Field("UserType") String UserType,
            @Field("Mobile") String Mobile,
            @Field("CompanyName") String CompanyName,
            @Field("Place") String Place,
            @Field("Name") String Name,
            @Field("EmailId") String EmailId


    );

    @FormUrlEncoded
    @POST("ChangePassword")
    Call<Changepassword> ChangePASS(

            @Field("UserName") String UserName,
            @Field("Password") String Password,
            @Field("NewPassword") String NewPassword


    );

    @FormUrlEncoded
    @POST("EditRegister")
    Call<UpdateModel> EditPageData(
            @Field("id") String id,
            @Field("UserName") String UserName,
            @Field("Type") String Type,
            @Field("OwnerName") String Owner_Name,
            @Field("ShopName") String Shop_Name,
            @Field("DoorNo") String DoorNo,
            @Field("Area") String Area,
            @Field("Street") String Street,
            @Field("LandMark") String LandMark,
            @Field("City") String City,
            @Field("State") String State,
            @Field("Country") String Country,
            @Field("Pincode") String Pincode,
            @Field("Mobile") String Mobile,
            @Field("Phone") String Phone,
            @Field("Email") String Email,
            @Field("GSTNumber") String GSTNumber,
            @Field("HowOldTheShopIs") String HowOldThe_Shop,
            @Field("TypeOfOrganisation") String TypeOf_Organisation,
            @Field("OtherPartnersNames") String Other_PartnersNames,
            @Field("Market") String Market,
            @Field("SegmentDeals") String SegmentDeals,
            @Field("ProductDealsWith") String ProductDealsWith,
            @Field("MonthyTurnOver") String MonthyTurnOver,
            @Field("MajorBrandDealsWithElectrical") String MajorBrandDealsWithElectrical,
            @Field("MajorBrandDealsWithElectricalother") String MajorBrandDealsWithElectrical_other,
            @Field("DealsWithOEBrand") String DealsWith_OEBrand,
            @Field("LucasTVSPurchaseDealsWith") String LucasTVSPurchaseDealsWith,
            @Field("LucasTVSPurchaseDealsWithOther") String LucasTVSPurchaseDealsWith_Other,
            @Field("NoOfStarterMotorServicedInMonth") String NoOfStarterMotorServicedInMonth,
            @Field("NoOfAlternatorServicedInMonth") String NoOfAlternatorServicedInMonth,
            @Field("NoOfWiperMotorSevicedInMonth") String NoOfWiperMotorSevicedInMonth,
            @Field("NoOfStaff") String NoOfStaff,
            @Field("SpecialistIn") String SpecialistIn,
            @Field("MaintainingStock") String MaintainingStock,
            @Field("VehicleAlterMonth") String VehicleAlterMonth,
            @Field("ShopImage") String ShopImage,
            @Field("shopimage1") String shopimage1,
            @Field("Idproof") String Visitingcaed,
            @Field("SpecialistInOther") String SpecialistInOther,
            @Field("CaptureLocation") String CaptureLocation,
            @Field("Latitude") String Latitude,
            @Field("Langtitude") String Langtitude,
            @Field("ProductDealsWithOther") String ProductDealsWithOther,
            @Field("DealsWithOEBrandOthers") String DealsWithOEBrandOthers

    );

    @FormUrlEncoded
    @POST("GetRegisterOEDealerDetails")
    Call<RegOEdealer> addOE_dealer(
            @Field("OEMName") String OEMName,
            @Field("City") String City,
            @Field("CompanyName") String CompanyName,
            @Field("DealerCode") String DealerCode,
            @Field("Address") String Address,
            @Field("Location") String Location,
            @Field("C_Name") String C_Name,
            @Field("C_Phone") String C_Phone,
            @Field("C_Desgination") String C_Desgination,
            @Field("C_Email") String C_Email,
            @Field("customerservice") String customerservice,
            @Field("accessiblemanner") String accessiblemanner,
            @Field("networkorservices") String networkorservices,
            @Field("customerserviceComments") String customerserviceComments,
            @Field("accessiblemannerComments") String accessiblemannerComments,
            @Field("networkorservicesComments") String networkorservicesComments,
            @Field("Latitude") String Latitude,
            @Field("Longitude") String Longitude

    );

    @GET("ViewRegisterOEDealerDetails")
    Call<ViewOE> getallOEdetails();


    @GET("StateList")
    Call<Citypredictive> getallstatelist();

    @GET("GetCityDetails")
    Call<Citypredictive> getallCity();

    @FormUrlEncoded
    @POST("GetState")
    Call<StatePridictive> getstate(
            @Field("City") String City
    );

    @FormUrlEncoded
    @POST("GetCity1")
    Call<StateList> getSateCity(
            @Field("State") String State
    );

    @FormUrlEncoded
    @POST("GetStatewiseCity")
    Call<StateList> getdistributer(
            @Field("statename") String State
    );

    @GET("StateList")
    Call<StateList> getstateall();


    @FormUrlEncoded
    @POST("SearchRegisterOEDealer")
    Call<FilterOe> SearchOE(
            @Field("City") String city
    );
    @FormUrlEncoded
    @POST("Usertype")
    Call<CustomerMobileList> getcusmob(
            @Field("Type") String type
    );

    @FormUrlEncoded
    @POST("Customer")
    Call<CustomerDetails> cusdetails(
            @Field("Mobile") String mob,
            @Field("Type") String type
    );

    @GET("OverallPartnumber")
    Call<GetPartNumber> getpartnumber();

    @FormUrlEncoded
    @POST("PartNoDetails")
    Call<GetPartDetails> getpartdetails(
            @Field("Part_No") String parno
    );
    @FormUrlEncoded
    @POST("GetDistributor")
    Call<DistributerDetails> getdistriburter(
            @Field("townnme") String city
    );

    @POST("PlaceOrderExisting")
    Call<OrderNumber> placeorder(
            @Body OrderDetailsToPost orderDetailsToPost
    );

    @FormUrlEncoded
    @POST("orderview")
    Call<ViewOrderSalesRepJson> vieworder(
            @Field("excuteid") String id
    );

    @FormUrlEncoded
    @POST("searchorderviewDetails")
    Call<ViewOrderDetailsFully> orderdetails(
            @Field("orderid") String id
    );


    @FormUrlEncoded
    @POST("AttendanceNew")
    public Call<AttendanceDetails> attendnace(
            @Field("EmpId") String id,
            @Field("Name") String Name,
            @Field("Date") String date,
            @Field("attendanceTime") String attendancetime,
            @Field("Address") String address,
            @Field("Designation") String designation,
            @Field("OutAddress") String outaddress,
            @Field("InLatitude") String inlat,
            @Field("InLongitude") String inlong,
            @Field("OutLatitude") String outlat,
            @Field("OutLongitude") String outlong,
            @Field("EmpCode") String code,
            @Field("AttendDay") String Day,
            @Field("Distance") String distance
    );

    @FormUrlEncoded
    @POST("AddDisributor")
    public Call<AddDistributor> adddis(

            @Field("Cus_No") String id,
            @Field("disname") String name,
            @Field("emailid") String email,
            @Field("mobileno") String date,
            @Field("address") String Name,
            @Field("townnme") String city,
            @Field("cname") String attendancetime
    );


    @FormUrlEncoded
    @POST("SECoodinateinsert")
    public Call<BackGroundServiceJson> background(
            @Field("saname") String name,
            @Field("S_id") String sid,
            @Field("TrackId") String trackid,
            @Field("datetime") String datatime,
            @Field("address") String address,
            @Field("timedate") String timedate,
            @Field("distance") float distance,
            @Field("Regid") String regid,
            @Field("langtitude") double lang,
            @Field("latitude") double latit,
            @Field("EmpCode") String empcode,
            @Field("AttendDay") String attend

    );

    @FormUrlEncoded
    @POST("Attendenceview")
    Call<ViewAttendanceJson> viewattendance(
            @Field("EmpId") String empid
    );

    @FormUrlEncoded
    @POST("AttendenceSearchview")
    Call<ViewAttendanceDateWiseDetails> viewattendancedate(
            @Field("EmpId") String empid,
            @Field("Date") String date
    );

    @FormUrlEncoded
    @POST("AttendenceCoordination")
    Call<CoordinatesView> getcoordinates(
            @Field("S_id") String empid,
            @Field("datetime") String datatime,
            @Field("TrackId") String tackid
    );

    @FormUrlEncoded
    @POST("CityBasedCustomerList")
    Call<GetCustomerListCityWise> getcustomername(
            @Field("City") String city

    );

    @FormUrlEncoded
    @POST("CustomerList")
    Call<CustomerDetailsName> customerdetails(
            @Field("ShopName") String ownername,
            @Field("Mobile") String mobile

    );

    @FormUrlEncoded
    @POST("searchorderviewdate")
    Call<ViewSalesDateWise> viewsalesdatewise(
            @Field("fromdate") String fromdate,
            @Field("todate") String todate,
            @Field("excuteid") String exeid

    );

    @FormUrlEncoded
    @POST("ViewLatlan")
    Call<AttendanceCoordinationMap> getcoordinate(
            @Field("datetime") String date,
            @Field("S_id") String sid
    );


    @FormUrlEncoded
    @POST("searchAttendanceviewdate")
    Call<Attedancedatewiseview> getdatewise(
            @Field("fromdate") String date,
            @Field("todate") String sid,
            @Field("excuteid") String execu
    );

    @FormUrlEncoded
    @POST("GetExeidbaseddetails")
    Call<GetExecutiveDetails> getexecutivedetails(
            @Field("Mobile") String exeid

    );

    @FormUrlEncoded
    @POST("RegionBasedStateList")
    Call<RegionBasedStateList> getStateList(
            @Field("Region") String region
    );
    @POST("StateList")
    Call<RegionBasedStateList> getState(
            @Field("State") String region
    );
    @FormUrlEncoded
    @POST("StateBasedCityList")
    Call<StateBasedCityList> getCityList(
            @Field("State") String region
    );


}
