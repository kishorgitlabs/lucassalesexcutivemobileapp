
package Model.distributerdetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DistributerDetails {

    @SerializedName("data")
    private List<DistriButerData> mData;
    @SerializedName("result")
    private String mResult;

    public List<DistriButerData> getData() {
        return mData;
    }

    public void setData(List<DistriButerData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
