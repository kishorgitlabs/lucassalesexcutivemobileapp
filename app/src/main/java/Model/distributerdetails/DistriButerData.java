
package Model.distributerdetails;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DistriButerData {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("AlterMobile")
    private String mAlterMobile;
    @SerializedName("CPname2")
    private String mCPname2;
    @SerializedName("cname")
    private String mCname;
    @SerializedName("Cus_No")
    private String mCusNo;
    @SerializedName("Cus_type")
    private String mCusType;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("disname")
    private String mDisname;
    @SerializedName("emailid")
    private String mEmailid;
    @SerializedName("insertdatetime")
    private String mInsertdatetime;
    @SerializedName("landline")
    private String mLandline;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("P_Id")
    private String mPId;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("placepassword")
    private String mPlacepassword;
    @SerializedName("protype")
    private String mProtype;
    @SerializedName("Reg_code")
    private String mRegCode;
    @SerializedName("Reg_email")
    private String mRegEmail;
    @SerializedName("Reg_mobile")
    private String mRegMobile;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("Regional_manager")
    private String mRegionalManager;
    @SerializedName("statename")
    private String mStatename;
    @SerializedName("townnme")
    private String mTownnme;
    @SerializedName("updatetime")
    private String mUpdatetime;
    @SerializedName("username")
    private String mUsername;

    @SerializedName("Shortname")
    private String mShortname;

    public String getmShortname() {
        return mShortname;
    }

    public void setmShortname(String mShortname) {
        this.mShortname = mShortname;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAlterMobile() {
        return mAlterMobile;
    }

    public void setAlterMobile(String alterMobile) {
        mAlterMobile = alterMobile;
    }

    public String getCPname2() {
        return mCPname2;
    }

    public void setCPname2(String cPname2) {
        mCPname2 = cPname2;
    }

    public String getCname() {
        return mCname;
    }

    public void setCname(String cname) {
        mCname = cname;
    }

    public String getCusNo() {
        return mCusNo;
    }

    public void setCusNo(String cusNo) {
        mCusNo = cusNo;
    }

    public String getCusType() {
        return mCusType;
    }

    public void setCusType(String cusType) {
        mCusType = cusType;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public String getDisname() {
        return mDisname;
    }

    public void setDisname(String disname) {
        mDisname = disname;
    }

    public String getEmailid() {
        return mEmailid;
    }

    public void setEmailid(String emailid) {
        mEmailid = emailid;
    }

    public String getInsertdatetime() {
        return mInsertdatetime;
    }

    public void setInsertdatetime(String insertdatetime) {
        mInsertdatetime = insertdatetime;
    }

    public String getLandline() {
        return mLandline;
    }

    public void setLandline(String landline) {
        mLandline = landline;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getPId() {
        return mPId;
    }

    public void setPId(String pId) {
        mPId = pId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPlacepassword() {
        return mPlacepassword;
    }

    public void setPlacepassword(String placepassword) {
        mPlacepassword = placepassword;
    }

    public String getProtype() {
        return mProtype;
    }

    public void setProtype(String protype) {
        mProtype = protype;
    }

    public String getRegCode() {
        return mRegCode;
    }

    public void setRegCode(String regCode) {
        mRegCode = regCode;
    }

    public String getRegEmail() {
        return mRegEmail;
    }

    public void setRegEmail(String regEmail) {
        mRegEmail = regEmail;
    }

    public String getRegMobile() {
        return mRegMobile;
    }

    public void setRegMobile(String regMobile) {
        mRegMobile = regMobile;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getRegionalManager() {
        return mRegionalManager;
    }

    public void setRegionalManager(String regionalManager) {
        mRegionalManager = regionalManager;
    }

    public String getStatename() {
        return mStatename;
    }

    public void setStatename(String statename) {
        mStatename = statename;
    }

    public String getTownnme() {
        return mTownnme;
    }

    public void setTownnme(String townnme) {
        mTownnme = townnme;
    }

    public String getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(String updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
