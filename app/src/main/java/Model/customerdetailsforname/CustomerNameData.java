
package Model.customerdetailsforname;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CustomerNameData {

    @SerializedName("Approve")
    private Object mApprove;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("CaptureLocation")
    private Object mCaptureLocation;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("data")
    private CustomerNameData mData;
    @SerializedName("DateOfDataCapture")
    private Object mDateOfDataCapture;
    @SerializedName("DealsWithOEBrand")
    private String mDealsWithOEBrand;
    @SerializedName("DealsWithOEBrandOthers")
    private Object mDealsWithOEBrandOthers;
    @SerializedName("Dealsin")
    private Object mDealsin;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Exeid")
    private Object mExeid;
    @SerializedName("GSTNumber")
    private String mGSTNumber;
    @SerializedName("HowOldTheShopIs")
    private String mHowOldTheShopIs;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Idproof")
    private String mIdproof;
    @SerializedName("IdproofURL")
    private String mIdproofURL;
    @SerializedName("Image")
    private Object mImage;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("LandMark")
    private String mLandMark;
    @SerializedName("Langtitude")
    private Object mLangtitude;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("LucasTVSPurchaseDealsWith")
    private String mLucasTVSPurchaseDealsWith;
    @SerializedName("LucasTVSPurchaseDealsWithOther")
    private Object mLucasTVSPurchaseDealsWithOther;
    @SerializedName("MaintainingStock")
    private Object mMaintainingStock;
    @SerializedName("MajorBrandDealsWithElectrical")
    private String mMajorBrandDealsWithElectrical;
    @SerializedName("MajorBrandDealsWithElectricalother")
    private Object mMajorBrandDealsWithElectricalother;
    @SerializedName("Market")
    private String mMarket;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("MonthyTurnOver")
    private String mMonthyTurnOver;
    @SerializedName("NoOfAlternatorServicedInMonth")
    private Object mNoOfAlternatorServicedInMonth;
    @SerializedName("NoOfStaff")
    private Object mNoOfStaff;
    @SerializedName("NoOfStarterMotorServicedInMonth")
    private Object mNoOfStarterMotorServicedInMonth;
    @SerializedName("NoOfWiperMotorSevicedInMonth")
    private Object mNoOfWiperMotorSevicedInMonth;
    @SerializedName("OtherPartnersNames")
    private Object mOtherPartnersNames;
    @SerializedName("OwnerName")
    private String mOwnerName;
    @SerializedName("Phone")
    private Object mPhone;
    @SerializedName("Pincode")
    private Long mPincode;
    @SerializedName("ProductDealsWith")
    private String mProductDealsWith;
    @SerializedName("ProductDealsWithOther")
    private Object mProductDealsWithOther;
    @SerializedName("result")
    private Object mResult;
    @SerializedName("SegmentDeals")
    private String mSegmentDeals;
    @SerializedName("Shop1URL")
    private String mShop1URL;
    @SerializedName("ShopImage")
    private String mShopImage;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("ShopURL")
    private String mShopURL;
    @SerializedName("shopimage1")
    private String mShopimage1;
    @SerializedName("sid")
    private Long mSid;
    @SerializedName("SpecialistIn")
    private Object mSpecialistIn;
    @SerializedName("SpecialistInOther")
    private Object mSpecialistInOther;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("Type")
    private String mType;
    @SerializedName("TypeOfOrganisation")
    private String mTypeOfOrganisation;
    @SerializedName("Updatetime")
    private Object mUpdatetime;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("VehicleAlterMonth")
    private Object mVehicleAlterMonth;

    public Object getApprove() {
        return mApprove;
    }

    public void setApprove(Object approve) {
        mApprove = approve;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public Object getCaptureLocation() {
        return mCaptureLocation;
    }

    public void setCaptureLocation(Object captureLocation) {
        mCaptureLocation = captureLocation;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public CustomerNameData getData() {
        return mData;
    }

    public void setData(CustomerNameData data) {
        mData = data;
    }

    public Object getDateOfDataCapture() {
        return mDateOfDataCapture;
    }

    public void setDateOfDataCapture(Object dateOfDataCapture) {
        mDateOfDataCapture = dateOfDataCapture;
    }

    public String getDealsWithOEBrand() {
        return mDealsWithOEBrand;
    }

    public void setDealsWithOEBrand(String dealsWithOEBrand) {
        mDealsWithOEBrand = dealsWithOEBrand;
    }

    public Object getDealsWithOEBrandOthers() {
        return mDealsWithOEBrandOthers;
    }

    public void setDealsWithOEBrandOthers(Object dealsWithOEBrandOthers) {
        mDealsWithOEBrandOthers = dealsWithOEBrandOthers;
    }

    public Object getDealsin() {
        return mDealsin;
    }

    public void setDealsin(Object dealsin) {
        mDealsin = dealsin;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String doorNo) {
        mDoorNo = doorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getExeid() {
        return mExeid;
    }

    public void setExeid(Object exeid) {
        mExeid = exeid;
    }

    public String getGSTNumber() {
        return mGSTNumber;
    }

    public void setGSTNumber(String gSTNumber) {
        mGSTNumber = gSTNumber;
    }

    public String getHowOldTheShopIs() {
        return mHowOldTheShopIs;
    }

    public void setHowOldTheShopIs(String howOldTheShopIs) {
        mHowOldTheShopIs = howOldTheShopIs;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIdproof() {
        return mIdproof;
    }

    public void setIdproof(String idproof) {
        mIdproof = idproof;
    }

    public String getIdproofURL() {
        return mIdproofURL;
    }

    public void setIdproofURL(String idproofURL) {
        mIdproofURL = idproofURL;
    }

    public Object getImage() {
        return mImage;
    }

    public void setImage(Object image) {
        mImage = image;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getLandMark() {
        return mLandMark;
    }

    public void setLandMark(String landMark) {
        mLandMark = landMark;
    }

    public Object getLangtitude() {
        return mLangtitude;
    }

    public void setLangtitude(Object langtitude) {
        mLangtitude = langtitude;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object latitude) {
        mLatitude = latitude;
    }

    public String getLucasTVSPurchaseDealsWith() {
        return mLucasTVSPurchaseDealsWith;
    }

    public void setLucasTVSPurchaseDealsWith(String lucasTVSPurchaseDealsWith) {
        mLucasTVSPurchaseDealsWith = lucasTVSPurchaseDealsWith;
    }

    public Object getLucasTVSPurchaseDealsWithOther() {
        return mLucasTVSPurchaseDealsWithOther;
    }

    public void setLucasTVSPurchaseDealsWithOther(Object lucasTVSPurchaseDealsWithOther) {
        mLucasTVSPurchaseDealsWithOther = lucasTVSPurchaseDealsWithOther;
    }

    public Object getMaintainingStock() {
        return mMaintainingStock;
    }

    public void setMaintainingStock(Object maintainingStock) {
        mMaintainingStock = maintainingStock;
    }

    public String getMajorBrandDealsWithElectrical() {
        return mMajorBrandDealsWithElectrical;
    }

    public void setMajorBrandDealsWithElectrical(String majorBrandDealsWithElectrical) {
        mMajorBrandDealsWithElectrical = majorBrandDealsWithElectrical;
    }

    public Object getMajorBrandDealsWithElectricalother() {
        return mMajorBrandDealsWithElectricalother;
    }

    public void setMajorBrandDealsWithElectricalother(Object majorBrandDealsWithElectricalother) {
        mMajorBrandDealsWithElectricalother = majorBrandDealsWithElectricalother;
    }

    public String getMarket() {
        return mMarket;
    }

    public void setMarket(String market) {
        mMarket = market;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getMonthyTurnOver() {
        return mMonthyTurnOver;
    }

    public void setMonthyTurnOver(String monthyTurnOver) {
        mMonthyTurnOver = monthyTurnOver;
    }

    public Object getNoOfAlternatorServicedInMonth() {
        return mNoOfAlternatorServicedInMonth;
    }

    public void setNoOfAlternatorServicedInMonth(Object noOfAlternatorServicedInMonth) {
        mNoOfAlternatorServicedInMonth = noOfAlternatorServicedInMonth;
    }

    public Object getNoOfStaff() {
        return mNoOfStaff;
    }

    public void setNoOfStaff(Object noOfStaff) {
        mNoOfStaff = noOfStaff;
    }

    public Object getNoOfStarterMotorServicedInMonth() {
        return mNoOfStarterMotorServicedInMonth;
    }

    public void setNoOfStarterMotorServicedInMonth(Object noOfStarterMotorServicedInMonth) {
        mNoOfStarterMotorServicedInMonth = noOfStarterMotorServicedInMonth;
    }

    public Object getNoOfWiperMotorSevicedInMonth() {
        return mNoOfWiperMotorSevicedInMonth;
    }

    public void setNoOfWiperMotorSevicedInMonth(Object noOfWiperMotorSevicedInMonth) {
        mNoOfWiperMotorSevicedInMonth = noOfWiperMotorSevicedInMonth;
    }

    public Object getOtherPartnersNames() {
        return mOtherPartnersNames;
    }

    public void setOtherPartnersNames(Object otherPartnersNames) {
        mOtherPartnersNames = otherPartnersNames;
    }

    public String getOwnerName() {
        return mOwnerName;
    }

    public void setOwnerName(String ownerName) {
        mOwnerName = ownerName;
    }

    public Object getPhone() {
        return mPhone;
    }

    public void setPhone(Object phone) {
        mPhone = phone;
    }

    public Long getPincode() {
        return mPincode;
    }

    public void setPincode(Long pincode) {
        mPincode = pincode;
    }

    public String getProductDealsWith() {
        return mProductDealsWith;
    }

    public void setProductDealsWith(String productDealsWith) {
        mProductDealsWith = productDealsWith;
    }

    public Object getProductDealsWithOther() {
        return mProductDealsWithOther;
    }

    public void setProductDealsWithOther(Object productDealsWithOther) {
        mProductDealsWithOther = productDealsWithOther;
    }

    public Object getResult() {
        return mResult;
    }

    public void setResult(Object result) {
        mResult = result;
    }

    public String getSegmentDeals() {
        return mSegmentDeals;
    }

    public void setSegmentDeals(String segmentDeals) {
        mSegmentDeals = segmentDeals;
    }

    public String getShop1URL() {
        return mShop1URL;
    }

    public void setShop1URL(String shop1URL) {
        mShop1URL = shop1URL;
    }

    public String getShopImage() {
        return mShopImage;
    }

    public void setShopImage(String shopImage) {
        mShopImage = shopImage;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getShopURL() {
        return mShopURL;
    }

    public void setShopURL(String shopURL) {
        mShopURL = shopURL;
    }

    public String getShopimage1() {
        return mShopimage1;
    }

    public void setShopimage1(String shopimage1) {
        mShopimage1 = shopimage1;
    }

    public Long getSid() {
        return mSid;
    }

    public void setSid(Long sid) {
        mSid = sid;
    }

    public Object getSpecialistIn() {
        return mSpecialistIn;
    }

    public void setSpecialistIn(Object specialistIn) {
        mSpecialistIn = specialistIn;
    }

    public Object getSpecialistInOther() {
        return mSpecialistInOther;
    }

    public void setSpecialistInOther(Object specialistInOther) {
        mSpecialistInOther = specialistInOther;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getTypeOfOrganisation() {
        return mTypeOfOrganisation;
    }

    public void setTypeOfOrganisation(String typeOfOrganisation) {
        mTypeOfOrganisation = typeOfOrganisation;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public Object getVehicleAlterMonth() {
        return mVehicleAlterMonth;
    }

    public void setVehicleAlterMonth(Object vehicleAlterMonth) {
        mVehicleAlterMonth = vehicleAlterMonth;
    }

}
