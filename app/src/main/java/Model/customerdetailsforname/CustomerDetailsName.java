
package Model.customerdetailsforname;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CustomerDetailsName {

    @SerializedName("data")
    private CustomerNameData mData;
    @SerializedName("result")
    private String mResult;

    public CustomerNameData getData() {
        return mData;
    }

    public void setData(CustomerNameData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
