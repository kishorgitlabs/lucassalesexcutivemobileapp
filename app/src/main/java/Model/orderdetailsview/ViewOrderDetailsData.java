
package Model.orderdetailsview;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ViewOrderDetailsData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("amount")
    private String mAmount;
    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("Branchcode")
    private String mBranchcode;
    @SerializedName("Color")
    private String mColor;
    @SerializedName("CusCode")
    private String mCusCode;
    @SerializedName("CusName")
    private String mCusName;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dealerContactperson")
    private String mDealerContactperson;
    @SerializedName("dealerGst")
    private String mDealerGst;
    @SerializedName("dealerShop")
    private String mDealerShop;
    @SerializedName("dealeraddress")
    private String mDealeraddress;
    @SerializedName("dealercity")
    private String mDealercity;
    @SerializedName("dealercode")
    private String mDealercode;
    @SerializedName("dealeremail")
    private String mDealeremail;
    @SerializedName("dealerid")
    private String mDealerid;
    @SerializedName("dealermobile")
    private String mDealermobile;
    @SerializedName("dealername")
    private String mDealername;
    @SerializedName("dealerregion")
    private String mDealerregion;
    @SerializedName("dealerstate")
    private String mDealerstate;
    @SerializedName("dealerusertype")
    private String mDealerusertype;
    @SerializedName("descode")
    private String mDescode;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Dis_Mobile")
    private String mDisMobile;
    @SerializedName("Discode")
    private String mDiscode;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("Disid")
    private Long mDisid;
    @SerializedName("DistributorName")
    private String mDistributorName;
    @SerializedName("distrubutor")
    private String mDistrubutor;
    @SerializedName("EmpCode")
    private String mEmpCode;
    @SerializedName("excuteid")
    private Long mExcuteid;
    @SerializedName("executiveCode")
    private String mExecutiveCode;
    @SerializedName("executivename")
    private String mExecutivename;
    @SerializedName("Grandtotal")
    private String mGrandtotal;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("inserteddate")
    private String mInserteddate;
    @SerializedName("LrNo")
    private String mLrNo;
    @SerializedName("ModeofTransport")
    private String mModeofTransport;
    @SerializedName("Number")
    private String mNumber;
    @SerializedName("OrderMode")
    private String mOrderMode;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("OrderedDate")
    private String mOrderedDate;
    @SerializedName("orderid")
    private String mOrderid;
    @SerializedName("package")
    private String mPackage;
    @SerializedName("package2")
    private String mPackage2;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("productname")
    private String mProductname;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("ReferenceNo")
    private String mReferenceNo;
    @SerializedName("reg_code")
    private String mRegCode;
    @SerializedName("Reg_mobile")
    private String mRegMobile;
    @SerializedName("Regid")
    private Long mRegid;
    @SerializedName("Regional_manager")
    private String mRegionalManager;
    @SerializedName("remarks")
    private String mRemarks;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("shiptocity")
    private String mShiptocity;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("unitprice")
    private String mUnitprice;
    @SerializedName("Unpackage")
    private String mUnpackage;
    @SerializedName("Voucher")
    private String mVoucher;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getBranchcode() {
        return mBranchcode;
    }

    public void setBranchcode(String branchcode) {
        mBranchcode = branchcode;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getCusCode() {
        return mCusCode;
    }

    public void setCusCode(String cusCode) {
        mCusCode = cusCode;
    }

    public String getCusName() {
        return mCusName;
    }

    public void setCusName(String cusName) {
        mCusName = cusName;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDealerContactperson() {
        return mDealerContactperson;
    }

    public void setDealerContactperson(String dealerContactperson) {
        mDealerContactperson = dealerContactperson;
    }

    public String getDealerGst() {
        return mDealerGst;
    }

    public void setDealerGst(String dealerGst) {
        mDealerGst = dealerGst;
    }

    public String getDealerShop() {
        return mDealerShop;
    }

    public void setDealerShop(String dealerShop) {
        mDealerShop = dealerShop;
    }

    public String getDealeraddress() {
        return mDealeraddress;
    }

    public void setDealeraddress(String dealeraddress) {
        mDealeraddress = dealeraddress;
    }

    public String getDealercity() {
        return mDealercity;
    }

    public void setDealercity(String dealercity) {
        mDealercity = dealercity;
    }

    public String getDealercode() {
        return mDealercode;
    }

    public void setDealercode(String dealercode) {
        mDealercode = dealercode;
    }

    public String getDealeremail() {
        return mDealeremail;
    }

    public void setDealeremail(String dealeremail) {
        mDealeremail = dealeremail;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String dealerid) {
        mDealerid = dealerid;
    }

    public String getDealermobile() {
        return mDealermobile;
    }

    public void setDealermobile(String dealermobile) {
        mDealermobile = dealermobile;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String dealername) {
        mDealername = dealername;
    }

    public String getDealerregion() {
        return mDealerregion;
    }

    public void setDealerregion(String dealerregion) {
        mDealerregion = dealerregion;
    }

    public String getDealerstate() {
        return mDealerstate;
    }

    public void setDealerstate(String dealerstate) {
        mDealerstate = dealerstate;
    }

    public String getDealerusertype() {
        return mDealerusertype;
    }

    public void setDealerusertype(String dealerusertype) {
        mDealerusertype = dealerusertype;
    }

    public String getDescode() {
        return mDescode;
    }

    public void setDescode(String descode) {
        mDescode = descode;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDisMobile() {
        return mDisMobile;
    }

    public void setDisMobile(String disMobile) {
        mDisMobile = disMobile;
    }

    public String getDiscode() {
        return mDiscode;
    }

    public void setDiscode(String discode) {
        mDiscode = discode;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public Long getDisid() {
        return mDisid;
    }

    public void setDisid(Long disid) {
        mDisid = disid;
    }

    public String getDistributorName() {
        return mDistributorName;
    }

    public void setDistributorName(String distributorName) {
        mDistributorName = distributorName;
    }

    public String getDistrubutor() {
        return mDistrubutor;
    }

    public void setDistrubutor(String distrubutor) {
        mDistrubutor = distrubutor;
    }

    public String getEmpCode() {
        return mEmpCode;
    }

    public void setEmpCode(String empCode) {
        mEmpCode = empCode;
    }

    public Long getExcuteid() {
        return mExcuteid;
    }

    public void setExcuteid(Long excuteid) {
        mExcuteid = excuteid;
    }

    public String getExecutiveCode() {
        return mExecutiveCode;
    }

    public void setExecutiveCode(String executiveCode) {
        mExecutiveCode = executiveCode;
    }

    public String getExecutivename() {
        return mExecutivename;
    }

    public void setExecutivename(String executivename) {
        mExecutivename = executivename;
    }

    public String getGrandtotal() {
        return mGrandtotal;
    }

    public void setGrandtotal(String grandtotal) {
        mGrandtotal = grandtotal;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInserteddate() {
        return mInserteddate;
    }

    public void setInserteddate(String inserteddate) {
        mInserteddate = inserteddate;
    }

    public String getLrNo() {
        return mLrNo;
    }

    public void setLrNo(String lrNo) {
        mLrNo = lrNo;
    }

    public String getModeofTransport() {
        return mModeofTransport;
    }

    public void setModeofTransport(String modeofTransport) {
        mModeofTransport = modeofTransport;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getOrderMode() {
        return mOrderMode;
    }

    public void setOrderMode(String orderMode) {
        mOrderMode = orderMode;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getOrderedDate() {
        return mOrderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        mOrderedDate = orderedDate;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

    public String getPackage() {
        return mPackage;
    }

//    public void setPackage(String package) {
//        mPackage = package;
//    }

    public String getPackage2() {
        return mPackage2;
    }

    public void setPackage2(String package2) {
        mPackage2 = package2;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String productname) {
        mProductname = productname;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getReferenceNo() {
        return mReferenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        mReferenceNo = referenceNo;
    }

    public String getRegCode() {
        return mRegCode;
    }

    public void setRegCode(String regCode) {
        mRegCode = regCode;
    }

    public String getRegMobile() {
        return mRegMobile;
    }

    public void setRegMobile(String regMobile) {
        mRegMobile = regMobile;
    }

    public Long getRegid() {
        return mRegid;
    }

    public void setRegid(Long regid) {
        mRegid = regid;
    }

    public String getRegionalManager() {
        return mRegionalManager;
    }

    public void setRegionalManager(String regionalManager) {
        mRegionalManager = regionalManager;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getShiptocity() {
        return mShiptocity;
    }

    public void setShiptocity(String shiptocity) {
        mShiptocity = shiptocity;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUnitprice() {
        return mUnitprice;
    }

    public void setUnitprice(String unitprice) {
        mUnitprice = unitprice;
    }

    public String getUnpackage() {
        return mUnpackage;
    }

    public void setUnpackage(String unpackage) {
        mUnpackage = unpackage;
    }

    public String getVoucher() {
        return mVoucher;
    }

    public void setVoucher(String voucher) {
        mVoucher = voucher;
    }

}
