
package Model.orderdetailsview;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ViewOrderDetailsFully {

    @SerializedName("data")
    private List<ViewOrderDetailsData> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewOrderDetailsData> getData() {
        return mData;
    }

    public void setData(List<ViewOrderDetailsData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
