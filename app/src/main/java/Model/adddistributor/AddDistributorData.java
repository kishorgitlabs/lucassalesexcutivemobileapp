
package Model.adddistributor;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddDistributorData {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("AlterMobile")
    private Object mAlterMobile;
    @SerializedName("CPname2")
    private Object mCPname2;
    @SerializedName("cname")
    private Object mCname;
    @SerializedName("Cus_No")
    private String mCusNo;
    @SerializedName("Cus_type")
    private Object mCusType;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("disname")
    private Object mDisname;
    @SerializedName("emailid")
    private Object mEmailid;
    @SerializedName("insertdatetime")
    private String mInsertdatetime;
    @SerializedName("landline")
    private Object mLandline;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("P_Id")
    private Long mPId;
    @SerializedName("password")
    private Object mPassword;
    @SerializedName("placepassword")
    private Object mPlacepassword;
    @SerializedName("protype")
    private Object mProtype;
    @SerializedName("Reg_code")
    private Object mRegCode;
    @SerializedName("Reg_email")
    private Object mRegEmail;
    @SerializedName("Reg_mobile")
    private Object mRegMobile;
    @SerializedName("Region")
    private Object mRegion;
    @SerializedName("Regional_manager")
    private Object mRegionalManager;
    @SerializedName("statename")
    private Object mStatename;
    @SerializedName("townnme")
    private Object mTownnme;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("username")
    private Object mUsername;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getAlterMobile() {
        return mAlterMobile;
    }

    public void setAlterMobile(Object alterMobile) {
        mAlterMobile = alterMobile;
    }

    public Object getCPname2() {
        return mCPname2;
    }

    public void setCPname2(Object cPname2) {
        mCPname2 = cPname2;
    }

    public Object getCname() {
        return mCname;
    }

    public void setCname(Object cname) {
        mCname = cname;
    }

    public String getCusNo() {
        return mCusNo;
    }

    public void setCusNo(String cusNo) {
        mCusNo = cusNo;
    }

    public Object getCusType() {
        return mCusType;
    }

    public void setCusType(Object cusType) {
        mCusType = cusType;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public Object getDisname() {
        return mDisname;
    }

    public void setDisname(Object disname) {
        mDisname = disname;
    }

    public Object getEmailid() {
        return mEmailid;
    }

    public void setEmailid(Object emailid) {
        mEmailid = emailid;
    }

    public String getInsertdatetime() {
        return mInsertdatetime;
    }

    public void setInsertdatetime(String insertdatetime) {
        mInsertdatetime = insertdatetime;
    }

    public Object getLandline() {
        return mLandline;
    }

    public void setLandline(Object landline) {
        mLandline = landline;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public Long getPId() {
        return mPId;
    }

    public void setPId(Long pId) {
        mPId = pId;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public Object getPlacepassword() {
        return mPlacepassword;
    }

    public void setPlacepassword(Object placepassword) {
        mPlacepassword = placepassword;
    }

    public Object getProtype() {
        return mProtype;
    }

    public void setProtype(Object protype) {
        mProtype = protype;
    }

    public Object getRegCode() {
        return mRegCode;
    }

    public void setRegCode(Object regCode) {
        mRegCode = regCode;
    }

    public Object getRegEmail() {
        return mRegEmail;
    }

    public void setRegEmail(Object regEmail) {
        mRegEmail = regEmail;
    }

    public Object getRegMobile() {
        return mRegMobile;
    }

    public void setRegMobile(Object regMobile) {
        mRegMobile = regMobile;
    }

    public Object getRegion() {
        return mRegion;
    }

    public void setRegion(Object region) {
        mRegion = region;
    }

    public Object getRegionalManager() {
        return mRegionalManager;
    }

    public void setRegionalManager(Object regionalManager) {
        mRegionalManager = regionalManager;
    }

    public Object getStatename() {
        return mStatename;
    }

    public void setStatename(Object statename) {
        mStatename = statename;
    }

    public Object getTownnme() {
        return mTownnme;
    }

    public void setTownnme(Object townnme) {
        mTownnme = townnme;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public Object getUsername() {
        return mUsername;
    }

    public void setUsername(Object username) {
        mUsername = username;
    }

}
