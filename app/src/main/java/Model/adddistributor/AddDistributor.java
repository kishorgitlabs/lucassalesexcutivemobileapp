
package Model.adddistributor;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddDistributor {

    @SerializedName("data")
    private AddDistributorData mData;
    @SerializedName("result")
    private String mResult;

    public AddDistributorData getData() {
        return mData;
    }

    public void setData(AddDistributorData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
