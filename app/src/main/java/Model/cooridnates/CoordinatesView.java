
package Model.cooridnates;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CoordinatesView {

    @SerializedName("data")
    private List<CoordinatesData> mData;
    @SerializedName("result")
    private String mResult;

    public List<CoordinatesData> getData() {
        return mData;
    }

    public void setData(List<CoordinatesData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
