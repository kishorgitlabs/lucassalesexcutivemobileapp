
package Model.customerdetails;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CustomerDetailsData {

    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("Area")
    private String mArea;
    @SerializedName("CaptureLocation")
    private String mCaptureLocation;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("data")
    private CustomerDetailsData mData;
    @SerializedName("DateOfDataCapture")
    private String mDateOfDataCapture;
    @SerializedName("DealsWithOEBrand")
    private String mDealsWithOEBrand;
    @SerializedName("DealsWithOEBrandOthers")
    private String mDealsWithOEBrandOthers;
    @SerializedName("Dealsin")
    private String mDealsin;
    @SerializedName("DoorNo")
    private String mDoorNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Exeid")
    private String mExeid;
    @SerializedName("GSTNumber")
    private String mGSTNumber;
    @SerializedName("HowOldTheShopIs")
    private String mHowOldTheShopIs;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Idproof")
    private String mIdproof;
    @SerializedName("IdproofURL")
    private String mIdproofURL;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("LandMark")
    private String mLandMark;
    @SerializedName("Langtitude")
    private String mLangtitude;
    @SerializedName("Latitude")
    private String mLatitude;
    @SerializedName("LucasTVSPurchaseDealsWith")
    private String mLucasTVSPurchaseDealsWith;
    @SerializedName("LucasTVSPurchaseDealsWithOther")
    private String mLucasTVSPurchaseDealsWithOther;
    @SerializedName("MaintainingStock")
    private String mMaintainingStock;
    @SerializedName("MajorBrandDealsWithElectrical")
    private String mMajorBrandDealsWithElectrical;
    @SerializedName("MajorBrandDealsWithElectricalother")
    private String mMajorBrandDealsWithElectricalother;
    @SerializedName("Market")
    private String mMarket;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("MonthyTurnOver")
    private String mMonthyTurnOver;
    @SerializedName("NoOfAlternatorServicedInMonth")
    private String mNoOfAlternatorServicedInMonth;
    @SerializedName("NoOfStaff")
    private String mNoOfStaff;
    @SerializedName("NoOfStarterMotorServicedInMonth")
    private String mNoOfStarterMotorServicedInMonth;
    @SerializedName("NoOfWiperMotorSevicedInMonth")
    private String mNoOfWiperMotorSevicedInMonth;
    @SerializedName("OtherPartnersNames")
    private String mOtherPartnersNames;
    @SerializedName("OwnerName")
    private String mOwnerName;
    @SerializedName("Phone")
    private String mPhone;
    @SerializedName("Pincode")
    private Long mPincode;
    @SerializedName("ProductDealsWith")
    private String mProductDealsWith;
    @SerializedName("ProductDealsWithOther")
    private String mProductDealsWithOther;
    @SerializedName("result")
    private String mResult;
    @SerializedName("SegmentDeals")
    private String mSegmentDeals;
    @SerializedName("Shop1URL")
    private String mShop1URL;
    @SerializedName("ShopImage")
    private String mShopImage;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("ShopURL")
    private String mShopURL;
    @SerializedName("shopimage1")
    private String mShopimage1;
    @SerializedName("sid")
    private Long mSid;
    @SerializedName("SpecialistIn")
    private String mSpecialistIn;
    @SerializedName("SpecialistInOther")
    private String mSpecialistInOther;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("Type")
    private String mType;
    @SerializedName("TypeOfOrganisation")
    private String mTypeOfOrganisation;
    @SerializedName("Updatetime")
    private String mUpdatetime;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("VehicleAlterMonth")
    private String mVehicleAlterMonth;

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public String getCaptureLocation() {
        return mCaptureLocation;
    }

    public void setCaptureLocation(String captureLocation) {
        mCaptureLocation = captureLocation;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public CustomerDetailsData getData() {
        return mData;
    }

    public void setData(CustomerDetailsData data) {
        mData = data;
    }

    public String getDateOfDataCapture() {
        return mDateOfDataCapture;
    }

    public void setDateOfDataCapture(String dateOfDataCapture) {
        mDateOfDataCapture = dateOfDataCapture;
    }

    public String getDealsWithOEBrand() {
        return mDealsWithOEBrand;
    }

    public void setDealsWithOEBrand(String dealsWithOEBrand) {
        mDealsWithOEBrand = dealsWithOEBrand;
    }

    public String getDealsWithOEBrandOthers() {
        return mDealsWithOEBrandOthers;
    }

    public void setDealsWithOEBrandOthers(String dealsWithOEBrandOthers) {
        mDealsWithOEBrandOthers = dealsWithOEBrandOthers;
    }

    public String getDealsin() {
        return mDealsin;
    }

    public void setDealsin(String dealsin) {
        mDealsin = dealsin;
    }

    public String getDoorNo() {
        return mDoorNo;
    }

    public void setDoorNo(String doorNo) {
        mDoorNo = doorNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getExeid() {
        return mExeid;
    }

    public void setExeid(String exeid) {
        mExeid = exeid;
    }

    public String getGSTNumber() {
        return mGSTNumber;
    }

    public void setGSTNumber(String gSTNumber) {
        mGSTNumber = gSTNumber;
    }

    public String getHowOldTheShopIs() {
        return mHowOldTheShopIs;
    }

    public void setHowOldTheShopIs(String howOldTheShopIs) {
        mHowOldTheShopIs = howOldTheShopIs;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIdproof() {
        return mIdproof;
    }

    public void setIdproof(String idproof) {
        mIdproof = idproof;
    }

    public String getIdproofURL() {
        return mIdproofURL;
    }

    public void setIdproofURL(String idproofURL) {
        mIdproofURL = idproofURL;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getLandMark() {
        return mLandMark;
    }

    public void setLandMark(String landMark) {
        mLandMark = landMark;
    }

    public String getLangtitude() {
        return mLangtitude;
    }

    public void setLangtitude(String langtitude) {
        mLangtitude = langtitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLucasTVSPurchaseDealsWith() {
        return mLucasTVSPurchaseDealsWith;
    }

    public void setLucasTVSPurchaseDealsWith(String lucasTVSPurchaseDealsWith) {
        mLucasTVSPurchaseDealsWith = lucasTVSPurchaseDealsWith;
    }

    public String getLucasTVSPurchaseDealsWithOther() {
        return mLucasTVSPurchaseDealsWithOther;
    }

    public void setLucasTVSPurchaseDealsWithOther(String lucasTVSPurchaseDealsWithOther) {
        mLucasTVSPurchaseDealsWithOther = lucasTVSPurchaseDealsWithOther;
    }

    public String getMaintainingStock() {
        return mMaintainingStock;
    }

    public void setMaintainingStock(String maintainingStock) {
        mMaintainingStock = maintainingStock;
    }

    public String getMajorBrandDealsWithElectrical() {
        return mMajorBrandDealsWithElectrical;
    }

    public void setMajorBrandDealsWithElectrical(String majorBrandDealsWithElectrical) {
        mMajorBrandDealsWithElectrical = majorBrandDealsWithElectrical;
    }

    public String getMajorBrandDealsWithElectricalother() {
        return mMajorBrandDealsWithElectricalother;
    }

    public void setMajorBrandDealsWithElectricalother(String majorBrandDealsWithElectricalother) {
        mMajorBrandDealsWithElectricalother = majorBrandDealsWithElectricalother;
    }

    public String getMarket() {
        return mMarket;
    }

    public void setMarket(String market) {
        mMarket = market;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getMonthyTurnOver() {
        return mMonthyTurnOver;
    }

    public void setMonthyTurnOver(String monthyTurnOver) {
        mMonthyTurnOver = monthyTurnOver;
    }

    public String getNoOfAlternatorServicedInMonth() {
        return mNoOfAlternatorServicedInMonth;
    }

    public void setNoOfAlternatorServicedInMonth(String noOfAlternatorServicedInMonth) {
        mNoOfAlternatorServicedInMonth = noOfAlternatorServicedInMonth;
    }

    public String getNoOfStaff() {
        return mNoOfStaff;
    }

    public void setNoOfStaff(String noOfStaff) {
        mNoOfStaff = noOfStaff;
    }

    public String getNoOfStarterMotorServicedInMonth() {
        return mNoOfStarterMotorServicedInMonth;
    }

    public void setNoOfStarterMotorServicedInMonth(String noOfStarterMotorServicedInMonth) {
        mNoOfStarterMotorServicedInMonth = noOfStarterMotorServicedInMonth;
    }

    public String getNoOfWiperMotorSevicedInMonth() {
        return mNoOfWiperMotorSevicedInMonth;
    }

    public void setNoOfWiperMotorSevicedInMonth(String noOfWiperMotorSevicedInMonth) {
        mNoOfWiperMotorSevicedInMonth = noOfWiperMotorSevicedInMonth;
    }

    public String getOtherPartnersNames() {
        return mOtherPartnersNames;
    }

    public void setOtherPartnersNames(String otherPartnersNames) {
        mOtherPartnersNames = otherPartnersNames;
    }

    public String getOwnerName() {
        return mOwnerName;
    }

    public void setOwnerName(String ownerName) {
        mOwnerName = ownerName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public Long getPincode() {
        return mPincode;
    }

    public void setPincode(Long pincode) {
        mPincode = pincode;
    }

    public String getProductDealsWith() {
        return mProductDealsWith;
    }

    public void setProductDealsWith(String productDealsWith) {
        mProductDealsWith = productDealsWith;
    }

    public String getProductDealsWithOther() {
        return mProductDealsWithOther;
    }

    public void setProductDealsWithOther(String productDealsWithOther) {
        mProductDealsWithOther = productDealsWithOther;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public String getSegmentDeals() {
        return mSegmentDeals;
    }

    public void setSegmentDeals(String segmentDeals) {
        mSegmentDeals = segmentDeals;
    }

    public String getShop1URL() {
        return mShop1URL;
    }

    public void setShop1URL(String shop1URL) {
        mShop1URL = shop1URL;
    }

    public String getShopImage() {
        return mShopImage;
    }

    public void setShopImage(String shopImage) {
        mShopImage = shopImage;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getShopURL() {
        return mShopURL;
    }

    public void setShopURL(String shopURL) {
        mShopURL = shopURL;
    }

    public String getShopimage1() {
        return mShopimage1;
    }

    public void setShopimage1(String shopimage1) {
        mShopimage1 = shopimage1;
    }

    public Long getSid() {
        return mSid;
    }

    public void setSid(Long sid) {
        mSid = sid;
    }

    public String getSpecialistIn() {
        return mSpecialistIn;
    }

    public void setSpecialistIn(String specialistIn) {
        mSpecialistIn = specialistIn;
    }

    public String getSpecialistInOther() {
        return mSpecialistInOther;
    }

    public void setSpecialistInOther(String specialistInOther) {
        mSpecialistInOther = specialistInOther;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getTypeOfOrganisation() {
        return mTypeOfOrganisation;
    }

    public void setTypeOfOrganisation(String typeOfOrganisation) {
        mTypeOfOrganisation = typeOfOrganisation;
    }

    public String getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(String updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getVehicleAlterMonth() {
        return mVehicleAlterMonth;
    }

    public void setVehicleAlterMonth(String vehicleAlterMonth) {
        mVehicleAlterMonth = vehicleAlterMonth;
    }

}
