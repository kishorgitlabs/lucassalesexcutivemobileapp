
package Model.executivedetails;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetExecutiveData {

    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("City")
    private String mCity;
    @SerializedName("CompanyName")
    private String mCompanyName;
    @SerializedName("DeleteFlag")
    private String mDeleteFlag;
    @SerializedName("EmailId")
    private String mEmailId;
    @SerializedName("id")
    private String mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private String mName;
    @SerializedName("NewPassword")
    private String mNewPassword;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("Place")
    private String mPlace;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("sid")
    private String mSid;
    @SerializedName("State")
    private String mState;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("UserImage")
    private String mUserImage;
    @SerializedName("Username")
    private String mUsername;
    @SerializedName("Usertype")
    private String mUsertype;

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getDeleteFlag() {
        return mDeleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        mDeleteFlag = deleteFlag;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public void setNewPassword(String newPassword) {
        mNewPassword = newPassword;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String place) {
        mPlace = place;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getSid() {
        return mSid;
    }

    public void setSid(String sid) {
        mSid = sid;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserImage() {
        return mUserImage;
    }

    public void setUserImage(String userImage) {
        mUserImage = userImage;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
