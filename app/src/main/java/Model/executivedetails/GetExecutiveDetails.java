
package Model.executivedetails;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetExecutiveDetails {

    @SerializedName("data")
    private List<GetExecutiveData> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetExecutiveData> getData() {
        return mData;
    }

    public void setData(List<GetExecutiveData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
