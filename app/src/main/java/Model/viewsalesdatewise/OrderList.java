
package Model.viewsalesdatewise;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderList {

    @SerializedName("dealername")
    private String mDealername;
    @SerializedName("DistributorName")
    private String mDistributorName;
    @SerializedName("executivename")
    private String mExecutivename;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("OrderedDate")
    private String mOrderedDate;
    @SerializedName("orderid")
    private String mOrderid;
    @SerializedName("unitprice")
    private String mUnitprice;

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String dealername) {
        mDealername = dealername;
    }

    public String getDistributorName() {
        return mDistributorName;
    }

    public void setDistributorName(String distributorName) {
        mDistributorName = distributorName;
    }

    public String getExecutivename() {
        return mExecutivename;
    }

    public void setExecutivename(String executivename) {
        mExecutivename = executivename;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getOrderedDate() {
        return mOrderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        mOrderedDate = orderedDate;
    }

    public String getOrderid() {
        return mOrderid;
    }

    public void setOrderid(String orderid) {
        mOrderid = orderid;
    }

    public String getUnitprice() {
        return mUnitprice;
    }

    public void setUnitprice(String unitprice) {
        mUnitprice = unitprice;
    }

}
