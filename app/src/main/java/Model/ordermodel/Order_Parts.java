package Model.ordermodel;

public class Order_Parts {

    String partNumber;
    String Description;
    String partImage;
    String partName;
    String Quantity;
    String PartPrice;
    String TotalAmount;
    String DeliveryStatus;
    String Active;

    public String getSegement() {
        return this.segement;
    }

    public void setSegement(final String segement) {
        this.segement = segement;
    }

    String segement;

    public String getPartNumber() {
        return this.partNumber;
    }

    public void setPartNumber(final String partNumber) {
        this.partNumber = partNumber;
    }

    public String getDescription() {
        return this.Description;
    }

    public void setDescription(final String description) {
        this.Description = description;
    }

    public String getPartImage() {
        return this.partImage;
    }

    public void setPartImage(final String partImage) {
        this.partImage = partImage;
    }

    public String getPartName() {
        return this.partName;
    }

    public void setPartName(final String partName) {
        this.partName = partName;
    }

    public String getQuantity() {
        return this.Quantity;
    }

    public void setQuantity(final String quantity) {
        this.Quantity = quantity;
    }

    public String getPartPrice() {
        return this.PartPrice;
    }

    public void setPartPrice(final String partPrice) {
        this.PartPrice = partPrice;
    }

    public String getTotalAmount() {
        return this.TotalAmount;
    }

    public void setTotalAmount(final String totalAmount) {
        this.TotalAmount = totalAmount;
    }

    public String getDeliveryStatus() {
        return this.DeliveryStatus;
    }

    public void setDeliveryStatus(final String deliveryStatus) {
        this.DeliveryStatus = deliveryStatus;
    }

    public String getActive() {
        return this.Active;
    }

    public void setActive(final String active) {
        this.Active = active;
    }
}
