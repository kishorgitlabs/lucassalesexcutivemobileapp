
package Model.orderresult;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderNumber {

    @SerializedName("data")
    private List<OrderResultData> mData;
    @SerializedName("result")
    private String mResult;

    public List<OrderResultData> getData() {
        return mData;
    }

    public void setData(List<OrderResultData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
