
package Model.backgroundattendance;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("datetime")
    private String mDatetime;
    @SerializedName("Disid")
    private Object mDisid;
    @SerializedName("distance")
    private Long mDistance;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("langtitude")
    private Double mLangtitude;
    @SerializedName("latitude")
    private Double mLatitude;
    @SerializedName("Message")
    private Object mMessage;
    @SerializedName("Regid")
    private Long mRegid;
    @SerializedName("S_id")
    private Long mSId;
    @SerializedName("saname")
    private String mSaname;
    @SerializedName("timedate")
    private String mTimedate;
    @SerializedName("TrackId")
    private Long mTrackId;
    @SerializedName("updateddate")
    private Object mUpdateddate;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDatetime() {
        return mDatetime;
    }

    public void setDatetime(String datetime) {
        mDatetime = datetime;
    }

    public Object getDisid() {
        return mDisid;
    }

    public void setDisid(Object disid) {
        mDisid = disid;
    }

    public Long getDistance() {
        return mDistance;
    }

    public void setDistance(Long distance) {
        mDistance = distance;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Double getLangtitude() {
        return mLangtitude;
    }

    public void setLangtitude(Double langtitude) {
        mLangtitude = langtitude;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public Object getMessage() {
        return mMessage;
    }

    public void setMessage(Object message) {
        mMessage = message;
    }

    public Long getRegid() {
        return mRegid;
    }

    public void setRegid(Long regid) {
        mRegid = regid;
    }

    public Long getSId() {
        return mSId;
    }

    public void setSId(Long sId) {
        mSId = sId;
    }

    public String getSaname() {
        return mSaname;
    }

    public void setSaname(String saname) {
        mSaname = saname;
    }

    public String getTimedate() {
        return mTimedate;
    }

    public void setTimedate(String timedate) {
        mTimedate = timedate;
    }

    public Long getTrackId() {
        return mTrackId;
    }

    public void setTrackId(Long trackId) {
        mTrackId = trackId;
    }

    public Object getUpdateddate() {
        return mUpdateddate;
    }

    public void setUpdateddate(Object updateddate) {
        mUpdateddate = updateddate;
    }

}
