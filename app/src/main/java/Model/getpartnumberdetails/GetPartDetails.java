
package Model.getpartnumberdetails;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetPartDetails {

    @SerializedName("data")
    private PartNumberDetails mData;
    @SerializedName("result")
    private String mResult;

    public PartNumberDetails getData() {
        return mData;
    }

    public void setData(PartNumberDetails data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
