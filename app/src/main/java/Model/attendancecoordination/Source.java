
package Model.attendancecoordination;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Source {

    @SerializedName("InLatitude")
    private double mInLatitude;
    @SerializedName("InLongitude")
    private double mInLongitude;

    public double getInLatitude() {
        return mInLatitude;
    }

    public void setInLatitude(double inLatitude) {
        mInLatitude = inLatitude;
    }

    public double getInLongitude() {
        return mInLongitude;
    }

    public void setInLongitude(double inLongitude) {
        mInLongitude = inLongitude;
    }

}
