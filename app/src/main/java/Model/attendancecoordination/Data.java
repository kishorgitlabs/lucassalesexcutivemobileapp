
package Model.attendancecoordination;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("codination")
    private List<Codination> mCodination;
    @SerializedName("designation")
    private Designation mDesignation;
    @SerializedName("Source")
    private Source mSource;

    public List<Codination> getCodination() {
        return mCodination;
    }

    public void setCodination(List<Codination> codination) {
        mCodination = codination;
    }

    public Designation getDesignation() {
        return mDesignation;
    }

    public void setDesignation(Designation designation) {
        mDesignation = designation;
    }

    public Source getSource() {
        return mSource;
    }

    public void setSource(Source source) {
        mSource = source;
    }

}
