
package Model.attendancecoordination;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Designation {

    @SerializedName("OutLatitude")
    private double mOutLatitude;
    @SerializedName("OutLongitude")
    private double mOutLongitude;

    public double getOutLatitude() {
        return mOutLatitude;
    }

    public void setOutLatitude(double outLatitude) {
        mOutLatitude = outLatitude;
    }

    public double getOutLongitude() {
        return mOutLongitude;
    }

    public void setOutLongitude(double outLongitude) {
        mOutLongitude = outLongitude;
    }

}
