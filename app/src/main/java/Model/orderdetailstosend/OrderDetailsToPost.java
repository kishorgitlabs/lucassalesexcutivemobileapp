
package Model.orderdetailstosend;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderDetailsToPost {

    @SerializedName("Exist_Orders_List")
    private List<ExistOrdersList> mExistOrdersList;

    public List<ExistOrdersList> getExistOrdersList() {
        return mExistOrdersList;
    }

    public void setExistOrdersList(List<ExistOrdersList> existOrdersList) {
        mExistOrdersList = existOrdersList;
    }

}
