
package Model.orderdetailstosend;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ExistOrdersList {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("amount")
    private String mAmount;
    @SerializedName("CusCode")
    private String mCusCode;
    @SerializedName("CusName")
    private String mCusName;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dealeraddress")
    private String mDealeraddress;
    @SerializedName("dealercity")
    private String mDealercity;
    @SerializedName("dealercode")
    private String mDealercode;
    @SerializedName("dealeremail")
    private String mDealeremail;
    @SerializedName("dealerid")
    private String mDealerid;
    @SerializedName("dealermobile")
    private String mDealermobile;
    @SerializedName("dealername")
    private String mDealername;
    @SerializedName("dealerregion")
    private String mDealerregion;
    @SerializedName("dealerstate")
    private String mDealerstate;
    @SerializedName("dealerusertype")
    private String mDealerusertype;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("Disid")
    private String mDisid;
    @SerializedName("DistributorName")
    private String mDistributorName;
    @SerializedName("EmpCode")
    private String mEmpCode;
    @SerializedName("excuteid")
    private String mExcuteid;
    @SerializedName("executivename")
    private String mExecutivename;
    @SerializedName("Grandtotal")
    private String mGrandtotal;
    @SerializedName("OrderMode")
    private String mOrderMode;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("productname")
    private String mProductname;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("remarks")
    private String mRemarks;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("unitprice")
    private String mUnitprice;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public String getCusCode() {
        return mCusCode;
    }

    public void setCusCode(String cusCode) {
        mCusCode = cusCode;
    }

    public String getCusName() {
        return mCusName;
    }

    public void setCusName(String cusName) {
        mCusName = cusName;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDealeraddress() {
        return mDealeraddress;
    }

    public void setDealeraddress(String dealeraddress) {
        mDealeraddress = dealeraddress;
    }

    public String getDealercity() {
        return mDealercity;
    }

    public void setDealercity(String dealercity) {
        mDealercity = dealercity;
    }

    public String getDealercode() {
        return mDealercode;
    }

    public void setDealercode(String dealercode) {
        mDealercode = dealercode;
    }

    public String getDealeremail() {
        return mDealeremail;
    }

    public void setDealeremail(String dealeremail) {
        mDealeremail = dealeremail;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String dealerid) {
        mDealerid = dealerid;
    }

    public String getDealermobile() {
        return mDealermobile;
    }

    public void setDealermobile(String dealermobile) {
        mDealermobile = dealermobile;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String dealername) {
        mDealername = dealername;
    }

    public String getDealerregion() {
        return mDealerregion;
    }

    public void setDealerregion(String dealerregion) {
        mDealerregion = dealerregion;
    }

    public String getDealerstate() {
        return mDealerstate;
    }

    public void setDealerstate(String dealerstate) {
        mDealerstate = dealerstate;
    }

    public String getDealerusertype() {
        return mDealerusertype;
    }

    public void setDealerusertype(String dealerusertype) {
        mDealerusertype = dealerusertype;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getDisid() {
        return mDisid;
    }

    public void setDisid(String disid) {
        mDisid = disid;
    }

    public String getDistributorName() {
        return mDistributorName;
    }

    public void setDistributorName(String distributorName) {
        mDistributorName = distributorName;
    }

    public String getEmpCode() {
        return mEmpCode;
    }

    public void setEmpCode(String empCode) {
        mEmpCode = empCode;
    }

    public String getExcuteid() {
        return mExcuteid;
    }

    public void setExcuteid(String excuteid) {
        mExcuteid = excuteid;
    }

    public String getExecutivename() {
        return mExecutivename;
    }

    public void setExecutivename(String executivename) {
        mExecutivename = executivename;
    }

    public String getGrandtotal() {
        return mGrandtotal;
    }

    public void setGrandtotal(String grandtotal) {
        mGrandtotal = grandtotal;
    }

    public String getOrderMode() {
        return mOrderMode;
    }

    public void setOrderMode(String orderMode) {
        mOrderMode = orderMode;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String productname) {
        mProductname = productname;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUnitprice() {
        return mUnitprice;
    }

    public void setUnitprice(String unitprice) {
        mUnitprice = unitprice;
    }

}
