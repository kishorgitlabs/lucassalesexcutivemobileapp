
package Model.attendance;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AttendanceDetails {

    @SerializedName("data")
    private AttendanceData mData;
    @SerializedName("result")
    private String mResult;

    public AttendanceData getData() {
        return mData;
    }

    public void setData(AttendanceData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
