
package Model.attendance;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AttendanceData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("AttendDay")
    private String mAttendDay;
    @SerializedName("attendanceTime")
    private String mAttendanceTime;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Designation")
    private String mDesignation;
    @SerializedName("Disid")
    private String mDisid;
    @SerializedName("Distance")
    private String mDistance;
    @SerializedName("EmpCode")
    private String mEmpCode;
    @SerializedName("EmpId")
    private Long mEmpId;
    @SerializedName("flag")
    private Long mFlag;
    @SerializedName("id")
    private String mId;
    @SerializedName("InLatitude")
    private String mInLatitude;
    @SerializedName("InLongitude")
    private String mInLongitude;
    @SerializedName("InTime")
    private String mInTime;
    @SerializedName("Name")
    private String mName;
    @SerializedName("OutAddress")
    private String mOutAddress;
    @SerializedName("OutLatitude")
    private String mOutLatitude;
    @SerializedName("OutLongitude")
    private String mOutLongitude;
    @SerializedName("OutTime")
    private String mOutTime;
    @SerializedName("RegName")
    private String mRegName;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("TotalDuration")
    private String mTotalDuration;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAttendDay() {
        return mAttendDay;
    }

    public void setAttendDay(String attendDay) {
        mAttendDay = attendDay;
    }

    public String getAttendanceTime() {
        return mAttendanceTime;
    }

    public void setAttendanceTime(String attendanceTime) {
        mAttendanceTime = attendanceTime;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDesignation() {
        return mDesignation;
    }

    public void setDesignation(String designation) {
        mDesignation = designation;
    }

    public String getDisid() {
        return mDisid;
    }

    public void setDisid(String disid) {
        mDisid = disid;
    }

    public String getDistance() {
        return mDistance;
    }

    public void setDistance(String distance) {
        mDistance = distance;
    }

    public String getEmpCode() {
        return mEmpCode;
    }

    public void setEmpCode(String empCode) {
        mEmpCode = empCode;
    }

    public Long getEmpId() {
        return mEmpId;
    }

    public void setEmpId(Long empId) {
        mEmpId = empId;
    }

    public Long getFlag() {
        return mFlag;
    }

    public void setFlag(Long flag) {
        mFlag = flag;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInLatitude() {
        return mInLatitude;
    }

    public void setInLatitude(String inLatitude) {
        mInLatitude = inLatitude;
    }

    public String getInLongitude() {
        return mInLongitude;
    }

    public void setInLongitude(String inLongitude) {
        mInLongitude = inLongitude;
    }

    public String getInTime() {
        return mInTime;
    }

    public void setInTime(String inTime) {
        mInTime = inTime;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOutAddress() {
        return mOutAddress;
    }

    public void setOutAddress(String outAddress) {
        mOutAddress = outAddress;
    }

    public String getOutLatitude() {
        return mOutLatitude;
    }

    public void setOutLatitude(String outLatitude) {
        mOutLatitude = outLatitude;
    }

    public String getOutLongitude() {
        return mOutLongitude;
    }

    public void setOutLongitude(String outLongitude) {
        mOutLongitude = outLongitude;
    }

    public String getOutTime() {
        return mOutTime;
    }

    public void setOutTime(String outTime) {
        mOutTime = outTime;
    }

    public String getRegName() {
        return mRegName;
    }

    public void setRegName(String regName) {
        mRegName = regName;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getTotalDuration() {
        return mTotalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        mTotalDuration = totalDuration;
    }

}
