
package Model.viewordersalesrep;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ViewOrderSalesRepJson {

    @SerializedName("data")
    private ViewOrderSalesData mData;
    @SerializedName("result")
    private String mResult;

    public ViewOrderSalesData getData() {
        return mData;
    }

    public void setData(ViewOrderSalesData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
