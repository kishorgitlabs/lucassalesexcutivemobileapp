
package Model.viewordersalesrep;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ViewOrderSalesData {

    @SerializedName("Order_list")
    private List<OrderList> mOrderList;
    @SerializedName("tot")
    private String mTot;

    public List<OrderList> getOrderList() {
        return mOrderList;
    }

    public void setOrderList(List<OrderList> orderList) {
        mOrderList = orderList;
    }

    public String getTot() {
        return mTot;
    }

    public void setTot(String tot) {
        mTot = tot;
    }

}
