
package Model.attendancedatewise;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Attedancedatewiseview {

    @SerializedName("data")
    private List<DatewiseAttendanceData> mData;
    @SerializedName("result")
    private String mResult;

    public List<DatewiseAttendanceData> getData() {
        return mData;
    }

    public void setData(List<DatewiseAttendanceData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
