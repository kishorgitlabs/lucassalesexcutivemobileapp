
package Model.viewattendance;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ViewAttendanceJson {

    @SerializedName("data")
    private List<ViewAttendanceData> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewAttendanceData> getData() {
        return mData;
    }

    public void setData(List<ViewAttendanceData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
