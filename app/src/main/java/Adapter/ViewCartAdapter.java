package Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.R;

import java.util.List;

import Model.ordermodel.Order_Parts;
import dbhelper.PartDetailsDB;

public class ViewCartAdapter extends ArrayAdapter  implements View.OnTouchListener {

    private Context context;
    private List<Order_Parts> cartdetails;


    public ViewCartAdapter(Context context, List<Order_Parts> order_parts) {
        super(context, R.layout.viewcartadapter);
        this.context=context;
        this.cartdetails=order_parts;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView=null;

        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.viewcartadapter,null);
            TextView partnumber=convertView.findViewById(R.id.partnumber);
            TextView qtyyy=convertView.findViewById(R.id.qtyyy);
            TextView amtt=convertView.findViewById(R.id.amtt);
            TextView sino=convertView.findViewById(R.id.sino);
            ImageView editalert=convertView.findViewById(R.id.editalert);
            ImageView deleteitem=convertView.findViewById(R.id.deleteitem);

            sino.setText(String.format("%s",position+1));
            partnumber.setText(cartdetails.get(position).getPartNumber());
            qtyyy.setText(cartdetails.get(position).getQuantity());
            amtt.setText(cartdetails.get(position).getTotalAmount());

            editalert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertBox(cartdetails.get(position).getPartNumber(),cartdetails.get(position).getQuantity(),cartdetails.get(position).getTotalAmount(),cartdetails.get(position).getPartPrice(),cartdetails.get(position).getDescription());
                }
            });

            deleteitem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDeleteAlertBox(position);
                }
            });
        }
        return convertView;
    }

    public void showAlertBox(final String partNumber, final String quantity, final String totalAmount, final String partPrice, final String description)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        View dialogView=LayoutInflater.from(context).inflate(R.layout.cart_quantity_alert,null);
        final TextView partNumbers=dialogView.findViewById(R.id.partnum_details);
        final EditText editText=dialogView.findViewById(R.id.quantity_edit_text);
        TextView save=dialogView.findViewById(R.id.cart_alert_save);
        TextView cancel=dialogView.findViewById(R.id.cart_alert_cancel);
        editText.setText(quantity);
        partNumbers.setText(partNumber);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qty=editText.getText().toString();
                if(qty.equals("0") || TextUtils.isEmpty(qty)) {
                    alertDialog.dismiss();
                    StyleableToast st = new StyleableToast(context,
                            "Please enter a valid number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else
                {
                    int pp= Integer.parseInt(String.valueOf(partPrice));
                    int qq=Integer.valueOf(String.valueOf(qty));
                    int tots=pp*qq;
                    String tamt=String.valueOf(tots);
                    PartDetailsDB partDetailsDB = new PartDetailsDB(context);
                    partDetailsDB.updateCartQuantity(partNumber, qty,tamt,description);
                    cartdetails = partDetailsDB.getCartItem();
                    notifyDataSetChanged();
                    alertDialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }


    private void showDeleteAlertBox(final int position)
    {
        final android.support.v7.app.AlertDialog alertDialog=new android.support.v7.app.AlertDialog.Builder(context).create();
        View deleteAlert=LayoutInflater.from(context).inflate(R.layout.cart_delete_alert,null);
        TextView yes=deleteAlert.findViewById(R.id.delete_yes);
        TextView no=deleteAlert.findViewById(R.id.delete_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PartDetailsDB partDetailsDB=new PartDetailsDB(context);
                boolean bool=partDetailsDB.deleteCartItems(cartdetails.get(position).getPartNumber());
                cartdetails=partDetailsDB.getCartItem();
                notifyDataSetChanged();
                alertDialog.dismiss();

                if(!bool)
                {
                    StyleableToast st = new StyleableToast(context,
                            "Please try again", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(deleteAlert);
        alertDialog.show();

    }

    @Override
    public int getCount() {
        return cartdetails.size();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
