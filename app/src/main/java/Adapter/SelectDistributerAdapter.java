package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tvs.R;

import java.util.List;

import Model.distributerdetails.DistriButerData;
import Model.distributerdetails.DistributerDetails;
import retrofit2.Callback;

public class SelectDistributerAdapter extends ArrayAdapter {

    private Context context;
    String distributermobile;
    private List<DistriButerData> distributerlist;
    private Disdetails disdetails;
    String disid="",disnameselected="",disaddressselected="";

    public SelectDistributerAdapter(Context context,List<DistriButerData> distributerlist) {
        super(context,R.layout.distributeradapter);
        this.context=context;
        this.distributerlist=distributerlist;
        disdetails=(Disdetails) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView=null;

        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.distributeradapter,null);
            CheckBox selectdis=convertView.findViewById(R.id.selectdis);
            TextView disname=convertView.findViewById(R.id.disname);
//            TextView dismobile=convertView.findViewById(R.id.dismobile);
            final TextView disaddress=convertView.findViewById(R.id.disaddress);
            disname.setText(distributerlist.get(position).getDisname());
//            dismobile.setText(distributerlist.get(position).getMobileno());
            disaddress.setText(distributerlist.get(position).getAddress());

            if(disid.equals(distributerlist.get(position).getPId()))
            {
                selectdis.setChecked(true);
            }
            selectdis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if (checked){
                        compoundButton.setChecked(checked);
                        disid=distributerlist.get(position).getPId();
                        disnameselected=distributerlist.get(position).getDisname();
                        disaddressselected=distributerlist.get(position).getAddress();
                         disdetails.senddistributerid(distributerlist.get(position).getPId());
                         disdetails.senddistributername(distributerlist.get(position).getDisname());
                         disdetails.senddistributeraddress(distributerlist.get(position).getAddress());
                         notifyDataSetChanged();
                    }else {
                        compoundButton.setChecked(false);
                        disid="";
                        disdetails.senddistributerid("");
                    }
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return distributerlist.size();
    }

    public  interface Disdetails{
        void senddistributerid(String distriid);
        void senddistributername(String distriname);
        void senddistributeraddress(String distriaddress);
    }
}
