package Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.R;

import java.util.List;

import Model.ordermodel.Order_Parts;
import dbhelper.PartDetailsDB;

public class ConfirmOrderAdapter extends ArrayAdapter {

    private Context context;
    private List<Order_Parts> cartdetails;

    public ConfirmOrderAdapter(Context context, List<Order_Parts> order_parts) {
        super(context, R.layout.viewconfirmorderadapter);
        this.context=context;
        this.cartdetails=order_parts;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView=null;
        if (convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.viewconfirmorderadapter,null);
            TextView sinoc=convertView.findViewById(R.id.sinoc);
            TextView partnumbercof=convertView.findViewById(R.id.partnumbercof);
            TextView qtycon=convertView.findViewById(R.id.qtycon);
            TextView amtcon=convertView.findViewById(R.id.amtcon);

            sinoc.setText(String.format("%s",position+1));
            partnumbercof.setText(cartdetails.get(position).getPartNumber());
            qtycon.setText(cartdetails.get(position).getQuantity());
            amtcon.setText("₹"+" "+cartdetails.get(position).getTotalAmount()+".00");

        }
        return convertView;
    }


    @Override
    public int getCount() {
        return cartdetails.size();
    }
}
