package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tvs.R;
import com.tvs.ViewAttendanceDate;

import java.text.DateFormatSymbols;
import java.util.List;

import Model.attendancedatewise.DatewiseAttendanceData;
import Model.viewattendance.ViewAttendanceData;

public class ViewAttendanceAdapter extends ArrayAdapter {

    private Context context;
    private List<ViewAttendanceData> viewAttendanceDatas;
    private List<DatewiseAttendanceData> datewiseAttendanceData;

    public ViewAttendanceAdapter(@NonNull Context context, List<ViewAttendanceData> attendanceData, boolean isbool) {
        super(context, R.layout.viewattendanceadapter);
        this.context=context;
        this.viewAttendanceDatas=attendanceData;
    }

    public ViewAttendanceAdapter(@NonNull Context context, List<DatewiseAttendanceData> datewiseAttendanceData,Boolean isbool) {
        super(context, R.layout.viewattendanceadapter);
        this.context=context;
        this.datewiseAttendanceData=datewiseAttendanceData;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=null;
        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.viewattendanceadapter,null,false);

            TextView day1=convertView.findViewById(R.id.day1);
            TextView date1=convertView.findViewById(R.id.date1);
            TextView intime=convertView.findViewById(R.id.intime);
            TextView outtime=convertView.findViewById(R.id.outtime);

            if (viewAttendanceDatas==null){
                day1.setText(datewiseAttendanceData.get(position).getAttendDay());
                final String databsedate=datewiseAttendanceData.get(position).getDate();
                String [] getdateonly=databsedate.split("T");
                String dateonly=getdateonly[0];
                String [] getdaymonthyear=dateonly.split("-");

                String monthnumber=getdaymonthyear[1];
                String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber)-1];
                String monthget=month.substring(0, 3);

                String year=getdaymonthyear[0];
                String yearend=year.substring(2,4);

                String daytemonthyear=getdaymonthyear[2]+"-"+monthget+"-"+yearend;
                date1.setText(daytemonthyear);
                intime.setText(datewiseAttendanceData.get(position).getInTime());
                outtime.setText(datewiseAttendanceData.get(position).getOutTime());

                date1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent datedetails=new Intent(context.getApplicationContext(), ViewAttendanceDate.class);
                        datedetails.putExtra("traveldate",datewiseAttendanceData.get(position).getDate());
                        datedetails.putExtra("trackids",datewiseAttendanceData.get(position).getId());
                        context.startActivity(datedetails);
                    }
                });
            }
            else {
                day1.setText(viewAttendanceDatas.get(position).getAttendDay());
                final String databsedate = viewAttendanceDatas.get(position).getDate();
                String[] getdateonly = databsedate.split("T");
                String dateonly = getdateonly[0];
                String[] getdaymonthyear = dateonly.split("-");
                String monthnumber=getdaymonthyear[1];
                String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber)-1];
                String monthget=month.substring(0, 3);
                String year=getdaymonthyear[0];
                String yearend=year.substring(2,4);
                String daytemonthyear = getdaymonthyear[2] + "-" + monthget + "-" + yearend;
                date1.setText(daytemonthyear);
                intime.setText(viewAttendanceDatas.get(position).getInTime());
                outtime.setText(viewAttendanceDatas.get(position).getOutTime());

                date1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent datedetails = new Intent(context.getApplicationContext(), ViewAttendanceDate.class);
                        datedetails.putExtra("traveldate", viewAttendanceDatas.get(position).getDate());
                        datedetails.putExtra("trackids", viewAttendanceDatas.get(position).getId());
                        context.startActivity(datedetails);
                    }
                });
            }
        }

        return convertView;
    }

    @Override
    public int getCount() {
        if (viewAttendanceDatas==null){
            return datewiseAttendanceData.size();
        }
        return viewAttendanceDatas.size();
    }
}
