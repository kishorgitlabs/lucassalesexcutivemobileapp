package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tvs.Coordinates;
import com.tvs.MapActivity;
import com.tvs.MapsActivity;
import com.tvs.R;
import com.tvs.ViewAttendanceDate;

import java.util.List;

import Model.viewattendancedatewise.ViewAttendanceData;

public class AttendanceDateWise extends RecyclerView.Adapter<AttendanceDateWise.Viewholder> {

    Context context;
    private List<ViewAttendanceData> viewAttendanceDatas;
    private String tackid,trackdate,dateformap;

    public AttendanceDateWise(ViewAttendanceDate viewAttendanceDate, List<ViewAttendanceData> viewAttendanceData, String trackid, String trackdate, String mapdate) {
        this.viewAttendanceDatas=viewAttendanceData;
        this.context=viewAttendanceDate;
        this.tackid=trackid;
        this.trackdate=mapdate;
        this.dateformap=trackdate;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.attendancedatedetailsadapter, parent,false);
            Viewholder viewholder=new Viewholder(view);

        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, final int position) {

        holder.sno.setText(position+1+"");
        holder.inaddress.setText(viewAttendanceDatas.get(position).getAddress());
        holder.outaddress.setText(viewAttendanceDatas.get(position).getOutAddress());

        holder.datedetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent coordinares=new Intent(context.getApplicationContext(), Coordinates.class);
//                coordinares.putExtra("trackid",viewAttendanceDatas.get(position).getId());
                coordinares.putExtra("trackid",tackid);
                coordinares.putExtra("trackdate",dateformap);
                context.startActivity(coordinares);
            }
        });

        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map=new Intent(context.getApplicationContext(), MapsActivity.class);
                map.putExtra("traveldate",dateformap);
                context.startActivity(map);
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewAttendanceDatas.size();
    }

        public class Viewholder extends RecyclerView.ViewHolder{

        public  TextView sno,inaddress,outaddress;
        public LinearLayout datedetails;
        public ImageView map;

        public Viewholder(View itemView) {
            super(itemView);
            sno=itemView.findViewById(R.id.sno);
            datedetails=itemView.findViewById(R.id.datedetails);
             inaddress=itemView.findViewById(R.id.inaddress);
             outaddress=itemView.findViewById(R.id.outaddress);
             map=itemView.findViewById(R.id.map);
        }
    }

}
