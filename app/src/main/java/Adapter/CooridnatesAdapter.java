package Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tvs.Coordinates;
import com.tvs.R;

import java.util.List;

import Model.cooridnates.CoordinatesData;

public class CooridnatesAdapter extends RecyclerView.Adapter<CooridnatesAdapter.Viewholderdata> {

    Context context;
    private List<CoordinatesData> coordinatesDatas;

    public CooridnatesAdapter(Coordinates coordinates, List<CoordinatesData> coordinatesData) {
        this.coordinatesDatas=coordinatesData;
        this.context=coordinates;

    }

    @NonNull
    @Override
    public Viewholderdata onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View views=LayoutInflater.from(parent.getContext()).inflate(R.layout.coordintesadapter, parent,false);
        Viewholderdata viewholderdata=new Viewholderdata(views);
        return viewholderdata;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholderdata holder, final int position) {

        holder.snoo.setText(position+1+"");
        holder.timing.setText(coordinatesDatas.get(position).getTimedate());
        holder.timeaddresss.setText(coordinatesDatas.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return coordinatesDatas.size();
    }


    public class Viewholderdata extends RecyclerView.ViewHolder{

        public TextView snoo,timing,timeaddresss;

        public Viewholderdata(View itemView) {
            super(itemView);
            snoo =itemView.findViewById(R.id.snoo);
            timing =itemView.findViewById(R.id.timing);
            timeaddresss =itemView.findViewById(R.id.timeaddresss);
        }
    }
}
