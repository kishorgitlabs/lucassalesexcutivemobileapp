package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tvs.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import Model.orderdetailsview.ViewOrderDetailsData;

public class OrderDetailsAdapter extends ArrayAdapter {

    Context context;
    private List<ViewOrderDetailsData> viewOrderDetailss;


    public OrderDetailsAdapter(Context context, List<ViewOrderDetailsData> viewOrderDetails) {
        super(context, R.layout.orderdetailsfully);
        this.context=context;
        this.viewOrderDetailss=viewOrderDetails;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView=null;
        if (convertView==null){
            convertView=LayoutInflater.from(context).inflate(R.layout.orderdetailsfully,null);

            TextView sino1=convertView.findViewById(R.id.sino1);
            TextView partno=convertView.findViewById(R.id.partno);
            TextView pardes=convertView.findViewById(R.id.pardes);
            TextView qtyss=convertView.findViewById(R.id.qtyss);
            TextView totalvalues=convertView.findViewById(R.id.totalvalues);

            sino1.setText(position+1+"");
            partno.setText(viewOrderDetailss.get(position).getPartNo());
            pardes.setText(viewOrderDetailss.get(position).getDescription());
            qtyss.setText(viewOrderDetailss.get(position).getQuantity());

            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            int ordervalue= Integer.parseInt(viewOrderDetailss.get(position).getAmount());
            String moneyString = formatter.format(ordervalue);
            totalvalues.setText(moneyString);

        }
        return convertView;
    }

    @Override
    public int getCount() {
        return viewOrderDetailss.size();
    }
}
