package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tvs.EditDistributer;
import com.tvs.R;

import java.util.List;

import Model.distributerdetails.DistriButerData;

public class ViewDistributorAdapter extends ArrayAdapter {
    Context context;

    private List<DistriButerData> disdata;
    private AlertDialog distributorinfo;

    public ViewDistributorAdapter(@NonNull Context context, List<DistriButerData> distriButerData) {
        super(context, R.layout.distributorviewadapter);
        this.context=context;
        this.disdata=distriButerData;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.distributorviewadapter,null);

            TextView sino1=convertView.findViewById(R.id.sino1);
            TextView disname=convertView.findViewById(R.id.disname);

            TextView discodead=convertView.findViewById(R.id.discodead);
            ImageView moreinfo=convertView.findViewById(R.id.moreinfo);
            ImageView editinfo=convertView.findViewById(R.id.editinfo);
            TextView shortname=convertView.findViewById(R.id.shortname);

            sino1.setText(position+1+"");
            disname.setText(disdata.get(position).getDisname());
            discodead.setText(disdata.get(position).getCusNo());
            shortname.setText(disdata.get(position).getmShortname());

            editinfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent editdis=new Intent(context, EditDistributer.class);
                    editdis.putExtra("city",disdata.get(position).getTownnme());
                    editdis.putExtra("discode",disdata.get(position).getCusNo());
                    editdis.putExtra("disname",disdata.get(position).getDisname());
                    editdis.putExtra("disemail",disdata.get(position).getEmailid());
                    editdis.putExtra("discontac",disdata.get(position).getCname());
                    editdis.putExtra("dismobile",disdata.get(position).getMobileno());
                    editdis.putExtra("disaddress",disdata.get(position).getAddress());
                    context.startActivity(editdis);
                }
            });

            moreinfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    distributorinfo(disdata.get(position).getTownnme(),disdata.get(position).getCusNo(),disdata.get(position).getDisname(),disdata.get(position).getEmailid(),disdata.get(position).getCname(),disdata.get(position).getMobileno(),disdata.get(position).getAddress());
                }
            });
        }
        return convertView;
    }

    private void distributorinfo(String townname, String cusno, String disname, String emailid, String cname, String mobileno, String addrrss) {
        distributorinfo=new AlertDialog.Builder(context).create();
        View distributordetails= LayoutInflater.from(context).inflate(R.layout.distributeralertview,null);

        TextView cityview=distributordetails.findViewById(R.id.cityview);
        TextView codeview=distributordetails.findViewById(R.id.codeview);
        TextView nameview=distributordetails.findViewById(R.id.nameview);
        TextView emailidview=distributordetails.findViewById(R.id.emailidview);
        TextView contactaddressview=distributordetails.findViewById(R.id.contactaddressview);
        TextView mobileview=distributordetails.findViewById(R.id.mobileview);
        TextView addressview=distributordetails.findViewById(R.id.addressview);
        Button cancelexit=distributordetails.findViewById(R.id.cancelexit);

        cityview.setText(townname);
        codeview.setText(cusno);
        nameview.setText(disname);
        emailidview.setText(emailid);
        contactaddressview.setText(cname);
        mobileview.setText(mobileno);
        addressview.setText(addrrss);

        cancelexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distributorinfo.dismiss();
            }
        });

        distributorinfo.setView(distributordetails);
        distributorinfo.setCanceledOnTouchOutside(false);
        distributorinfo.show();
    }
    @Override
    public int getCount() {
        return disdata.size();
    }
}
