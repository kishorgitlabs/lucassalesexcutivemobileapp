package Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tvs.OrderDetailsFully;
import com.tvs.R;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import Model.viewordersalesrep.ViewOrderSalesData;
import Model.viewsalesdatewise.OrderList;

public class OrderViewAdapter extends ArrayAdapter {

    Context context;
    private List<Model.viewordersalesrep.OrderList> viewOrderSalesDatas;
    private List<OrderList> orderLists;

    public OrderViewAdapter(Context context, List<Model.viewordersalesrep.OrderList> viewOrderSalesData) {
        super(context, R.layout.orderviewadapter);
        this.context=context;
        this.viewOrderSalesDatas=viewOrderSalesData;
    }

    public  OrderViewAdapter(Context context,List<OrderList> orderLists,boolean isBool){
        super(context, R.layout.orderviewadapter);
        this.context=context;
        this.orderLists=orderLists;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView=null;
        if (convertView==null){
            convertView=LayoutInflater.from(context).inflate(R.layout.orderviewadapter,null);
            TextView sinview=convertView.findViewById(R.id.sinview);
            TextView dateo=convertView.findViewById(R.id.dateo);
            TextView ordernumbview=convertView.findViewById(R.id.ordernumbview);
            TextView totalamt=convertView.findViewById(R.id.totalamt);

            if (viewOrderSalesDatas==null){
                sinview.setText(String.format("%s",position+1));
                String datatime=orderLists.get(position).getOrderedDate();
                String datesplit[]=datatime.split("T");
                String spliteddate=datesplit[0];
                String dateonly[]=spliteddate.split("-");

                String monthnumber=dateonly[1];
                String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber)-1];
                String monthget=month.substring(0, 3);
                String year=dateonly[0];
                String yearend=year.substring(2,4);
                String datefinal=dateonly[2]+"-"+monthget+"-"+yearend;
                dateo.setText(datefinal);
                ordernumbview.setText(orderLists.get(position).getOrderid());

                NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
                int ordervalue= Integer.parseInt(orderLists.get(position).getUnitprice());
                String moneyString = formatter.format(ordervalue);
                totalamt.setText(moneyString);
                ordernumbview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent orderdetails=new Intent(context, OrderDetailsFully.class);
                        orderdetails.putExtra("orderid",orderLists.get(position).getOrderid());
                        orderdetails.putExtra("total",orderLists.get(position).getUnitprice());
                        context.startActivity(orderdetails);
                    }
                });
            }else {
                sinview.setText(String.format("%s", position + 1));
                String datatime = viewOrderSalesDatas.get(position).getOrderedDate();
                String datesplit[] = datatime.split("T");
                String spliteddate = datesplit[0];
                String dateonly[] = spliteddate.split("-");
                String monthnumber=dateonly[1];
                String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber)-1];
                String monthget=month.substring(0, 3);
                String year=dateonly[0];
                String yearend=year.substring(2,4);
                String datefinal = dateonly[2] + "-" + monthget + "-" + yearend;
                dateo.setText(datefinal);
                ordernumbview.setText(viewOrderSalesDatas.get(position).getOrderid());

                NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
                int ordervalue= Integer.parseInt(viewOrderSalesDatas.get(position).getUnitprice());
                String moneyString = formatter.format(ordervalue);
                totalamt.setText(moneyString);

                ordernumbview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent orderdetails = new Intent(context, OrderDetailsFully.class);
                        orderdetails.putExtra("orderid", viewOrderSalesDatas.get(position).getOrderid());
                        orderdetails.putExtra("total",viewOrderSalesDatas.get(position).getUnitprice());
                        context.startActivity(orderdetails);
                    }
                });
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        if (viewOrderSalesDatas==null){
           return orderLists.size();

        }else {
            return viewOrderSalesDatas.size();
        }
    }
}
