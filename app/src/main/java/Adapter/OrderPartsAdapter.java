package Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.tvs.PartDetails;
import com.tvs.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import Model.ordermodel.Order_Parts;
import dbhelper.PartDetailsDB;

public class OrderPartsAdapter extends ArrayAdapter {

    private  Context context;
    private List<Order_Parts> order_partss;
    private Recretatesipelistview recretatesipelistview;


    public OrderPartsAdapter(@NonNull Context context, List<Order_Parts> order_parts) {
        super(context, R.layout.cartadapter);
        this.context=context;
        this.order_partss=order_parts;
        this.recretatesipelistview=(Recretatesipelistview) context;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView=null;
        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.cartadapter,null);

            TextView sinview=convertView.findViewById(R.id.sinview);
            TextView partnumn=convertView.findViewById(R.id.partnumn);
            TextView partprice=convertView.findViewById(R.id.partprice);
            TextView totalqty=convertView.findViewById(R.id.totalqty);
            TextView ordervalue=convertView.findViewById(R.id.ordervalue);
            sinview.setText(position+1+"");
            partnumn.setText(order_partss.get(position).getPartNumber());

            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            int ordervalues= Integer.parseInt(order_partss.get(position).getPartPrice());
            String moneyString = formatter.format(ordervalues);
            partprice.setText(moneyString);
            totalqty.setText(order_partss.get(position).getQuantity());

            NumberFormat formatters = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            int ordervaluess= Integer.parseInt(order_partss.get(position).getTotalAmount());
            String moneyStrings = formatter.format(ordervaluess);
            ordervalue.setText(moneyStrings);

            totalqty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlertBox(order_partss.get(position).getPartNumber(),order_partss.get(position).getQuantity(),order_partss.get(position).getTotalAmount(),order_partss.get(position).getPartPrice(),order_partss.get(position).getDescription());
                }
            });
            partnumn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent partdetails=new Intent(context, PartDetails.class);
                    partdetails.putExtra("partnumber",order_partss.get(position).getPartNumber());
                    context.startActivity(partdetails);
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return order_partss.size();
    }

    @Override
    public Order_Parts getItem(int position) {
        return order_partss.get(position);
    }

    public void showAlertBox(final String partNumber, final String quantity, final String totalAmount, final String partPrice, final String description)
    {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        View dialogView=LayoutInflater.from(context).inflate(R.layout.cart_quantity_alert,null);
        final TextView partNumbers=dialogView.findViewById(R.id.partnum_details);
        final EditText editText=dialogView.findViewById(R.id.quantity_edit_text);
        TextView save=dialogView.findViewById(R.id.cart_alert_save);
        TextView cancel=dialogView.findViewById(R.id.cart_alert_cancel);
        editText.setText(quantity);
        partNumbers.setText(partNumber);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qty=editText.getText().toString();
                if(qty.equals("0") || TextUtils.isEmpty(qty)) {
                    alertDialog.dismiss();
                    StyleableToast st = new StyleableToast(context,
                            "Please enter a valid number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else
                {
                    int pp= Integer.parseInt(String.valueOf(partPrice));
                    int qq=Integer.valueOf(String.valueOf(qty));
                    int tots=pp*qq;
                    String tamt=String.valueOf(tots);
                    PartDetailsDB partDetailsDB = new PartDetailsDB(context);
                    partDetailsDB.updateCartQuantity(partNumber, qty,tamt,description);
                    order_partss=partDetailsDB.getCartItem();
                    recretatesipelistview.recreted();
                    alertDialog.dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    public interface Recretatesipelistview{
        void recreted();
    }
}

