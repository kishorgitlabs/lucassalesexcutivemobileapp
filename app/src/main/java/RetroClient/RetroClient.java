package RetroClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import APIInterface.CategoryAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by system9 on 6/30/2017.
 */

public class RetroClient {

    private static final String ROOT_URL = "http://lucasnetwork.in/api/LucasTvs/"; // New Live URL 09 Sept 2022
    // private static final String ROOT_URL = "http://app.lucastvs-network.in/api/LucasTvs/"; // Old Live URL
  // private static final String ROOT_URL = "http://demo.brainmagicllc.com/api/LucasTvs/";

//    private static final String ROOT_URL = "http://demo.brainmagicllc.com/api/LucasTvs/";

    // private static final String ROOT_URL_SALES = "http://demo.brainmagicllc.com/api/Values/"; // Old Live URL
    private static final String ROOT_URL_SALES = "http://lucasnetwork.in/api/Values/"; // New  Live URL 09 Sept 2022
    /**
     * Get Retrofit Instance
     */

    Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    private static Retrofit getRetrofitInstanceSales() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL_SALES)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    /**
     * Get API Service
     *
     * @return API Service
     */
    public static CategoryAPI getApiService() {
        return getRetrofitInstance().create(CategoryAPI.class);
    }

    public static CategoryAPI getApiServicesales() {
        return getRetrofitInstanceSales().create(CategoryAPI.class);
    }
}
